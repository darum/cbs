package cbs.web.service.taskCol_query;

import java.util.List;

import org.junit.Test;

import cbs.web.service.packet.ColumnQuery;
import cbs.web.service.packet.databean.MetaDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestTaskColQueryQueryAll extends AbstractTestTaskColQuery {

	public TestTaskColQueryQueryAll() throws Exception {
		super();
	}
	
	/* Init DB Data */
	private static final String DATA_FILE = "TestTaskColQueryQueryAll.xml";
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_task_col_def",
				"cbs_pjuser",
				"cbs_project"
		};
	}

	/**
	 * 正常系
	 * @throws Exception
	 */
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		ColumnQuery req = new ColumnQuery(50);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ColumnQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ColumnQuery.CBS_WS_STAT_SUCCESS));

		List<MetaDataBean> cols = result.getColumns();
		assertThat(cols.size(), is(1));
		
		MetaDataBean bean = cols.get(0);
		assertThat(bean.getId(), is(2000));
		assertThat(bean.getName(), is("Column2"));
		assertThat(bean.getDataType(), is(1));
	}

	/**
	 * 0件取得テスト
	 * @throws Exception
	 */
	@Test
	public void Normal_Success0CountTest() throws Exception {
		// Packet
		ColumnQuery req = new ColumnQuery(60);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ColumnQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ColumnQuery.CBS_WS_STAT_SUCCESS));

		List<MetaDataBean> cols = result.getColumns();
		assertThat(cols.size(), is(0));
	}
	
	@Test
	public void Failure_InvalidProjectIdTest() throws Exception {
		// Packet
		ColumnQuery req = new ColumnQuery(70);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ColumnQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ColumnQuery.CBS_WS_STAT_INVALID_PROJECT_ID));
	}
	
	@Test
	public void Failure_InvalidSessionIdTest() throws Exception {
		// Packet
		ColumnQuery req = new ColumnQuery(50);
		req.setSessionId("session");
		
		// Exec
		ColumnQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ColumnQuery.CBS_WS_STAT_SESSID_ERROR));
	}
}
