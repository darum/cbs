package cbs.web.service.taskCol_query;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.TaskColQueryAction;
import cbs.web.service.packet.ColumnQuery;

public class AbstractTestTaskColQuery extends CbsWsTestBase {

	public AbstractTestTaskColQuery() throws Exception {
		super();
	}

	
	/* Test I/F */
	private static final String QUERYALL_PATH = "/taskCol_query/queryAll";
	
	@Override
	protected Application configure() {
		return new ResourceConfig(TaskColQueryAction.class);
	}
	
	/* API call */
	protected ColumnQuery queryAllPost(final ColumnQuery req) throws Exception {
		return sendPost(req, QUERYALL_PATH).readEntity(ColumnQuery.class);
	}
}
