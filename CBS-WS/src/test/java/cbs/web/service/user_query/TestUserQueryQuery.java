package cbs.web.service.user_query;

import java.util.List;

import org.junit.Test;

import cbs.web.service.packet.SysUserQuery;
import cbs.web.service.packet.databean.UserDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestUserQueryQuery extends TestUserQueryBase {
	
	private final String DATA_FILE = "TestUserQueryQuery.xml";

	public TestUserQueryQuery() throws Exception {
		super();
	}

	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}

	@Test
	public void Normal_SuccessTest() throws Exception {
		
		// Query
		SysUserQuery req = new SysUserQuery("account");
		req.setSessionId(getAuthInfo().getSessionId());
		SysUserQuery resp = queryPost(req);

		assertThat(resp, is(notNullValue()));
		assertThat(resp.getStatus(), is(SysUserQuery.CBS_WS_STAT_SUCCESS));
		List<UserDataBean> result = resp.getResult();
		assertThat(result.size(), is(1));
		
		UserDataBean bean = result.get(0);
		assertThat(bean.getAccount(), startsWith("account"));
		assertThat(bean.getEmail(), startsWith("foo@example.com"));
		assertThat(bean.getFirstName(), startsWith("FIRST"));
		assertThat(bean.getLastName(), startsWith("LAST"));
	}
	
	@Test
	public void Normal_Success_Count0Test() throws Exception {
		// Query
		SysUserQuery req = new SysUserQuery("nothing");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		SysUserQuery result = queryPost(req); 
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserQuery.CBS_WS_STAT_SUCCESS));
		assertThat(result.getResult().size(), is(0));
	}
	
	@Test
	public void Fail_SessionErrorTest() throws Exception {
		SysUserQuery req = new SysUserQuery("account");
		req.setSessionId("a");
		
		// Exec
		SysUserQuery result = queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserQuery.CBS_WS_STAT_SESSID_ERROR));
	}
	
	@Test
	public void Fail_InvalidPacketTest() throws Exception {
		// Packet
		SysUserQuery req = new SysUserQuery();
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		SysUserQuery result = queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserQuery.CBS_WS_STAT_INVALID_PACKET));
	}
}
