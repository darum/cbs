package cbs.web.service.user_query;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.UserQueryAction;
import cbs.web.service.packet.SysUserQuery;

abstract class TestUserQueryBase extends CbsWsTestBase {

	public TestUserQueryBase() throws Exception {
		super();
	}
	
	/* Test用 I/F */
	protected final String QUERY_PATH = "/user_query/query";
	
	@Override
	protected Application configure() {
		return new ResourceConfig(UserQueryAction.class);
	}
	
	/* API Call用メソッド */
	protected SysUserQuery queryPost(final SysUserQuery req) throws Exception {
		return sendPost(req, QUERY_PATH).readEntity(SysUserQuery.class);
	}
	
			

}
