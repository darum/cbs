package cbs.web.service.task_ctrl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.TaskControl;
import cbs.web.service.packet.databean.ColumnDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestTaskCtrlAdd extends AbstractTestTaskCtrl {
	
	private final String DATA_FILE = "TestTaskCtrlAdd.xml";

	public TestTaskCtrlAdd() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}

	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_pjuser",
				"cbs_task",
				"cbs_project_resource",
				"cbs_resource",
				"cbs_project"
		};
	}
	
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_SUCCESS));
		assertThat(result.getTaskId(), greaterThan(1));
		
		// DB
		ITable table = super.getTableData("cbs_task");
		assertThat((Integer)table.getValue(0, "project_id"), is(30));
		assertThat((String)table.getValue(0, "task_name"), startsWith("Task01"));
		
		System.out.println(table.getValue(0, "start_datetime").getClass().toString());
		
	}
	
	/**
	 * プロジェクトIDエラーテスト
	 * @throws Exception
	 */
	@Test
	public void Failure_InvalidProjectId() throws Exception {
		// Packet
		TaskControl req = new TaskControl();
		req.setTaskName("New Task");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_PJID));
		
	}
	
	@Test
	public void Failure_InvalidParentIdTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		req.setParentTaskId(11111);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_PARENT_NOT_FOUND));
		
	}
	
	@Test
	public void Failure_InvalidResIdTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		req.setResId(7000);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_RES_ID));
		
	}
	
	@Test
	public void Failure_InvalidDateTimeTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 8, 14);
		req.setTermDT(termDT.getTimeInMillis());
		
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_DATE));
		
	}
	
	@Test
	public void Failure_InvalidProgressTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(120.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_PROG));
	}
	
	/**
	 * カラムIDエラーのテスト
	 * @throws Exception
	 */
	@Test
	public void Failure_InvalidColumnIdError() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		ColumnDataBean dataBean = new ColumnDataBean(1, "VALUE");	// ID=1: 存在しない
		columns.add(dataBean);
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_COLUMN_ID));
		
	}
	
	@Test
	public void Failure_InvalidSessionIdTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl(30, "Task01");
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId("Session");
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_SESSID_ERROR));
	}
	
	@Test
	public void Failure_LackOfTaskNameTest() throws Exception {
		// Packet
		TaskControl req = new TaskControl();
		req.setPjId(30);
		Calendar startDT = Calendar.getInstance();
		startDT.set(2014, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2014, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_PACKET));
		
	}
}
