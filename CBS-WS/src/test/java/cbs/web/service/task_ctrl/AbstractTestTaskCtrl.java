package cbs.web.service.task_ctrl;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.packet.TaskControl;

public class AbstractTestTaskCtrl extends CbsWsTestBase {

	public AbstractTestTaskCtrl() throws Exception {
		super();
	}
	
	@Override
	protected Application configure() {
		return new ResourceConfig(TaskControl.class);
	}
	
	private static String ADD_PATH = "/task_ctrl/add";
	private static String MODIFY_PATH = "/task_ctrl/modify";
	private static String DEL_PATH = "/task_ctrl/del";
	
	protected TaskControl addPost(final TaskControl req) throws Exception {
		return sendPost(req, ADD_PATH).readEntity(TaskControl.class);
	}

	protected TaskControl modifyPost(final TaskControl req) throws Exception {
		return sendPost(req, MODIFY_PATH).readEntity(TaskControl.class);
	}

	protected TaskControl delPost(final TaskControl req) throws Exception {
		return sendPost(req, DEL_PATH).readEntity(TaskControl.class);
	}

}
