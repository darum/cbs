package cbs.web.service.task_ctrl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.TaskControl;
import cbs.web.service.packet.databean.ColumnDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestTaskCtrlModify extends AbstractTestTaskCtrl {
	
	private final String DATA_FILE = "TestTaskCtrlModify.xml";
	
	public TestTaskCtrlModify() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_task"
				,"cbs_project_resource"
				,"cbs_resource"
				,"cbs_pjuser"
				,"cbs_project"
		};
	}

	@Test
	public void Normal_SuccessTest() throws Exception {
		// すべての項目が更新できること
		TaskControl req = new TaskControl(5000);
		Calendar startDT = Calendar.getInstance();
		startDT.set(2015, 8, 15);
		req.setStartDT(startDT.getTimeInMillis());
		Calendar termDT = Calendar.getInstance();
		termDT.set(2015, 9, 15);
		req.setTermDT(termDT.getTimeInMillis());
		req.setProg(50.0);
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_SUCCESS));
		
		// DB
		ITable table = super.getTableData("cbs_task");
		Timestamp tmp;
		tmp = (Timestamp)table.getValue(0, "start_datetime");
		assertThat(tmp.getTime(), is(startDT.getTimeInMillis()));
		tmp = (Timestamp)table.getValue(0, "term_datetime");
		assertThat(tmp.getTime(), is(termDT.getTimeInMillis()));
		assertThat((double)table.getValue(0, "progress"), is(50.0));
		assertThat((int)table.getValue(0, "update_number"), is(2));
	}
	
	/**
	 * タスクIDエラー
	 * @throws Exception
	 */
	@Test
	public void Failure_InvalidTaskId() throws Exception {
		TaskControl req = new TaskControl(6000);
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = modifyPost(req);
		
		// Assert 
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_TASKID));
	}
	
	/**
	 * 親タスクなし
	 */
	@Test
	public void Failure_InvalidParentTaskId() throws Exception {
		TaskControl req = new TaskControl(5000);
		
		req.setParentTaskId(new Integer(10));
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = modifyPost(req);
		
		// Assert 
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_PARENT_NOT_FOUND));
		
	}
	
	@Test
	public void Failure_InvalidResId() throws Exception {
		TaskControl req = new TaskControl(5000);
		
		req.setResId(7001);
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_RES_ID));
	}
	
	@Test
	public void Failure_InvalidDate() throws Exception {
		TaskControl req;
		
//		Date start = new Date();
		Calendar start = Calendar.getInstance();
		Calendar term = Calendar.getInstance();
		
		// Note: XMLで初期値をうまく設定できないのでここでセット
		req = new TaskControl(5001);
		start.set(2015, 0, 1);		// 2015/1/1
		term.set(2015, 2, 1);		// 2015/3/1
		req.setStartDT(start.getTimeInMillis());
		req.setTermDT(term.getTimeInMillis());
		req.setSessionId(getAuthInfo().getSessionId());
		TaskControl result = modifyPost(req);
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_SUCCESS));
		
		// 開始日が設定済みの終了日より後
		req  = new TaskControl(5001);
		start.set(2015, 3, 1);		// 2015/4/1
		req.setStartDT(start.getTimeInMillis());
		
		// Exec
		req.setSessionId(getAuthInfo().getSessionId());
		result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_DATE));
		
		
		// 終了日が設定済みの開始日より前
		req  = new TaskControl(5001);
		term.set(2014, 11, 1);		// 2014/12/1
		req.setTermDT(term.getTimeInMillis());
		
		// Exec
		req.setSessionId(getAuthInfo().getSessionId());
		result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_DATE));
		
		
		// 終了日＜開始日
		req  = new TaskControl(5001);
		start.set(2015, 3, 1);		// 2015/4/1
		term.set(2015, 0, 1);		// 2015/1/1
		req.setStartDT(start.getTimeInMillis());
		req.setTermDT(term.getTimeInMillis());
		
		// Exec
		req.setSessionId(getAuthInfo().getSessionId());
		result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_DATE));
	}
	
	@Test
	public void Failure_InvalidProgress() throws Exception {
		TaskControl req;
		TaskControl result;

		// Progressが0以下
		req = new TaskControl(5000);
		req.setProg(-1.0);
		req.setSessionId(getAuthInfo().getSessionId());
		result = modifyPost(req);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_PROG));
		
		// Progressが100以上
		req = new TaskControl(5000);
		req.setProg(100.1);
		req.setSessionId(getAuthInfo().getSessionId());
		result = modifyPost(req);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_PROG));
	}
	
	// TODO
//	@Test
	public void Failure_InvalidColumnIdError() throws Exception {
		// Packet
		TaskControl req = new TaskControl(5000);
		
		List<ColumnDataBean> columns = new ArrayList<ColumnDataBean>();
		ColumnDataBean dataBean = new ColumnDataBean(1, "VALUE");	// ID=1: 存在しない
		columns.add(dataBean);
		req.setColumns(columns);
		
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		TaskControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_COLUMN_ID));
		
	}
	
	@Test
	public void Failure_InvalidSessionId() throws Exception {
		TaskControl req = new TaskControl(5000);
		
		TaskControl result = modifyPost(req);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_SESSID_ERROR));
	}
	
	/**
	 * タスクID なし
	 * @throws Exception
	 */
	@Test
	public void Failure_NoTaskId() throws Exception {
		TaskControl req = new TaskControl();
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		TaskControl result = modifyPost(req);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_TASKID));
	}

	@Test
	public void Failure_NoUpdateData() throws Exception {
		TaskControl req = new TaskControl(5000);
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		TaskControl result = modifyPost(req);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskControl.CBS_WS_STAT_INVALID_PACKET));
	}
}
