package cbs.web.service.task_query;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.TaskQueryAction;
import cbs.web.service.packet.TaskQuery;

public abstract class AbstractTestTaskQuery extends CbsWsTestBase {

	public AbstractTestTaskQuery() throws Exception {
		super();
	}

	private static final String QUERY_PATH = "/task_query/query";
	
	@Override
	protected Application configure() {
		return new ResourceConfig(TaskQueryAction.class);
	}
	
	protected TaskQuery queryPost(final TaskQuery req) throws Exception {
		return sendPost(req, QUERY_PATH).readEntity(TaskQuery.class);
	}
}
