package cbs.web.service.task_query;

import java.util.List;

import org.junit.Test;

import cbs.web.service.packet.TaskQuery;
import cbs.web.service.packet.databean.MetaDataBean;
import cbs.web.service.packet.databean.TaskDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestTaskQueryQuery extends AbstractTestTaskQuery {
	
	/** DB Data */
	private final String DATA_FILE = "TestTaskQueryQuery.xml";

	public TestTaskQueryQuery() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_task",
				"cbs_pjuser",
				"cbs_project_resource",
				"cbs_project",
				"cbs_resource"
		};
	}

	/**
	 * 全件取得テスト
	 * @throws Exception
	 */
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 0);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_SUCCESS));
		
		List<TaskDataBean> data = result.getTask();
		assertThat(data.size(), is(1));
		
		TaskDataBean bean = data.get(0);
		assertThat(bean.getId(), is(10000));
		assertThat(bean.getTaskName(), is("タスク10000"));
		assertThat(bean.getResId(), is(7000));
		assertThat(bean.getResName(), is("リソース600"));
		
		List<MetaDataBean> listColumn = bean.getColumns();
		assertThat(listColumn.size(), is(0));		// TODO
	}
	
	@Test
	public void Normal_Success_0CountTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 11000);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_SUCCESS));
		
		List<TaskDataBean> data = result.getTask();
		assertThat(data.size(), is(0));
		
	}
	
	@Test
	public void Normal_Success_CountLimitTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 10000);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		req.setQueryCount(2);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_SUCCESS));
		
		List<TaskDataBean> data = result.getTask();
		assertThat(data.size(), is(2));
		
		// タスク1件目
		TaskDataBean bean = data.get(0);
		assertThat(bean.getId(), is(11000));
		assertThat(bean.getTaskName(), is("サブタスク11000"));
		assertThat(bean.getResId(), is(7000));
		
		List<MetaDataBean> listColumn = bean.getColumns();
		assertThat(listColumn.size(), is(0));		// TODO
		
		// タスク2件目
		bean = data.get(1);
		assertThat(bean.getId(), is(12000));
		assertThat(bean.getTaskName(), is("サブタスク12000"));
		assertThat(bean.getResId(), is(7000));
		
		listColumn = bean.getColumns();
		assertThat(listColumn.size(), is(0));		// TODO
	}
	
	@Test
	public void Normal_Success_CountLimit_0CountTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 11000);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		req.setQueryCount(2);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_SUCCESS));
		
		List<TaskDataBean> data = result.getTask();
		assertThat(data.size(), is(0));
		
	}
	
	@Test
	public void Normal_Success_CountLimitStartPosTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 10000);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(1);
		req.setQueryCount(2);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_SUCCESS));
		
		List<TaskDataBean> data = result.getTask();
		assertThat(data.size(), is(2));
		
		// タスク1件目
		TaskDataBean bean = data.get(0);
		assertThat(bean.getId(), is(12000));
		assertThat(bean.getTaskName(), is("サブタスク12000"));
		assertThat(bean.getResId(), is(7000));
		
		List<MetaDataBean> listColumn = bean.getColumns();
		assertThat(listColumn.size(), is(0));		// TODO
		
		// タスク2件目
		bean = data.get(1);
		assertThat(bean.getId(), is(13000));
		assertThat(bean.getTaskName(), is("サブタスク13000"));
		assertThat(bean.getResId(), is(7000));
		
		listColumn = bean.getColumns();
		assertThat(listColumn.size(), is(0));		// TODO
		
	}
	
	@Test
	public void Failure_InvalidProjectIdTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(51, 0);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_INVALID_PROJECT_ID));
		
	}
	
	@Test
	public void Failure_INvalidTaskIdTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 1);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_INVALID_TASK_ID));
		
	}
	
	@Test
	public void Failure_StartPosErrorTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 0);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(-1);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_INVALID_START_POS));
		
	}
	
	@Test
	public void Failure_GetCountErrorTest() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 0);
		req.setSessionId(getAuthInfo().getSessionId());
		req.setQueryStart(0);
		req.setQueryCount(-1);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_INVALID_COUNT));
		
	}
	
	@Test
	public void Failure_InvalidSessionId() throws Exception {
		// Packet
		TaskQuery req = new TaskQuery(50, 0);
		req.setSessionId("session");
		req.setQueryStart(0);
		
		// Exec
		TaskQuery result = super.queryPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_SESSID_ERROR));
		
	}
	
//	@Test
//	public void Failure_LackOfProjectIDTest() throws Exception {
//		// Packet
//		TaskQuery req = new TaskQuery();
//		req.setSessionId(getAuthInfo().getSessionId());
//		req.setQueryStart(0);
//		
//		// Exec
//		TaskQuery result = super.queryPost(req);
//		
//		// Assert
//		assertThat(result, is(notNullValue()));
//		assertThat(result.getStatus(), is(TaskQuery.CBS_WS_STAT_INVALID_PACKET));
//		
//	}

}
