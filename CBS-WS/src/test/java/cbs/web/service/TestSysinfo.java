package cbs.web.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.junit.Test;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.SysinfoAction;

public class TestSysinfo extends CbsWsTestBase {
	public TestSysinfo() throws Exception {
		super();
	}
	
	@Override
	protected Application configure() {
		return new ResourceConfig(SysinfoAction.class);		// 対象クラス
	}
	
	@Test
	public void getTest() {
		String response = target("Sysinfo").request().get(String.class);
		assertThat(response, containsString("API Version=0.1.0"));	// TODO 定数
	}

	@Override
	protected String getDataFile() {
		return null;
	}

}
