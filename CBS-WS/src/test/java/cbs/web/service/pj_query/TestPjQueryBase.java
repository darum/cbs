package cbs.web.service.pj_query;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
//import org.glassfish.jersey.test.TestProperties;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.PjQueryAction;
import cbs.web.service.packet.ProjQuery;

public abstract class TestPjQueryBase extends CbsWsTestBase {

	public TestPjQueryBase() throws Exception {
		super();
	}
	
	/* I/F */
	protected final String QUERY_PATH = "/pj_query/queryAll";
	
	@Override
	protected Application configure() {
		// for DEBUG
//        enable(TestProperties.LOG_TRAFFIC);
//        enable(TestProperties.DUMP_ENTITY);
		
		return new ResourceConfig(PjQueryAction.class);
	}
	
	/* API Call Method */
	protected ProjQuery queryAllPost(final ProjQuery req) throws Exception {
		return sendPost(req, QUERY_PATH).readEntity(ProjQuery.class);
	}

}
