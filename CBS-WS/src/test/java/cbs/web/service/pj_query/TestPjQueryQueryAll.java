package cbs.web.service.pj_query;

import java.util.List;

import org.junit.Test;

import cbs.web.service.packet.ProjQuery;
import cbs.web.service.packet.SysUserAuth;
import cbs.web.service.packet.databean.ProjDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestPjQueryQueryAll extends TestPjQueryBase {
	
	/* テスト DBデータ */
	private final String DATA_FILE = "TestPjQueryQueryAll.xml";

	public TestPjQueryQueryAll() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_pjuser",
				"cbs_project"
		};
	}

	/**
	 * 1件取得のテスト
	 * @throws Exception
	 */
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		ProjQuery req = new ProjQuery(100);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjQuery.CBS_WS_STAT_SUCCESS));
		
		List<ProjDataBean> data = result.getResult();
		assertThat(data.size(), is(1));
		
		ProjDataBean bean = data.get(0);
		assertThat(bean.getProjId(), is(50));
		assertThat(bean.getName(), is("Project50"));
		assertThat(bean.getPermission(), is(1));	// TODO 権限
		assertThat(bean.getUserId(), is(4000));
		assertThat(bean.getOwnerId(), is(100));
		assertThat(bean.getOwnerAccount(), is("account"));
	}
	
	@Test
	public void Normal_Success_0CountTest() throws Exception {
		// Login
		SysUserAuth login = super.loginExec("account102", "password");
		
		// Packet
		ProjQuery req = new ProjQuery(102);
		req.setSessionId(login.getSessionId());
		
		// Exec
		ProjQuery resp = queryAllPost(req);
		
		// Assert
		assertThat(resp, is(notNullValue()));
		assertThat(resp.getStatus(), is(ProjQuery.CBS_WS_STAT_SUCCESS));
		assertThat(resp.getResult().size(), is(0));
	}

	@Test
	public void Fail_InvalidUserIdTest() throws Exception {
		// Packet
		ProjQuery req = new ProjQuery(101);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjQuery.CBS_WS_STAT_INVALID_USERID));
	}

	@Test
	public void Fail_SessionErrorTest() throws Exception {
		// Packet
		ProjQuery req = new ProjQuery(101);
		req.setSessionId("a");
		
		// Exec
		ProjQuery result = queryAllPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjQuery.CBS_WS_STAT_SESSID_ERROR));
	}
}
