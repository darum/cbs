package cbs.web.service.taskCol_ctrl;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.MetaControl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestTaskColCtrlModify extends AbstractTestTaskColCtrl {
	
	private static final String DATA_FILE = "TestTaskColCtrlModify.xml";

	public TestTaskColCtrlModify() throws Exception {
		super();
	}

	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}

	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[]{
				"cbs_task_col_def",
				"cbs_pjuser",
				"cbs_project"
		};
	}
	
	/* Test */
//	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setMetaId(2000);
		req.setColumn("ColumnUpdate");
		req.setDataType("2");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_SUCCESS));
		
		// DB
		ITable table = getTableData("cbs_task_col_def");
		assertThat((String)table.getValue(0, "col_name"), startsWith("ColumnUpdate"));
		assertThat((Integer)table.getValue(0, "item_datatype"), is(2));
		assertThat((Integer)table.getValue(0, "update_number"), is(2));
	}

	// TODO
//	@Test
	public void Fail_InvalidColumnIdTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setMetaId(4000);
		req.setColumn("ColumnUpdate");
		req.setDataType("2");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_INVALID_METAID));
	}

	// TODO
//	@Test
	public void Fail_MultipleColumnNameTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setMetaId(2000);
		req.setColumn("Column3");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_MULTIPLE_NAME));
		
		// DB
		ITable table = getTableData("cbs_task_col_def");
		assertThat((Integer)table.getValue(0, "update_number"), is(1));		// 更新されていない
	}

	// TODO
//	@Test
	public void Fail_SessionIdErrorTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setMetaId(2000);
		req.setColumn("ColumnUpdate");
		req.setDataType("2");
		req.setSessionId("a");
		
		// Exec
		MetaControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_SESSID_ERROR));
		
		// DB
		ITable table = getTableData("cbs_task_col_def");
		assertThat((Integer)table.getValue(0, "update_number"), is(1));
	}
	
	// TODO
//	@Test
	public void Fail_LackOfColumnIdTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setColumn("ColumnUpdate");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_INVALID_PACKET));
		
	}

	// TODO
//	@Test
	public void Fail_LackOfModifyContentsTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setMetaId(2000);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_INVALID_PACKET));
		
		// DB
		ITable table = getTableData("cbs_task_col_def");
		assertThat((Integer)table.getValue(0, "update_number"), is(1));		// 更新されていない
	}

}
