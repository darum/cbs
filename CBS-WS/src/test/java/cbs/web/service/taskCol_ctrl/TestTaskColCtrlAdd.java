package cbs.web.service.taskCol_ctrl;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.MetaControl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestTaskColCtrlAdd extends AbstractTestTaskColCtrl {
	
	private static final String DATA_FILE = "TestTaskColCtrlAdd.xml";

	public TestTaskColCtrlAdd() throws Exception {
		super();
	}

	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[]{
				"cbs_task_col_def",
				"cbs_pjuser",
				"cbs_project"
		};
	}
	
	/* Test */
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setPjId(50);
		req.setColumn("Column1");
		req.setDataType("1");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_SUCCESS));
		assertThat(result.getMetaId(), greaterThan(1));
		
		// DB
		ITable table = getTableData("cbs_task_col_def");
		assertThat((Integer)table.getValue(1, "project_id"), is(50));
		assertThat((String)table.getValue(1, "col_name"), is("Column1"));
		assertThat((String)table.getValue(1, "item_datatype"), is("1"));
		assertThat((Integer)table.getValue(1, "update_number"), is(1));
	}

	@Test
	public void Fail_InvalidPjIdError() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setPjId(60);
		req.setColumn("Column1");
		req.setDataType("1");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_INVALID_PJID));
	}
	
	@Test
	public void Fail_MultipleColumnNameTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setPjId(51);
		req.setColumn("Column2");
		req.setDataType("1");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_MULTIPLE_NAME));
		
	}

	@Test
	public void Fail_SessionError() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setPjId(50);
		req.setColumn("Column1");
		req.setDataType("1");
		req.setSessionId("a");
		
		// Exec
		MetaControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_SESSID_ERROR));
	}

	@Test
	public void Fail_LackOfColNameTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setPjId(50);
		req.setDataType("1");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_INVALID_PACKET));
	}

	@Test
	public void Fail_LackOfDatatypeTest() throws Exception {
		// Packet
		MetaControl req = new MetaControl();
		req.setPjId(50);
		req.setColumn("ColumnUpdate");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		MetaControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(MetaControl.CBS_WS_STAT_INVALID_PACKET));
	}
}
