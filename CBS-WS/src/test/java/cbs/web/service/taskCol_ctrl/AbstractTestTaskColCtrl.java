package cbs.web.service.taskCol_ctrl;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.TaskColCtrlAction;
import cbs.web.service.packet.MetaControl;

public abstract class AbstractTestTaskColCtrl extends CbsWsTestBase {

	/**
	 * コンストラクタ
	 * @throws Exception
	 */
	public AbstractTestTaskColCtrl() throws Exception {
		super();
	}

	/* Test用 I/F */
	private static final String ADD_PATH = "/taskCol_ctrl/add";
	private static final String MODIFY_PATH = "/taskCol_ctrl/modify";
	private static final String DEL_PATH = "/taskCol_ctrl/del";
	
	@Override
	protected Application configure() {
		return new ResourceConfig(TaskColCtrlAction.class);
	}
	
	/* API Call用メソッド */
	protected MetaControl addPost(final MetaControl req) throws Exception {
		return sendPost(req, ADD_PATH).readEntity(MetaControl.class);
	}

	protected MetaControl modifyPost(final MetaControl req) throws Exception {
		return sendPost(req, MODIFY_PATH).readEntity(MetaControl.class);
	}

	protected MetaControl delPost(final MetaControl req) throws Exception {
		return sendPost(req, DEL_PATH).readEntity(MetaControl.class);
	}
}
