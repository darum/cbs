package cbs.web.service.auth;

import org.junit.Test;

import cbs.web.service.packet.SysUserAuth;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class TestAuthLogout extends TestAuthBase {
	
	public TestAuthLogout() throws Exception {
		super();
	}
	/** テスト対象のPATH */
	private final String TARGET_PATH = LOGOUT_PATH;
	/** データファイル(XML) */
	private final String DATA_FILE = "TestAuthLogout.xml";

	/**
	 * データファイル名の指定
	 * @Return データファイル名
	 */
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}

	private SysUserAuth authPost(final SysUserAuth req) throws Exception {
		return sendPost(req, TARGET_PATH);
	}

	@Test
	public void Normal_LogoutSuccessTest() throws Exception {
		// Pre-requisite
		// Packet
		SysUserAuth loginResp = super.loginExec("account", "password");
		assertThat(loginResp, is(notNullValue()));
		
		// Test Packet
		SysUserAuth logout = new SysUserAuth(loginResp.getUserId());
		logout.setSessionId(loginResp.getSessionId());
		
		// 実行
		SysUserAuth result = authPost(logout);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_SUCCESS));
		
		// 再実行してもエラーになる
		result = authPost(logout);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_SESSID_ERROR));
	}
	
	@Test
	public void Normal_LogoutSessionErrorTest() throws Exception {
		SysUserAuth req = new SysUserAuth(0);
		req.setSessionId("000");
		req.setUserId(1);
		
		// 実行
		SysUserAuth result = authPost(req);
		
		// 結果
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_SESSID_ERROR));
	}
	
	@Test
	public void Fail_InvalidPacketTest() throws Exception {
		SysUserAuth loginResp = super.loginExec("account", "password");
		assertThat(loginResp, is(notNullValue()));
		// Packet
		SysUserAuth req = new SysUserAuth();
		req.setSessionId(loginResp.getSessionId());
		
		// Exec
		SysUserAuth result = authPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_INVALID_PACKET));
	}
}
