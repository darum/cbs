package cbs.web.service.auth;

import org.junit.Test;

import cbs.web.service.packet.SysUserAuth;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class TestAuthLogin extends TestAuthBase {
	
	/** テスト対象のPATH */
	private final String TARGET_PATH = LOGIN_PATH;
	/** データファイル(XML) */
	private final String DATA_FILE = "TestAuthLogin.xml";
	
	public TestAuthLogin() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	/**
	 * テスト対象PATHの実行
	 * @param req Requestパケット
	 * @return Responseパケット
	 * @throws Exception エラー
	 */
	private SysUserAuth authPost(final SysUserAuth req) throws Exception {
		return sendPost(req, TARGET_PATH);
	}
	
	@Test
	public void Normal_LoginSuccessTest() throws Exception {
		// Test Packet
		SysUserAuth req = new SysUserAuth("account", "password");
		
		// 実行
		SysUserAuth result = authPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_SUCCESS));
		assertThat(result.getSessionId(), is(notNullValue()));
		assertThat(result.getUserId(), is(greaterThan(0)));
	}
	
	@Test
	public void Fail_LoginErrorTest() throws Exception {
		// Test Packet
		SysUserAuth req = new SysUserAuth("account", "password0");
		
		// 実行
		SysUserAuth result = authPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_AUTH_ERROR));
		
	}

	@Test
	public void Fail_NoUserErrorTest() throws Exception {
		// Test Packet
		SysUserAuth req = new SysUserAuth("accounT", "password");
		
		// 実行
		SysUserAuth result = authPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_AUTH_ERROR));
		
	}

	@Test
	public void Fail_LackOfAccountParameterTest() throws Exception {
		// Test Packet
		SysUserAuth req = new SysUserAuth("", "password");
		
		// 実行
		SysUserAuth result = authPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_INVALID_PACKET));
	}

	@Test
	public void Fail_LackOfPasswordParameterTest() throws Exception {
		// Test Packet
		SysUserAuth req = new SysUserAuth("account", "");
		
		// 実行
		SysUserAuth result = authPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserAuth.CBS_WS_STAT_INVALID_PACKET));
	}

}
