package cbs.web.service.auth;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.AuthAction;
import cbs.web.service.packet.SysUserAuth;

public abstract class TestAuthBase extends CbsWsTestBase {
	public TestAuthBase() throws Exception {
		super();
	}

	protected final String BASE_PATH = "/auth";
	
	protected final String LOGIN_PATH = BASE_PATH + "/login";
	protected final String LOGOUT_PATH = BASE_PATH + "/logout";
	
	protected Application configure() {
		return new ResourceConfig(AuthAction.class);		// 対象クラス
	}
	
	protected SysUserAuth sendPost(final SysUserAuth req, String path) {
		SysUserAuth result = target(path).request()
				.post(Entity.entity(req,  MediaType.APPLICATION_JSON), SysUserAuth.class);
		
		return result;
	}
}
