package cbs.web.service.pjuser_ctrl;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.PjUserCtrlAction;
import cbs.web.service.packet.ProjUserControl;

public class TestPjUserCtrlBase extends CbsWsTestBase {

	private static String ADD_PATH = "/pjuser_ctrl/add";
	private static String MODIFY_PATH = "/pjuser_ctrl/modify";
	private static String DEL_PATH = "/pjuser_ctrl/del";
	
	public TestPjUserCtrlBase() throws Exception {
		super();
	}
	
	@Override
	protected Application configure() {
		return new ResourceConfig(PjUserCtrlAction.class);
	}
	
	protected ProjUserControl addPost(final ProjUserControl req) throws Exception {
		return sendPost(req, ADD_PATH).readEntity(ProjUserControl.class);
	}

	protected ProjUserControl modifyPost(final ProjUserControl req) throws Exception {
		return sendPost(req, MODIFY_PATH).readEntity(ProjUserControl.class);
	}
	
	protected ProjUserControl delPost(final ProjUserControl req) throws Exception {
		return sendPost(req, DEL_PATH).readEntity(ProjUserControl.class);
	}
}
