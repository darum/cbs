package cbs.web.service.pjuser_ctrl;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.ProjUserControl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestPjUserCtrlAdd extends TestPjUserCtrlBase {
	
	private static String DATA_FILE = "TestPjUserControlAdd.xml";

	public TestPjUserCtrlAdd() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[]{
				"cbs_pjuser",
				"cbs_project"
		};
	}
	
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(10, 200);
		req.setPermission(1);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjUserControl result = addPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_SUCCESS));
		
		// DB Check
		ITable acctualTable = super.getTableData("cbs_pjuser");
		assertThat((Integer)acctualTable.getValue(0, "project_id"), is(200));
		assertThat((Integer)acctualTable.getValue(0, "login_user_id"), is(10));
		assertThat((Integer)acctualTable.getValue(0, "permission"), is(1));
	}
	
	@Test
	public void Fail_NoPjPermissionTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(10, 201);
		req.setPermission(2);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjUserControl result = addPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_INVALID_PROJ_ID));
	}
	
	@Test
	public void Fail_InvalidUserIdTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(11, 200);
		req.setPermission(1);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjUserControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_INVALID_SYSUSER));
		
	}

	@Test
	public void Fail_InvalidProjIdTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(10, 210);
		req.setPermission(1);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjUserControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_INVALID_PROJ_ID));
		
	}

	@Test
	public void Fail_SessionErrorTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(10, 200);
		req.setPermission(1);
		req.setSessionId("a");
		
		// Execute
		ProjUserControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_SESSID_ERROR));
	}

	@Test
	public void Fail_LackOfPermissionTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(10, 200);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjUserControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_INVALID_PACKET));
	}
}
