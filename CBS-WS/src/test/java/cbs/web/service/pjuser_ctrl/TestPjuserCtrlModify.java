package cbs.web.service.pjuser_ctrl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.ProjUserControl;

public class TestPjuserCtrlModify extends TestPjUserCtrlBase {

	private static String DATA_FILE = "TestPjUserControlModify.xml";
	
	public TestPjuserCtrlModify() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_pjuser",
				"cbs_project"
		};
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}

	/* Test */
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(3000);
		req.setPermission(5);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjUserControl result = modifyPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_SUCCESS));
		
		// DB
		ITable acctualTable = getTableData("cbs_pjuser");
		assertThat((Integer)acctualTable.getValue(0, "permission"), is(5));
		assertThat((Integer)acctualTable.getValue(0, "update_number"), is(2));
	}

	@Test
	public void Fail_InvalidPjUserIDTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(4000);
		req.setPermission(5);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjUserControl result = modifyPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_INVALID_PJUSER_ID));
		
		// DB
		ITable acctualTable = getTableData("cbs_pjuser");
		assertThat((Integer)acctualTable.getValue(0, "update_number"), is(1));
	}

	@Test
	public void Fail_SessionErrorTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(3000);
		req.setPermission(5);
		req.setSessionId("aaa");
		
		// Exec
		ProjUserControl result = modifyPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_SESSID_ERROR));
	}

	@Test
	public void Fail_LackOfPermissionTest() throws Exception {
		// Packet
		ProjUserControl req = new ProjUserControl(3000);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjUserControl result = modifyPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjUserControl.CBS_WS_STAT_INVALID_PACKET));
		
		// DB
		assertThat((Integer)super.getTableData("cbs_pjuser").getValue(0, "update_number"), is(1));
	}

}
