package cbs.web.service.pj_ctrl;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.ProjControl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestPjCtrlModify extends TestPjCtrlBase {
	public TestPjCtrlModify() throws Exception {
		super();
	}
	
	private final String DATA_FILE = "TestPjCtrlModify.xml";
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	/**
	 * 
	 */
	@Override
	protected String[] getDelTableName() {
		String[] tables = new String[]{
				"cbs_project"
		};
		
		return tables;
	}
	
	@Test
	public void Normal_SuccessTest() throws Exception {
		// packet
		ProjControl req = new ProjControl(50);
		req.setName("New Project");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_SUCCESS));
		
		// DB Check
		IDatabaseConnection connection = base.getDatabaseTester().getConnection();
		ITable acctualTable = connection.createDataSet().getTable("cbs_project");
		assertThat((String)acctualTable.getValue(0, "name"), startsWith("New Project"));
		assertThat((Integer)acctualTable.getValue(0, "update_number"), is(2));
		
	}
	
	@Test
	public void Fail_MultipleProjNameTest() throws Exception {
		// packet
		ProjControl req = new ProjControl(50);
		req.setName("Project02");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_MULTIPLE_NAME));
		
		// DB Check
		IDatabaseConnection connection = base.getDatabaseTester().getConnection();
		ITable acctualTable = connection.createDataSet().getTable("cbs_project");
		assertThat((String)acctualTable.getValue(0, "name"), startsWith("Project"));
		assertThat((Integer)acctualTable.getValue(0, "update_number"), is(1));

	}
	
	/**
	 * プロジェクトIDエラーのテスト（存在しないID）
	 * @throws Exception
	 */
	@Test
	public void Fail_ProjIdInvalidError() throws Exception {
		// packet
		ProjControl req = new ProjControl(40);
		req.setName("New Project");
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Execute
		ProjControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_PROJID_ERROR));
		
	}
	
	@Test
	public void Fail_SessionErrorTest() throws Exception {
		// packet
		ProjControl req = new ProjControl(50);
		req.setName("Project02");
		req.setSessionId("a");
		
		// Execute
		ProjControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_SESSID_ERROR));
	}

	@Test
	public void Fail_LackOfProjIdTest() throws Exception {
		// packet
		ProjControl req = new ProjControl();
		req.setName("New Project");
		req.setSessionId(getAuthInfo().getSessionId());

		// Execute
		ProjControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_INVALID_PACKET));
		
	}
	
	@Test
	public void Fail_InvalidProjNameTest() throws Exception {
		// packet
		ProjControl req = new ProjControl(50);
		req.setSessionId(getAuthInfo().getSessionId());

		// Execute
		ProjControl result = modifyPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_INVALID_PACKET));
	}
}
