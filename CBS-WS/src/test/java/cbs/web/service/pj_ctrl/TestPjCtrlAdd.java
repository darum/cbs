package cbs.web.service.pj_ctrl;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.ProjControl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestPjCtrlAdd extends TestPjCtrlBase {
	
	private final String DATA_FILE = "TestPjCtrlAdd.xml";
	
	private String sessionId = "test_session";	// XMLで固定
	public TestPjCtrlAdd() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[]{
				"cbs_project"
		};
	}

	@Test
	public void Normal_AddSuccessTest() throws Exception {

		ProjControl req = new ProjControl("プロジェクト");
		req.setSessionId(sessionId);
		
		ProjControl result = addPost(req);
		
		assertThat(result, is(notNullValue()));
		assertThat(result.getSessionId(), equalTo(sessionId));
		assertThat(result.getProjId(), is(greaterThan(0)));
		
		// DB格納値の検証
		ITable acctualTable = getTableData("cbs_project");
		assertThat((Integer)acctualTable.getValue(0, "login_user_id"), is(1));
		assertThat((String)acctualTable.getValue(0, "name"), startsWith("プロジェクト"));
	}
	
	@Test
	public void Failure_InvalidPacketTest() throws Exception {
		ProjControl req = new ProjControl();
		req.setSessionId(sessionId);
		
		ProjControl result = addPost(req);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_INVALID_PACKET));
	}
	
	/**
	 * 同一プロジェクト名のテスト
	 * @throws Exception
	 */
	@Test
	public void Failure_MultiplenameTest() throws Exception {
		ProjControl req = new ProjControl("プロジェクト1");
		req.setSessionId(sessionId);
		
		ProjControl result = addPost(req);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_SUCCESS));
		
		result = addPost(req);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_MULTIPLE_NAME));
	}
	
	@Test
	public void Fail_SessionErrorTest() throws Exception {
		ProjControl req = new ProjControl("プロジェクト2");
		req.setSessionId("a");
		
		// Exec
		ProjControl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjControl.CBS_WS_STAT_SESSID_ERROR));
	}
}
