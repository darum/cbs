package cbs.web.service.pj_ctrl;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.PjCtrlAction;
import cbs.web.service.packet.ProjControl;

public abstract class TestPjCtrlBase extends CbsWsTestBase {
	
	protected final String ADD_PATH = "/pj_ctrl/add";
	protected final String MODIFY_PATH = "/pj_ctrl/modify";
	protected final String DEL_PATH = "/pj_ctrl/delete";
	
	protected Application configure() {
		return new ResourceConfig(PjCtrlAction.class);
	}

	public TestPjCtrlBase() throws Exception {
		super();
	}
	
	protected ProjControl addPost(final ProjControl req) throws Exception {
		Response resp = sendPost(req, ADD_PATH);
		return resp.readEntity(ProjControl.class);
	}
	
	protected ProjControl modifyPost(final ProjControl req) throws Exception {
		return sendPost(req, MODIFY_PATH).readEntity(ProjControl.class);
	}
	
	protected ProjControl deletePost (final ProjControl req) throws Exception {
		return sendPost(req, DEL_PATH).readEntity(ProjControl.class);
	}
	
}
