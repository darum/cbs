package cbs.web.service.user;

import org.junit.Rule;
import org.junit.Test;

import cbs.test.CbsDbUnitBase;
import cbs.web.service.packet.SysUserControl;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class TestUserDelete extends TestUserBase {
	private final String DATA_FILE = "TestUserDelete.xml";
	
	// - DbUnitによるDB内容初期化
	@Rule
	public CbsDbUnitBase base;
	public TestUserDelete() throws Exception {
		base = new CbsDbUnitBase(DATA_FILE);
	}
	
	@Test
	public void 正常系_ユーザ削除Test() throws Exception {
		// 前提：ユーザ登録
		SysUserControl req = new SysUserControl("delAcc", "pass", "mail");
		
		SysUserControl addResp = addPost(req);
		assertThat(addResp, is(notNullValue()));
		int targetId = addResp.getUserId();	// ID
		
		// テスト実行
		req = new SysUserControl(targetId);
		SysUserControl delResp = delPost(req);
		
		// Assert
		assertThat(delResp.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
	}

	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}

}
