package cbs.web.service.user;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.UserAction;
import cbs.web.service.packet.SysUserControl;

abstract class TestUserBase extends CbsWsTestBase {
	public TestUserBase() throws Exception {
		super();
	}

	protected final String ADD_PATH = "/user/add";
	protected final String MODIFY_PATH = "/user/modify";
	protected final String DELETE_PATH = "/user/delete";
	
	@Override
	protected Application configure() {
		return new ResourceConfig(UserAction.class);		// 対象クラス
	}
	
	/* API Call用メソッド */
	protected SysUserControl addPost(final SysUserControl req) throws Exception {
		return sendPost(req, ADD_PATH).readEntity(SysUserControl.class);
	}
	
	protected SysUserControl modifyPost(final SysUserControl req) throws Exception {
		Response resp = sendPost(req, MODIFY_PATH);
		return resp.readEntity(SysUserControl.class);
	}

	protected SysUserControl delPost(final SysUserControl req) throws Exception {
		return sendPost(req, DELETE_PATH).readEntity(SysUserControl.class);
	}
	
}
