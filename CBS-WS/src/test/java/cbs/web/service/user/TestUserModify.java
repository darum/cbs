package cbs.web.service.user;

import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.ITable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cbs.web.service.packet.SysUserAuth;
import cbs.web.service.packet.SysUserControl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestUserModify extends TestUserBase {
	
	public TestUserModify() throws Exception {
		super();
	}

	private final String DATA_FILE = "TestUserModify.xml";
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
	}
	
	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void Normal_AccountModifyTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setAccount("new_account");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
		
		// DBチェック
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "login_account"), startsWith("new_account"));
		assertThat((Integer)table.getValue(0, "update_number"), is(2));
	}
	
	@Test
	public void Normal_PasswordModifyTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setPassword("new_pass");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
		
		// DBチェック
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "login_passwd"), startsWith("new_pass"));
		assertThat((Integer)table.getValue(0, "update_number"), is(2));
	}
	
	@Test
	public void Normal_EmailModifyTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setEmail("new_mail");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
		
		// DBチェック
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "login_user_email"), startsWith("new_mail"));
		assertThat((Integer)table.getValue(0, "update_number"), is(2));
	}
	
	@Test
	public void 正常系_FirstName変更Test() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setFirstName("FirstName");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
		
		// DBチェック
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "user_first_name"), startsWith("FirstName"));
		assertThat((Integer)table.getValue(0, "update_number"), is(2));
	}

	@Test
	public void Normal_LastNameModifyTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setLastName("LastName");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
		
		// DBチェック
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "user_last_name"), startsWith("LastName"));
		assertThat((Integer)table.getValue(0, "update_number"), is(2));
	}
	
	@Test
	public void Fail_MultipleAccountTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setAccount("account2");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_ACCOUNT_DUPLICATED));
		
		// DBチェック: 更新されていないこと
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "login_account"), startsWith("account"));
		assertThat((Integer)table.getValue(0, "update_number"), is(1));
		
	}
	
	@Test
	public void Fail_IDErrorTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(0);
		req.setSessionId(authInfo.getSessionId());
		// 編集内容
		req.setAccount("new_account");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_START_USERID_INVALID));
		
		// DBチェック: 更新されていないこと
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "login_account"), startsWith("account"));
		assertThat((Integer)table.getValue(0, "update_number"), is(1));
	}
	
	@Test
	public void Fail_SessionErrorTest() throws Exception {
		SysUserAuth authInfo = getAuthInfo();
		// JSON Request
		SysUserControl req = new SysUserControl(authInfo.getUserId());
		req.setSessionId("a");
		// 編集内容
		req.setAccount("new_account");
		
		// 実行
		SysUserControl resp = super.modifyPost(req);
		assertThat(resp.getStatus(), is(SysUserControl.CBS_WS_STAT_SESSID_ERROR));
		
		// DBチェック: 更新されていないこと
		IDatabaseConnection conn = base.getDatabaseTester().getConnection();
		ITable table = conn.createDataSet().getTable("cbs_sysuser");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(authInfo.getUserId()));
		assertThat((String)table.getValue(0, "login_account"), startsWith("account"));
		assertThat((Integer)table.getValue(0, "update_number"), is(1));
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
}
