package cbs.web.service.user;

import org.junit.Rule;
import org.junit.Test;

import cbs.test.CbsDbUnitBase;
import cbs.web.service.packet.SysUserControl;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class TestUserAdd extends TestUserBase {
	
	private final String DATA_FILE = "TestUserAdd.xml";
	
	// - DbUnitによるDB内容初期化
	@Rule
	public CbsDbUnitBase base; 
	
	public TestUserAdd() throws Exception {
		base = new CbsDbUnitBase(DATA_FILE);
	}
	
	@Test
	public void addTest() throws Exception {
		
		// - JSON Request パケットの生成
		SysUserControl req = new SysUserControl("account", "password", "email");
		
		// - Test実行
		// #1は、正常に終了していればよい
		SysUserControl result = addPost(req);
		
		SysUserControl req2 = new SysUserControl("account2", "password", "email");
		SysUserControl result2 = addPost(req2);

		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result2.getStatus(), is(SysUserControl.CBS_WS_STAT_SUCCESS));
		assertThat(result2.getUserId(), is(result.getUserId() + 1));
	}
	
	@Test
	public void 同一アカウント登録エラーtest() throws Exception {
		// - JSON Request パケットの生成
		SysUserControl req = new SysUserControl("cbs001", "password", "email");
		
		// - Test実行
		SysUserControl result = addPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserControl.CBS_WS_STAT_ACCOUNT_DUPLICATED));
	}
	
	@Test
	public void Requestパケット不足test() throws Exception {
		SysUserControl req, result;
		
		// -JSON Request パケットの生成：アカウント名なし
		req  = new SysUserControl("", "password", "email");
		
		// - Test実行
		result = addPost(req);
		
		// Assertion
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserControl.CBS_WS_STAT_INVALID_PACKET));
		
		// パスワードなし
		req = new SysUserControl("account3", "", "email");
		result = addPost(req);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserControl.CBS_WS_STAT_INVALID_PACKET));
		
		// メールアドレスなし
		req = new SysUserControl("account3", "pass", "");
		result = addPost(req);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(SysUserControl.CBS_WS_STAT_INVALID_PACKET));
	}

	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}

}
