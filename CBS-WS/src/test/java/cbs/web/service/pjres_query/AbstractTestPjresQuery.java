package cbs.web.service.pjres_query;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.PjresQueryAction;
import cbs.web.service.packet.ProjResQuery;

public class AbstractTestPjresQuery extends CbsWsTestBase {
	
	protected final String QUERY_PATH = "/pjres_query/query";

	public AbstractTestPjresQuery() throws Exception {
		super();
	}

	@Override
	protected Application configure() {
		return new ResourceConfig(PjresQueryAction.class);
	}
	
	/* API call */
	protected ProjResQuery queryPost(final ProjResQuery req) throws Exception {
		return sendPost(req, QUERY_PATH).readEntity(ProjResQuery.class);
	}
	
}
