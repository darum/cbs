package cbs.web.service.pjres_query;

import java.util.List;

import org.junit.Test;

import cbs.web.service.packet.ProjResQuery;
import cbs.web.service.packet.databean.PjresDataBean;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestPjresQueryQuery extends AbstractTestPjresQuery {

	public TestPjresQueryQuery() throws Exception {
		super();
	}

	private final String DATA_FILE = "TestPjresQueryQuery.xml";
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_project_resource",
				"cbs_pjuser",
				"cbs_project",
				"cbs_resource"
		};
	}

	@Test
	public void Normal_SuccessTest() throws Exception {
		/* Request */
		ProjResQuery req = new ProjResQuery(10);
		req.setSessionId(getAuthInfo().getSessionId());
		
		/* Exec */
		ProjResQuery result = queryPost(req);
		
		/* Assert */
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResQuery.CBS_WS_STAT_SUCCESS));
		List<PjresDataBean> beans = result.getList();
		assertThat(beans, is(notNullValue()));
		assertThat(beans.size(), is(greaterThan(0)));

		PjresDataBean bean = beans.get(0);
		assertThat(bean.getPjResId(), is(1000));
		assertThat(bean.getResId(), is(100));
		assertThat(bean.getResName(), is("Resource100"));
	}
	
	/**
	 * 0件取得のテスト
	 * @throws Exception
	 */
	@Test
	public void Normal_Success_Count0Test() throws Exception {
		/* Request */
		ProjResQuery req = new ProjResQuery(30);
		req.setSessionId(getAuthInfo().getSessionId());
		
		/* Exec */
		ProjResQuery result = queryPost(req);
		
		/* Assert */
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResQuery.CBS_WS_STAT_SUCCESS));
		List<PjresDataBean> beans = result.getList();
		assertThat(beans, is(notNullValue()));
		assertThat(beans.size(), is(0));
	}
	
	/**
	 * ProjectIDなしのテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_InvalidProjectIdTest() throws Exception {
		/* Request */
		ProjResQuery req = new ProjResQuery(11);
		req.setSessionId(getAuthInfo().getSessionId());
		
		/* Exec */
		ProjResQuery result = queryPost(req);
		
		/* Assert */
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResQuery.CBS_WS_STAT_INVALID_PJID));
	}
	
	/**
	 * セッションIDエラーのテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_SessionIDErrorTest() throws Exception {
		/* Request */
		ProjResQuery req = new ProjResQuery(10);
		req.setSessionId("SessionID");
		
		/* Exec */
		ProjResQuery result = queryPost(req);
		
		/* Assert */
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResQuery.CBS_WS_STAT_SESSID_ERROR));
	}
}
