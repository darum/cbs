package cbs.web.service.res_ctrl;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.packet.ResCtrl;

public class TestResCtrlBase extends CbsWsTestBase {
	
	private final String ADD_PATH = "/res_ctrl/add";
	private final String MODIFY_PATH = "/res_ctrl/add";
	private final String DEL_PATH = "/res_ctrl/del";

	public TestResCtrlBase() throws Exception {
		super();
	}
	
	@Override
	protected Application configure() {
		return new ResourceConfig(ResCtrl.class);
	}

	protected ResCtrl addPost(final ResCtrl req) throws Exception {
		return sendPost(req, ADD_PATH).readEntity(ResCtrl.class);
	}
	
	protected ResCtrl modifyPost(final ResCtrl req) throws Exception {
		return sendPost(req, MODIFY_PATH).readEntity(ResCtrl.class);
	}
	
	protected ResCtrl delPost(final ResCtrl req) throws Exception {
		return sendPost(req, DEL_PATH).readEntity(ResCtrl.class);
	}
}
