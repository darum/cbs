package cbs.web.service.res_ctrl;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.ResCtrl;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class TestResCtrlAdd extends TestResCtrlBase {
	
	private final String DATA_FILE = "TestResCtrlAdd.xml";

	public TestResCtrlAdd() throws Exception {
		super();
	}
	
	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_resource"
				, "cbs_pjuser"
				, "cbs_project"
				, "cbs_sysuser"
		};
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Test
	public void Normal_SuccessTest() throws Exception {
		// Packet
		int userId = super.getAuthInfo().getUserId();
		ResCtrl req = new ResCtrl(userId);
		req.setResName("リソース01");
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_SUCCESS));
		assertThat(result.getResId(), greaterThan(1));
		
		// DB
		ITable table = getTableData("cbs_resource");
		assertThat((Integer)table.getValue(0, "login_user_id"), is(userId));
		assertThat((String)table.getValue(0, "name"), is("リソース01"));
	}
	
	@Test
	public void Failure_InvalidUserIdTest() throws Exception {
		// Packet
		int userId = 1000;
		ResCtrl req = new ResCtrl(userId);
		req.setResName("リソース01");
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_INVALID_USER_ID));
	}
	
	@Test
	public void Failure_MultipleResName() throws Exception {
		ResCtrl packet, result;
		// Pre-requisite
		packet = new ResCtrl();
		packet.setUserId(super.getAuthInfo().getUserId());
		packet.setResName("リソース01");
		packet.setSessionId(getAuthInfo().getSessionId());
		result = addPost(packet);
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_SUCCESS));

		// Exec
		result = addPost(packet);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_MULTIPLE_RES_NAME));
		
	}
	
	@Test
	public void Failure_SessionIDErrorTest() throws Exception {
		// Packet
		int userId = super.getAuthInfo().getUserId();
		ResCtrl req = new ResCtrl(userId);
		req.setResName("リソース01");
		
		// Exec
		ResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_SESSID_ERROR));
		
	}
	
	@Test
	public void Failure_LackOfUserIdTest() throws Exception {
		// Packet
		int userId = super.getAuthInfo().getUserId();
		ResCtrl req = new ResCtrl();
		req.setResName("リソース01");
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_INVALID_PACKET));
	}
	
	@Test
	public void Failure_LackOfResourceNameTest() throws Exception {
		// Packet
		int userId = super.getAuthInfo().getUserId();
		ResCtrl req = new ResCtrl(userId);
		
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ResCtrl.CBS_WS_STAT_INVALID_PACKET));
	}
	

}
