package cbs.web.service.pjres_ctrl;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.dbunit.dataset.ITable;
import org.junit.Test;

import cbs.web.service.packet.ProjResCtrl;

public class TestPjresCtrlAdd extends TestPjresCtrlBase {
	
	private final String DATA_FILE = "TestResCtrlAdd.xml";

	public TestPjresCtrlAdd() throws Exception {
		super();
	}

	@Override
	protected String getDataFile() {
		return DATA_FILE;
	}
	
	@Override
	protected String getLoginAccount() {
		return "account";
	}
	
	@Override
	protected String getLoginPassword() {
		return "password";
	}
	
	@Override
	protected String[] getDelTableName() {
		return new String[] {
				"cbs_project_resource",
				"cbs_pjuser",
				"cbs_project",
				"cbs_resource"
		};
	}
	
	@Test
	public void Normal_AddSuccessTest() throws Exception {
		
		// Packet
		ProjResCtrl req = new ProjResCtrl(10, 1000);
		req.setSessionId(super.getAuthInfo().getSessionId());
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_SUCCESS));
		assertThat(result.getResId(), is(greaterThan(0)));
		
		// DB
		ITable table = super.getTableData("cbs_project_resource");
		assertThat((Integer)table.getValue(0, "project_id"), is(10));
		assertThat((Integer)table.getValue(0, "res_id"), is(1000));
		assertThat((Integer)table.getValue(0, "create_user_id"), is(getAuthInfo().getUserId()));
	}
	
	/**
	 * ProjectIDが存在しないテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_InvalidProjIdTest() throws Exception {
		// packet
		ProjResCtrl req = new ProjResCtrl(20, 1000);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_INVALID_PROJID));
	}
	
	/**
	 * Projectに権限がないテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_NotPermitProjectTest() throws Exception {
		// packet
		ProjResCtrl req = new ProjResCtrl(11, 1000);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_INVALID_PROJID));
	}
	
	/**
	 * リソースIDなしテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_NoResourceIdTest() throws Exception {
		// packet
		ProjResCtrl req = new ProjResCtrl(10, 1100);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_INVALID_RESID));
	}

	/**
	 * セッションエラーのテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_SessionErrorTest() throws Exception {
		
		// Packet
		ProjResCtrl req = new ProjResCtrl(10, 1000);
		req.setSessionId("a");
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_SESSID_ERROR));
	}	

	/**
	 * プロジェクトIDが設定されていないテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_LackOfProjIdTest() throws Exception {
		
		// Packet
		ProjResCtrl req = new ProjResCtrl();
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_INVALID_PACKET));
	}
	
	/**
	 * リソースIDが設定されていないテスト
	 * @throws Exception
	 */
	@Test
	public void Fail_LackOfResIDTest() throws Exception {
		
		// Packet
		ProjResCtrl req = new ProjResCtrl();
		req.setProjId(10);
		req.setSessionId(getAuthInfo().getSessionId());
		
		// Exec
		ProjResCtrl result = addPost(req);
		
		// Assert
		assertThat(result, is(notNullValue()));
		assertThat(result.getStatus(), is(ProjResCtrl.CBS_WS_STAT_INVALID_PACKET));
	}
}
