package cbs.web.service.pjres_ctrl;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;

import cbs.test.CbsWsTestBase;
import cbs.web.service.action.PjresCtrlAction;
import cbs.web.service.packet.ProjResCtrl;

public class TestPjresCtrlBase extends CbsWsTestBase {
	
	protected final String ADD_PATH = "/pjres_ctrl/add";
	protected final String MODIFY_PATH = "/pjres_ctrl/modify";
	protected final String DEL_PATH = "/pjres_ctrl/del";

	public TestPjresCtrlBase() throws Exception {
		super();
	}
	
	@Override
	protected Application configure() {
		return new ResourceConfig(PjresCtrlAction.class);
	}

	protected ProjResCtrl addPost(final ProjResCtrl req) throws Exception {
		return sendPost(req, ADD_PATH).readEntity(ProjResCtrl.class);
	}

	protected ProjResCtrl modifyPost(final ProjResCtrl req) throws Exception {
		return sendPost(req, MODIFY_PATH).readEntity(ProjResCtrl.class);
	}

	protected ProjResCtrl delPost(final ProjResCtrl req) throws Exception {
		return sendPost(req, DEL_PATH).readEntity(ProjResCtrl.class);
	}
}
