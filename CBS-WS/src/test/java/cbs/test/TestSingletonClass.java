package cbs.test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSourceFactory;
import cbs.web.service.CBSWebServiceSCListener;
import cbs.web.service.DBCPManager;

public class TestSingletonClass {

	private final String PROP_FILE = "/dbcp.properties";
	private static TestSingletonClass instance;
	
	private TestSingletonClass() throws Exception {
        // DBCP生成
		Properties properties = new Properties();
		InputStream is = CBSWebServiceSCListener.class.getResourceAsStream(PROP_FILE);
		if (is == null) {
			throw new FileNotFoundException(PROP_FILE);
		}
		properties.load(is);
		DataSource ds = BasicDataSourceFactory.createDataSource(properties);
		DBCPManager.setDataSource(ds);
	}

	public synchronized static TestSingletonClass getInstance() throws Exception {
		if (instance == null) {
			instance = new TestSingletonClass();
		}
		
		return instance;
	}
}
