package cbs.test;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.StatusType;

import org.dbunit.dataset.DefaultDataSet;
import org.dbunit.dataset.DefaultTable;
import org.dbunit.dataset.ITable;
import org.dbunit.operation.DatabaseOperation;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.grizzly2.servlet.GrizzlyWebContainerFactory;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.spi.TestContainer;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.glassfish.jersey.test.DeploymentContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.postgresql.util.PSQLException;
import org.postgresql.util.ServerErrorMessage;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.SysUserAuth;

public abstract class CbsWsTestBase extends JerseyTest {
	
	@Override
	protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
	    return new TestContainerFactory() {
	        @Override
	        public TestContainer create(final URI baseUri, DeploymentContext context) throws IllegalArgumentException {
	            return new TestContainer() {
	                private HttpServer server;

	                @Override
	                public ClientConfig getClientConfig() {
	                    return null;
	                }

	                @Override
	                public URI getBaseUri() {
	                    return baseUri;
	                }

	                @Override
	                public void start() {
	                    try {
	                        this.server = GrizzlyWebContainerFactory.create(
	                                baseUri, Collections.singletonMap("jersey.config.server.provider.packages", "cbs.web.service")
	                        );
	                        
		                    // DBCP
		                    TestSingletonClass.getInstance();
	                    } catch (ProcessingException e) {
	                        throw new TestContainerException(e);
	                    } catch (IOException e) {
	                        throw new TestContainerException(e);
	                    } catch (PSQLException e) {
	                    	ServerErrorMessage m = e.getServerErrorMessage();
	                    	System.out.println(m.getHint());
	                    	throw new TestContainerException(e);
	                    } catch (Exception e) {
	                    	throw new TestContainerException(e);
	                    }
	                    
	                }

	                @Override
	                public void stop() {
	                    this.server.shutdownNow();
	                }
	            };

	        }
	    };
	}
	
	/** Database Object */
	@Rule
	public CbsDbUnitBase base;
	public CbsWsTestBase() throws Exception {
		String dataFile = getDataFile();
		if (dataFile != null) {
			base = new CbsDbUnitBase(dataFile);
		}
		
	}
	/**
	 * DBへロードするデータファイル名を指定する
	 * @return
	 */
	protected String getDataFile() {
		return null;
	}
	/**
	 * テスト時にログインするアカウントを指定する
	 * @return
	 */
	protected String getLoginAccount() {
		return null;
	}
	/**
	 * テスト時にログインするアカウントのパスワードを指定する
	 * @return
	 */
	protected String getLoginPassword() {
		return null;
	}
	
	/**
	 * APIをコールする
	 * @param req パケット
	 * @param path APIのパス
	 * @return Responseオブジェクト readEntity()でパケットを取り出す。
	 */
	protected <T> Response sendPost(final T req, String path) {
		Response result = target(path).request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(req,  MediaType.APPLICATION_JSON));
		result.getStringHeaders().putSingle(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);

		StatusType status = result.getStatusInfo();
		if (status.getStatusCode() != HttpServletResponse.SC_OK) {
			// 200 OKでない
			throw new ProcessingException(String.valueOf(status.getStatusCode()) + " " + status.getReasonPhrase());
		}
		
		return result;
	}
	
	/**
	 * テスト結果判定用：DB値取得メソッド
	 * @param tableName テーブル名
	 * @return テーブルオブジェクト
	 * @throws Exception 致命的なエラー
	 */
	protected ITable getTableData(String tableName) throws Exception {
		ITable table = null;
		if (base != null) {
			table = base.getDatabaseTester().getConnection().createDataSet().getTable(tableName);
		}
		return table;
	}
	
	/**
	 * WSへログインする
	 * @param account アカウント名
	 * @param password パスワード
	 * @return 戻りパケット
	 * @throws Exception 認証エラー
	 */
	protected SysUserAuth loginExec(String account, String password) throws Exception {
		SysUserAuth req = new SysUserAuth(account, password);
		SysUserAuth resp = sendPost(req, "/auth/login").readEntity(SysUserAuth.class);
		if (resp.getStatus() != SysUserAuth.CBS_WS_STAT_SUCCESS) {
			throw new Exception("Auth Error");
		}
		
		return resp;
	}

	/**
	 * WSからのログアウト
	 * @param loginResp ログイン時のパケット
	 * @return ステータス
	 */
	protected int logoutExec(final SysUserAuth loginResp) {
		SysUserAuth req = new SysUserAuth(loginResp.getUserId());
		req.setSessionId(loginResp.getSessionId());
		SysUserAuth resp = sendPost(req, "/auth/logout").readEntity(SysUserAuth.class);
		
		return resp.getStatus();
	}
	
	/** ログイン情報(セッション情報) */
	private SysUserAuth authInfo = null;;
	@Before
	public void setUp() throws Exception {
		super.setUp();
		
		String account = getLoginAccount();
		if (account != null) {
			String pass = getLoginPassword();
			
			this.authInfo = loginExec(account, pass);
		}
	}
	@After
	public void tearDown() throws Exception {
		// Logout処理
		if (authInfo != null) {
			int status = logoutExec(authInfo); 
			if (status != AbstractPacket.CBS_WS_STAT_SUCCESS) {
				throw new Exception("Logout failed. Status=" + status);
			}
		}
		
		// DB Clear
		String[] tables = getDelTableName();
		if (tables != null) {
			for (String table : tables) {
				try {
					DatabaseOperation.DELETE_ALL.execute(base.getDatabaseTester().getConnection(), 
							new DefaultDataSet(new DefaultTable(table)));
				} catch (Exception e) {
					e.printStackTrace(System.err);
					throw new RuntimeException(e);
				}
			}
		}
		
		super.tearDown();
	}
	protected SysUserAuth getAuthInfo() {
		return authInfo;
	}
	/**
	 * 終了時にデータを削除するテーブル名を返す
	 * ない場合はnull
	 * @return テーブル名の配列
	 */
	protected String[] getDelTableName() {
		return null;
	}
}
