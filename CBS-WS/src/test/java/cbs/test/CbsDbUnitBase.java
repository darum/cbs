package cbs.test;

import java.io.File;
import java.util.ResourceBundle;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.rules.ExternalResource;

public class CbsDbUnitBase extends ExternalResource {
	
	private final String XML_DIR = "testXml";
	private final String PROP_FILE = "test_dbconfig";
	private final String DRIVER_NAME = "org.postgresql.Driver";

	private IDataSet dataSet;
	private IDatabaseTester databaseTester;
	
	private IDataSet allDataSet;
	
	public CbsDbUnitBase(String xmlDataFile) throws Exception {
		FlatXmlDataSetBuilder dsBuilder = new FlatXmlDataSetBuilder();
		dsBuilder.setCaseSensitiveTableNames(true);
		dsBuilder.setColumnSensing(true);
		dataSet = dsBuilder.build(new File(XML_DIR + "/" + xmlDataFile));		// TODO パス
		
		ResourceBundle bundle = ResourceBundle.getBundle(PROP_FILE);
		StringBuffer url = new StringBuffer();
		url.append("jdbc:postgresql://");
		url.append(bundle.getString("SERVER"));
		url.append(":");
		url.append(bundle.getString("PORT"));
		url.append("/");
		url.append(bundle.getString("DB"));
		url.append("/");
		
		databaseTester = new JdbcDatabaseTester(DRIVER_NAME, 
						url.toString(),
						bundle.getString("USER"),
						bundle.getString("PASSWORD"),
						"public");	// TODO 設定ファイル 
		
		// DBデータのバックアップ
		allDataSet = databaseTester.getConnection().createDataSet();
	}
	
	public IDatabaseTester getDatabaseTester() {
		return databaseTester;
	}

	@Override
	protected void before() throws Exception {
		// 内容をCLEANして、INSERTする
		databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
		
		// 大文字小文字を区別する
		DatabaseConfig dbConfig = databaseTester.getConnection().getConfig();
		dbConfig.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);
		dbConfig.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);

		// DataSetをセットする
		databaseTester.setDataSet(dataSet);
		
		// Setup実行
		databaseTester.onSetup();
	}
	
	@Override
	protected void after() {
		try {
			// データのリストア
			DatabaseOperation.CLEAN_INSERT.execute(databaseTester.getConnection(), allDataSet);
			
			this.databaseTester.onTearDown();
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}
}
