package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CbsTaskDataEx extends CbsTaskData {
//	private Logger logger = LoggerFactory.getLogger(getClass());
	private static Logger staticLogger = LoggerFactory.getLogger(CbsTaskDataEx.class);
	
	private String colName;
	private int itemDattype;
	
	public String getColName() {
		return colName;
	}
	public int getItemDattype() {
		return itemDattype;
	}
	public void setColName(String colName) {
		this.colName = colName;
	}
	public void setItemDattype(int itemDattype) {
		this.itemDattype = itemDattype;
	}

	/**
	 * タスクIDに該当するカラムリストを返す
	 * @param conn DB Connection
	 * @param listTaskId タスクIDのリスト(長さ1以上であること)
	 * @return カラムデータのリスト
	 * @throws SQLException
	 */
	public static List<CbsTaskDataEx> findByPjIDs(Connection conn, List<BigDecimal> listTaskId) throws SQLException {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("	* ");
		sql.append("FROM");
		sql.append("	CBS_TASK_DATA data,");
		sql.append("	CBS_TASK_COL_DEF def ");
		sql.append("WHERE ");
		sql.append("data.task_id IN ( ");
		for (int i = 0; i < listTaskId.size(); ++i) {
			if (i != 0) {
				sql.append(",");
			}
			sql.append("?");
		}
		sql.append(") AND ");
		sql.append("	data.item_id = def.item_id ");
		sql.append("ORDER BY data.task_id");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			List<CbsTaskDataEx> list = new ArrayList<CbsTaskDataEx>();
			
			st = conn.prepareStatement(sql.toString());
			staticLogger.debug(sql.toString());
			for (int i = 0; i < listTaskId.size(); ++i) {
				st.setBigDecimal(i + 1, listTaskId.get(i));
				staticLogger.debug("[{}]={}", i, listTaskId.get(i));
			}
			
			rs = st.executeQuery();
			while(rs.next()){
				CbsTaskDataEx instance = new CbsTaskDataEx();
				instance.setColId(rs.getBigDecimal("data.col_id"));
				instance.setTaskId(rs.getBigDecimal("data.task_id"));
				instance.setItemId(rs.getBigDecimal("data.item_id"));
				instance.setItemData(rs.getString("data.item_data"));
				instance.setColName(rs.getString("def.col_name"));
				instance.setItemDattype(Integer.valueOf(rs.getString("def.item_datatype")));
				
				instance.setCreateDatetime(rs.getTimestamp("data.create_datetime"));
				instance.setCreateUserId(rs.getBigDecimal("data.create_user_id"));
				instance.setUpdateDatetime(rs.getTimestamp("data.update_datetime"));
				instance.setUpdateUserId(rs.getBigDecimal("data.update_user_id"));
				instance.setUpdateNumber(rs.getBigDecimal("data.update_number"));
				list.add(instance);
			}
			
			return list;
		} catch (SQLException ex) {
			LoggerFactory.getLogger(CbsTaskDataEx.class).debug("SQL=[{}]", sql.toString(), ex);
			throw ex;
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		}
	}

	
}
