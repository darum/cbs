package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CbsTaskEx extends CbsTask {
	private static Logger staticLogger = LoggerFactory.getLogger(CbsTaskDataEx.class);
	
	private String resName;
	private List<CbsTaskDataEx> columns;

	/**
	 * 親タスクIDを指定して、タスク情報のリストを取得する
	 * @param conn
	 * @param parentId
	 * @param offset 開始位置
	 * @param limit 取得件数
	 * @return
	 * @throws SQLException
	 */
	public static List<CbsTaskEx> findByParentId(final Connection conn, BigDecimal projId, BigDecimal parentId, BigDecimal offset, BigDecimal limit) throws SQLException {
		List<CbsTaskEx> list = new ArrayList<CbsTaskEx>();
		
		/////////
		// Select TASK
		/////////
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("	task.task_id task_id,");
		sql.append("	task.project_id project_id,");
		sql.append("	task.parent_task_id parent_task_id,");
		sql.append("	task.task_name task_name,");
		sql.append("	task.pj_res_id pj_res_id,");
		sql.append("	task.start_datetime start_datetime,");
		sql.append("	task.term_datetime term_datetime,");
		sql.append("	task.progress progress,");
		sql.append("	task.create_datetime create_datetime,");
		sql.append("	task.create_user_id create_user_id,");
		sql.append("	task.update_datetime update_datetime,");
		sql.append("	task.update_user_id update_user_id,");
		sql.append("	task.update_number update_number,");
		sql.append("	res.name res_name ");
		sql.append("FROM");
		sql.append("	CBS_TASK task ");
		sql.append("	LEFT OUTER JOIN");
		sql.append("		CBS_PROJECT_RESOURCE pj_res ");
		sql.append("	ON");
		sql.append("		pj_res.pj_res_id = task.pj_res_id");
		sql.append("		LEFT OUTER JOIN");
		sql.append("			CBS_RESOURCE res");
		sql.append("		ON");
		sql.append("			res.res_id=pj_res.res_id ");
		sql.append("WHERE");
		sql.append("	task.project_id=? AND");
		sql.append("	task.parent_task_id=? ");
		sql.append("ORDER BY task.task_id ");
		sql.append("OFFSET ? ");
		if (!BigDecimal.ZERO.equals(limit)) {
			sql.append("LIMIT ?");
		}
		
		List<BigDecimal> taskIds = new ArrayList<BigDecimal>();
		ResultSet rs = null;
		PreparedStatement st = null;
		try{
			staticLogger.debug(sql.toString());
			st = conn.prepareStatement(sql.toString());
			StringBuffer debug = new StringBuffer();
			int idx = 1;
			{
				debug.append("[");
				debug.append(idx);
				debug.append("]=");
				debug.append(projId);
				debug.append(", ");
				st.setBigDecimal(idx++, projId);
			}
			{
				debug.append("[");
				debug.append(idx);
				debug.append("]=");
				debug.append(parentId);
				debug.append(", ");
				st.setBigDecimal(idx++, parentId);
			}
			{
				debug.append("[");
				debug.append(idx);
				debug.append("]=");
				debug.append(offset);
				debug.append(", ");
				st.setBigDecimal(idx++, offset);
			}
			if(!BigDecimal.ZERO.equals(limit)) {
				debug.append("[");
				debug.append(idx);
				debug.append("]=");
				debug.append(limit);
				st.setBigDecimal(idx, limit);
			}
			
			staticLogger.debug(debug.toString());
			rs = st.executeQuery();
			while(rs.next()) {
				CbsTaskEx instance = new CbsTaskEx();
				instance.setTaskId(rs.getBigDecimal("task_id"));
				instance.setProjectId(rs.getBigDecimal("project_id"));
				instance.setParentTaskId(rs.getBigDecimal("parent_task_id"));
				instance.setTaskName(rs.getString("task_name"));
				instance.setResId(rs.getBigDecimal("pj_res_id"));
				instance.setStartDatetime(rs.getTimestamp("start_datetime"));
				instance.setTermDatetime(rs.getTimestamp("term_datetime"));
				instance.setProgress(rs.getBigDecimal("progress"));
				instance.setCreateDatetime(rs.getTimestamp("create_datetime"));
				instance.setCreateUserId(rs.getBigDecimal("create_user_id"));
				instance.setUpdateDatetime(rs.getTimestamp("update_datetime"));
				instance.setUpdateUserId(rs.getBigDecimal("update_user_id"));
				instance.setUpdateNumber(rs.getBigDecimal("update_number"));
				instance.resName = rs.getString("res_name");
				instance.setColumns(new ArrayList<CbsTaskDataEx>());
				list.add(instance);
				
				taskIds.add(instance.getTaskId());
			}
			
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		}

		if (taskIds.size() > 0) {
			//////////
			// Select Columns
			//////////
			List<CbsTaskDataEx> listCols = CbsTaskDataEx.findByPjIDs(conn, taskIds);
			for (CbsTaskDataEx column : listCols) {
				for (CbsTaskEx tasks : list) {
					if (tasks.getTaskId().equals(column.getTaskId())) {
						tasks.columns.add(column);
						break;
					}
				}
			}
		}
		
		return list;
	}

	public List<CbsTaskDataEx> getColumns() {
		return columns;
	}
	
	public String getResName() {
		return resName;
	}

	public void setColumns(List<CbsTaskDataEx> columns) {
		this.columns = columns;
	}

	
}
