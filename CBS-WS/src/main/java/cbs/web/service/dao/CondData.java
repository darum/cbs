package cbs.web.service.dao;

/**
 * DBのカラムと値を格納するDataBean
 *
 */
public class CondData {
	
	private String column;		// カラム名
	private SqlData param;		// 値
	
	public CondData(String inColumn, SqlData inParam) {
		this.column = inColumn;
		this.param = inParam;
	}

	public String getColumn() {
		return column;
	}

	public SqlData getParam() {
		return param;
	}

}
