package cbs.web.service.dao;

public class SqlData {

	public SqlData(Object value, int sqlType) {
		super();
		this.value = value;
		this.sqlType = sqlType;
	}
	
	private Object value;
	private int sqlType;
	
	public Object getValue() {
		return value;
	}
	public int getSqlType() {
		return sqlType;
	}
}
