package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UpdateBuilder {
	
	private String table;
	private String pkColumn;
	private SqlData pk;
	private BigDecimal updateUserId;
	private BigDecimal updateNum;
	
	private List<SqlData> param;
	private StringBuffer set;	// SET句
	
	private List<CondData> where;	// WHERE句
	
	/**
	 * コンストラクタ
	 * @param tableName UPDATE対象のテーブル
	 * @param pkColumn PKのカラム
	 * @param primaryKey PKの値
	 * @param updateNum 更新カウンタ
	 */
	public UpdateBuilder(String tableName, String pkColumn, SqlData primaryKey, BigDecimal updateNum) {
		this.table = tableName;
		this.pkColumn = pkColumn;
		this.pk = primaryKey;
		this.updateNum = updateNum;
		
		this.param = new ArrayList<SqlData>();
		this.set = new StringBuffer();
		
		this.where = new ArrayList<CondData>();
	}
	
	/**
	 * 更新するカラムの追加
	 * @param column カラム名
	 * @param value 値
	 */
	public void addColumn(String column, SqlData value) {
		// column=? の追加
		if (param.size() != 0) {
			set.append(",");
		}
		set.append(column);
		set.append("=?");
		
		// Bind Parameterの追加
		param.add(value);
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("UPDATE ");
		sb.append(this.table);
		sb.append(" SET ");
		sb.append(set.toString());
		sb.append(", update_datetime=now(), update_user_id=?, update_number=?");
		sb.append(" WHERE ");
		sb.append(this.pkColumn);
		sb.append("=?");
		
		if (where.size() != 0) {
			for(CondData it: this.where) {
				sb.append(" AND ");
				sb.append(it.getColumn());
				sb.append("=?");
			}
		}
		
		return sb.toString();
	}

	public PreparedStatement createStatement(Connection conn) throws Exception {
		if (this.pk == null) {
			throw new Exception("The value of 'Primary key' is not set");
		}
		
		String sql = this.toString();
		PreparedStatement st = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

		int idx = 1;
		for(SqlData data:param) {
			st.setObject(idx, data.getValue(), data.getSqlType());
			idx++;
		}

		st.setBigDecimal(idx++, updateUserId);
		st.setBigDecimal(idx++, updateNum);
		st.setObject(idx++, this.pk.getValue(), this.pk.getSqlType());
		
		if(where.size() != 0) {
			for (CondData it : this.where) {
				SqlData param = it.getParam();
				st.setObject(idx, param.getValue(), param.getSqlType());
			}
		}
		
		return st;
	}

	public void setUpdateUserId(BigDecimal updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * 条件の追加
	 * @param data 条件データ
	 */
	public void addCondition(CondData data) {
		this.where.add(data);
	}
}
