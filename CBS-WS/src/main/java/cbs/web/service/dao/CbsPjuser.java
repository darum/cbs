package cbs.web.service.dao;

import java.sql.Timestamp;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Types;
import java.util.List;
import java.util.ArrayList;
/**
 *	cbs_pjuserテーブルの入出力を行う。
 *	Generated by EasyDao at 2014-08-16 15:24
 */
public class CbsPjuser {
	private BigDecimal projectUserId;
	private BigDecimal projectId;
	private BigDecimal loginUserId;
	private BigDecimal memberGroupId;
	private BigDecimal permission;
	private Timestamp createDatetime;
	private BigDecimal createUserId;
	private Timestamp updateDatetime;
	private BigDecimal updateUserId;
	private BigDecimal updateNumber;

	private String lastUpdateSql = null;
	
	/*
	 * getter
	 */
	public BigDecimal getProjectUserId() {
		return projectUserId;
	}
	public BigDecimal getProjectId() {
		return projectId;
	}
	public BigDecimal getLoginUserId() {
		return loginUserId;
	}
	public BigDecimal getMemberGroupId() {
		return memberGroupId;
	}
	public BigDecimal getPermission() {
		return permission;
	}
	public Timestamp getCreateDatetime() {
		return createDatetime;
	}
	public BigDecimal getCreateUserId() {
		return createUserId;
	}
	public Timestamp getUpdateDatetime() {
		return updateDatetime;
	}
	public BigDecimal getUpdateUserId() {
		return updateUserId;
	}
	public BigDecimal getUpdateNumber() {
		return updateNumber;
	}

	/*
	 * setter
	 */
	public void setProjectUserId(BigDecimal projectUserId) {
		this.projectUserId = projectUserId;
	}
	public void setProjectId(BigDecimal projectId) {
		this.projectId = projectId;
	}
	public void setLoginUserId(BigDecimal loginUserId) {
		this.loginUserId = loginUserId;
	}
	public void setMemberGroupId(BigDecimal memberGroupId) {
		this.memberGroupId = memberGroupId;
	}
	public void setPermission(BigDecimal permission) {
		this.permission = permission;
	}
	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}
	public void setCreateUserId(BigDecimal createUserId) {
		this.createUserId = createUserId;
	}
	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}
	public void setUpdateUserId(BigDecimal updateUserId) {
		this.updateUserId = updateUserId;
	}
	public void setUpdateNumber(BigDecimal updateNumber) {
		this.updateNumber = updateNumber;
	}

	/**
	 * 主キーを指定した検索を行う。
	 * 該当無ければnullを返す。
	 */
	public static CbsPjuser find(Connection conn, BigDecimal projectUserId) throws SQLException {
		ResultSet rs = null;
		PreparedStatement st = null;
		try {
			CbsPjuser instance = null;
			final String SQL = "select * from cbs_pjuser where project_user_id=?";
			st = conn.prepareStatement(SQL);
			st.setBigDecimal(1, projectUserId);
			rs = st.executeQuery();
			if(rs.next()) {
				instance = new CbsPjuser();
				instance.projectUserId = rs.getBigDecimal("project_user_id");
				instance.projectId = rs.getBigDecimal("project_id");
				instance.loginUserId = rs.getBigDecimal("login_user_id");
				instance.memberGroupId = rs.getBigDecimal("member_group_id");
				instance.permission = rs.getBigDecimal("permission");
				instance.createDatetime = rs.getTimestamp("create_datetime");
				instance.createUserId = rs.getBigDecimal("create_user_id");
				instance.updateDatetime = rs.getTimestamp("update_datetime");
				instance.updateUserId = rs.getBigDecimal("update_user_id");
				instance.updateNumber = rs.getBigDecimal("update_number");
			}
			return instance;

		} catch(SQLException ex) {
			throw ex;
		} finally {
			if(rs!=null) {
				rs.close();
			}
			if(st!=null) {
				st.close();
			}
		}
	}

	/**
	 * 全件を返す。
	 */
	public static List<CbsPjuser> selectAll(Connection conn) throws SQLException {
		ResultSet rs = null;
		PreparedStatement st = null;
		try {
			List<CbsPjuser> list = new ArrayList<CbsPjuser>();
			final String SQL = "select * from cbs_pjuser";
			st = conn.prepareStatement(SQL);
			rs = st.executeQuery();
			while(rs.next()) {
				CbsPjuser instance = new CbsPjuser();
				instance.projectUserId = rs.getBigDecimal("project_user_id");
				instance.projectId = rs.getBigDecimal("project_id");
				instance.loginUserId = rs.getBigDecimal("login_user_id");
				instance.memberGroupId = rs.getBigDecimal("member_group_id");
				instance.permission = rs.getBigDecimal("permission");
				instance.createDatetime = rs.getTimestamp("create_datetime");
				instance.createUserId = rs.getBigDecimal("create_user_id");
				instance.updateDatetime = rs.getTimestamp("update_datetime");
				instance.updateUserId = rs.getBigDecimal("update_user_id");
				instance.updateNumber = rs.getBigDecimal("update_number");
				list.add(instance);
			}
			return list;
		} catch(SQLException ex) {
			throw ex;
		} finally {
			if(rs!=null) {
				rs.close();
			}
			if(st!=null) {
				st.close();
			}
		}
	}

	public static List<CbsPjuser> findPjuser(Connection conn, final BigDecimal proj_id, final BigDecimal user_id) 
			throws SQLException {
		ResultSet rs = null;
		PreparedStatement st = null;
		try {
			List<CbsPjuser> list = new ArrayList<CbsPjuser>();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT");
			sql.append("	project_user_id,");
			sql.append("	project_id,");
			sql.append("	login_user_id,");
			sql.append("	permission ");
			sql.append("FROM");
			sql.append("	cbs_pjuser ");
			sql.append("WHERE");
			sql.append("	project_id=? AND");
			sql.append("	login_user_id=?");
			st = conn.prepareStatement(sql.toString());
			st.setBigDecimal(1, proj_id);
			st.setBigDecimal(2, user_id);
			
			rs = st.executeQuery();
			while(rs.next()) {
				CbsPjuser instance = new CbsPjuser();
				instance.projectUserId = rs.getBigDecimal("project_user_id");
				instance.projectId = rs.getBigDecimal("project_id");
				instance.loginUserId = rs.getBigDecimal("login_user_id");
//				instance.memberGroupId = rs.getBigDecimal("member_group_id");
				instance.permission = rs.getBigDecimal("permission");
				list.add(instance);
			}
			return list;
		} catch(SQLException ex) {
			throw ex;
		} finally {
			if(rs!=null) {
				rs.close();
			}
			if(st!=null) {
				st.close();
			}
		}
	}
	
	/**
	 * Insert処理
	 */
	public int insert(Connection conn) throws SQLException {
		PreparedStatement st = null;
		try {
			final StringBuffer sql = new StringBuffer();
			sql.append("INSERT INTO");
			sql.append("	cbs_pjuser (");
			sql.append("		project_id,");
			sql.append("		login_user_id,");
			sql.append("		permission,");
			sql.append("		create_datetime,");
			sql.append("		create_user_id,");
			sql.append("		update_datetime,");
			sql.append("		update_user_id)");
			sql.append("	values");
			sql.append("		(?, ?, ?, now(), ?, now(), ?)");
			
			st = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			int i = 1;
			st.setBigDecimal(i++, projectId);
			st.setBigDecimal(i++, loginUserId);
			st.setBigDecimal(i++, permission);
			st.setBigDecimal(i++, createUserId);
			st.setBigDecimal(i++, updateUserId);
			int count = st.executeUpdate();
			
			ResultSet rs = st.getGeneratedKeys();
			try {
				rs.next();
				this.projectUserId = rs.getBigDecimal(1);
			} finally {
				rs.close();
			}
			
			return count;
		} catch(SQLException ex) {
			throw ex;
		} finally {
			if(st!=null) {
				st.close();
			}
		}
	}

	/**
	 * Update処理
	 * @throws Exception 
	 */
	public int update(Connection conn) throws Exception {
		UpdateBuilder builder = new UpdateBuilder(
				"cbs_pjuser",									// TABLE
				"project_user_id",								// PK Column
				new SqlData(this.projectUserId, Types.INTEGER),	// PK value
				updateNumber);									// UpdateCounter
		builder.addColumn("permission", new SqlData(this.permission, Types.INTEGER));
		builder.setUpdateUserId(updateUserId);
		
		PreparedStatement st = builder.createStatement(conn);
		this.lastUpdateSql = builder.toString();
		int count = st.executeUpdate();
		return count;
	}

	/**
	 * Delete処理
	 */
	public int delete(Connection conn) throws SQLException {
		PreparedStatement st = null;
		try {
			final String SQL = "delete from cbs_pjuser where project_user_id=?";
			st = conn.prepareStatement(SQL);
			st.setBigDecimal(1, projectUserId);
			int count = st.executeUpdate();
			return count;
		} catch(SQLException ex) {
			throw ex;
		} finally {
			if(st!=null) {
				st.close();
			}
		}
	}
	public String getLastUpdateSql() {
		return lastUpdateSql;
	}
}
