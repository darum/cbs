package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PJ権限チェックDAO
 * @author Yamashita
 *
 */
public class PjPermissionMng {
	private static Logger logger = LoggerFactory.getLogger(PjPermissionMng.class);
	
	/**
	 * リソースIDが所属するプロジェクトのPJユーザにuserIdが登録されているかチェック
	 * @param resId リソースID
	 * @param userId システムユーザID
	 * @return true=Admin権限あり。false=なし または IDが不正
	 * @throws SQLException 
	 */
	public static boolean checkAdminByResId(Connection conn, final BigDecimal resId, final BigDecimal userId) throws SQLException {
		boolean result = false;
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("	pjuser.permission ");
		sql.append("FROM");
		sql.append("	CBS_PROJECT_RESOURCE res,");
		sql.append("	CBS_PJUSER pjuser ");
		sql.append("WHERE");
		sql.append("	res.res_id=? AND");
		sql.append("	pjuser.project_id=res.project_id AND");
		sql.append("	pjuser.login_user_id=?");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql.toString());
			st.setBigDecimal(1, resId);
			st.setBigDecimal(2, userId);
			
			rs = st.executeQuery();
			if (rs.next()) {
				if (BigDecimal.ONE.equals(rs.getBigDecimal(1))) {
					result = true;
				}
			}
		} catch(SQLException ex) {
			logger.error("PSQLException", ex);
			throw new SQLException(sql.toString(), ex);
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		}
		
		return result;
	}

}
