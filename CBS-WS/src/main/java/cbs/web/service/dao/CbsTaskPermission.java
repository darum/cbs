package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * タスク権限取得DAO
 * 
 * @author darum
 *
 */
public class CbsTaskPermission {
	private static Logger staticLogger = LoggerFactory.getLogger(CbsTaskDataEx.class);
	
	/* LoginUser ID */
	private BigDecimal loginUserId;
	/* Project ID */
	private BigDecimal projectId;
	/* Task ID */
	private BigDecimal taskId;
	
	public BigDecimal getLoginUserId() {
		return loginUserId;
	}
	public void setLoginUserId(BigDecimal loginUserId) {
		this.loginUserId = loginUserId;
	}
	public BigDecimal getProjectId() {
		return projectId;
	}
	public void setProjectId(BigDecimal projectId) {
		this.projectId = projectId;
	}
	public BigDecimal getTaskId() {
		return taskId;
	}
	public void setTaskId(BigDecimal taskId) {
		this.taskId = taskId;
	}

	/**
	 * 指定したタスクIDに権限があるかチェックする
	 * 
	 * @param taskId
	 * @param userId
	 * @return 権限があればDB上のデータを返す。権限がなければnull
	 */
	public static CbsTaskPermission checkExecute(final Connection conn, final BigDecimal taskId, final BigDecimal userId) throws Exception {
		staticLogger.info("Check User Permission: taskId={}, userId={}", taskId, userId);
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT");
		sql.append("	pjuser.login_user_id user_id");
		sql.append("	,project.project_id project_id");
		sql.append("	,task.task_id task_id ");
		sql.append("FROM");
		sql.append("	CBS_PJUSER pjuser ");
		sql.append("	,CBS_PROJECT project ");
		sql.append("	,CBS_TASK task ");
		sql.append("WHERE");
		sql.append("	task.task_id=? ");
		sql.append("AND task.project_id=project.project_id ");
		sql.append("AND project.project_id=pjuser.project_id ");
		sql.append("AND pjuser.login_user_id=?");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		CbsTaskPermission instance = null;
		try {
			staticLogger.debug(sql.toString());
			
			st = conn.prepareStatement(sql.toString());
			int idx = 1;
			st.setBigDecimal(idx++, taskId);
			st.setBigDecimal(idx++, userId);
			
			rs = st.executeQuery();
			
			if (rs.next()) {
				instance = new CbsTaskPermission();
				instance.setLoginUserId(rs.getBigDecimal("user_id"));
				instance.setProjectId(rs.getBigDecimal("project_id"));
				instance.setTaskId(rs.getBigDecimal("task_id"));
				
				staticLogger.info("Guarantee: projectID={}, taskID={}, userID={}", instance.getProjectId(), instance.getTaskId(), instance.getLoginUserId());
			} else {
				staticLogger.info("Not Guarantee");
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		}
		
		return instance;
	}
	
}
