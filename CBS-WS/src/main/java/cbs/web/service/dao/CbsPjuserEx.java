package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CbsPjuserEx extends CbsPjuser {
	
	/** プロジェクト名 */
	private String name;
	/** オーナーID */
	private BigDecimal sysUserId;
	/** オーナーアカウント名 */
	private String userAccount;
	
	/**
	 * ユーザに権限のあるプロジェクト一覧の取得
	 * @param conn DBコネクション
	 * @param userId ゆーざID
	 * @return 一覧
	 * @throws SQLException
	 */
	public static List<CbsPjuserEx> findByUserId(Connection conn, BigDecimal userId) throws SQLException {
		List<CbsPjuserEx> list = null;
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("	pjuser.project_user_id user_id,");
		sql.append("	pjuser.project_id pj_id,");
		sql.append("	pjuser.permission permission,");
		sql.append("	project.name pjname,");
		sql.append("    project.login_user_id login_user_id,");
		sql.append("    sysuser.login_account account ");
		sql.append("FROM");
		sql.append("	cbs_pjuser pjuser,");
		sql.append("	cbs_project project,");
		sql.append("	cbs_sysuser sysuser ");
		sql.append("WHERE");
		sql.append("	pjuser.login_user_id=? AND");
		sql.append("	project.project_id=pjuser.project_id AND");
		sql.append("	sysuser.login_user_id=project.login_user_id");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			st = conn.prepareStatement(sql.toString());
			st.setBigDecimal(1, userId);
			
			rs = st.executeQuery();
			while(rs.next()) {
				if (list == null) {
					list = new ArrayList<CbsPjuserEx>();
				}
				CbsPjuserEx entry = new CbsPjuserEx();
				entry.setProjectUserId(rs.getBigDecimal("user_id"));
				entry.setProjectId(rs.getBigDecimal("pj_id"));
				entry.setPermission(rs.getBigDecimal("permission"));
				entry.setName(rs.getString("pjname"));
				entry.setSysUserId(rs.getBigDecimal("login_user_id"));
				entry.setUserAccount(rs.getString("account"));
				
				list.add(entry);
			}
		} catch(SQLException ex) {
			throw new SQLException("SQL=[" + sql.toString() + "]", ex);
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		}
		
		return list;
	}

	/* getter */
	public String getName() {
		return name;
	}

	public BigDecimal getSysUserId() {
		return sysUserId;
	}

	public String getUserAccount() {
		return userAccount;
	}

	/* setter */
	public void setName(String name) {
		this.name = name;
	}

	public void setSysUserId(BigDecimal sysUserId) {
		this.sysUserId = sysUserId;
	}

	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

}
