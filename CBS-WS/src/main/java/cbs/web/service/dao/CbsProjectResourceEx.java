package cbs.web.service.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * PJリソース照会用DAO
 * @author Yamashita
 *
 */
public class CbsProjectResourceEx extends CbsProjectResource {
	private String name;

	/* getter */
	public String getName() {
		return name;
	}

	/* setter */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * ProjectIDに該当するリソースの取得
	 * @param conn DB Connection
	 * @param pjId ProjectID
	 * @return 結果。該当なしの場合はnull
	 * @throws SQLException 
	 */
	public static List<CbsProjectResourceEx> findByPjId(Connection conn, BigDecimal pjId) throws SQLException {
		List<CbsProjectResourceEx> ret = null;

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT");
		sql.append("	pjres.pj_res_id pj_res_id,");
		sql.append("	pjres.res_id res_id,");
		sql.append("	res.name resname ");
		sql.append("FROM");
		sql.append("	cbs_project_resource pjres,");
		sql.append("	cbs_resource res ");
		sql.append("WHERE");
		sql.append("	pjres.project_id=? AND");
		sql.append("	res.res_id=pjres.res_id");
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try{
			st = conn.prepareStatement(sql.toString());
			st.setBigDecimal(1, pjId);
			
			rs = st.executeQuery();
			while(rs.next()) {
				if (ret == null) {
					ret = new ArrayList<CbsProjectResourceEx>();
				}
				
				CbsProjectResourceEx bean = new CbsProjectResourceEx();
				bean.setPjResId(rs.getBigDecimal("pj_res_id"));
				bean.setResId(rs.getBigDecimal("res_id"));
				bean.setName(rs.getString("resname"));
				
				ret.add(bean);
			}
		} catch(SQLException ex) {
			throw new SQLException("SQL=[" + sql.toString() + "]", ex);
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
		}
		
		return ret;
	}
	
}
