package cbs.web.service;

import java.sql.SQLException;

import javax.sql.DataSource;

public class DBCPManager {

	private static DBCPManager instance;
	private DataSource dataSource = null;

	private void setDataSource1(DataSource ds) {
		dataSource = ds;
	}
	
	private DataSource getDataSource1() {
		return this.dataSource;
	}

	private DBCPManager(DataSource ds) {
		dataSource = ds;
	}
	
	/**
	 * DataSourceの設定
	 * @param ds
	 */
	public synchronized static void setDataSource(DataSource ds) {
		if (instance == null) {
			instance = new DBCPManager(ds);
		} else {
			instance.setDataSource1(ds);
		}
	}
	
	/**
	 * DataSourceの取得
	 * @return Connection
	 * @throws SQLException
	 */
	public static DataSource getDataSource() throws SQLException {
		if (instance == null) {
			throw new RuntimeException("DBに接続されていません");
		}
		return instance.getDataSource1();
	}
}
