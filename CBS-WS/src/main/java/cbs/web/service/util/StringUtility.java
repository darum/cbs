package cbs.web.service.util;

public class StringUtility {
	
	/**
	 * DAOデータ用に取得する
	 * @param inData 入力データ
	 * @return 変換データ。無効データの場合はnull
	 */
	public static String getDaoData(final String inData) {
		if (inData == null) {
			return null;
		} else {
			if (inData.length() == 0) {
				return null;
			}
		}
		return inData;
	}

}
