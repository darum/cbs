package cbs.web.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CBSWebServiceSCListener implements
		ServletContextListener {
	private final String PROP_FILE = "/dbcp.properties";
	
	Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		 //  Deploy Message
		logger.info("**************************");
		logger.info("Hello CBS WebService");
		logger.info("Version " + CBSWebServiceSystemInfo.API_VERSION);
		logger.info("**************************");
		
		// DB Connection Pooling
		try {
			Properties properties = new Properties();
			InputStream is = CBSWebServiceSCListener.class.getResourceAsStream(PROP_FILE);
			if (is == null) {
				throw new FileNotFoundException(PROP_FILE);
			}
			properties.load(is);
			DataSource ds = BasicDataSourceFactory.createDataSource(properties);
			DBCPManager.setDataSource(ds);
			
		} catch (IOException e) {
			// Property fileがない
			logger.error("File error: " + PROP_FILE + " - The application may not run normally.", e);
		} catch (Exception e) {
			logger.error("createDataSource() error - The Application may not run normally.", e);
		}
		
		logger.info("DBCP Setup finished.");
	}

}
