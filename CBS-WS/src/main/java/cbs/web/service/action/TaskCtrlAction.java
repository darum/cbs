package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsProjectResource;
import cbs.web.service.dao.CbsTask;
import cbs.web.service.dao.CbsTaskColDef;
import cbs.web.service.dao.CbsTaskData;
import cbs.web.service.packet.TaskControl;
import cbs.web.service.packet.databean.ColumnDataBean;
import cbs.web.service.util.StringUtility;

@Path("/task_ctrl")
public class TaskCtrlAction extends AbstractAction {

	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TaskControl addTask(TaskControl inData) throws Exception {
		logger.info("/task_ctrl/add");
		
		Connection conn = null;
		TaskControl outData = null;
		
		// Get Packet Data
		BigDecimal bdPjId = BigDecimal.valueOf(inData.getPjId());
		String task = inData.getTaskName();
		BigDecimal bdParentId = BigDecimal.valueOf(inData.getParentTaskId());
		BigDecimal charge = BigDecimal.valueOf(inData.getResId());
		long l_start = inData.getStartDT();
		Timestamp start = null;
		if (l_start != 0L) {
			start = new Timestamp(l_start);
		}
		
		long l_term = inData.getTermDT();
		Timestamp term = null;
		if (l_term != 0L) {
			term = new Timestamp(l_term);
		}
		
		double progress = inData.getProg();
		List<ColumnDataBean> columns = inData.getColumns();
		
		// Packet Check
		if ((StringUtility.getDaoData(task) == null)
				||(columns == null)) {
			logger.error("Packet Error");
			
			outData = new TaskControl();
			outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}

		// Project ID Check
		if (BigDecimal.ZERO.equals(bdPjId)) {
			logger.error("Project ID Error");
			
			outData = new TaskControl();
			outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_PJID);
			
			return outData;
		}
		
		
		try {
			conn = super.getConn();
			
			// Get SessionInfo
			String sessInfo = inData.getSessionId();
			if (StringUtility.getDaoData(sessInfo) == null) {
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_SESSID_ERROR);
				
				return outData;
			}
			
			int userId = super.getSessionInfo(conn, sessInfo);
			if (userId < 0) {
				outData = new TaskControl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			BigDecimal bdUserId = BigDecimal.valueOf(userId);
			
			// Check Project Permission
			if (!checkProjectPermission(conn, bdPjId, bdUserId)) {
				logger.info("ProjectID Error: id={}", bdPjId.intValue());
				
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_PJID);
				
				return outData;
			}
			
			// Check parent Task
			if (!BigDecimal.ZERO.equals(bdParentId)) {
				if (!isValidParentTask(conn, bdPjId, bdParentId)) {
					logger.info("Parent Task Error: id={}", bdParentId.intValue());
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_PARENT_NOT_FOUND);
					
					return outData;
				}
			}
			
			// Check ResID
			if (!BigDecimal.ZERO.equals(charge)) {
				if (!this.isValidResId(conn, bdPjId, charge)) {
					logger.error("Resource ID Error: id={}", charge.intValue());
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_RES_ID);
					
					return outData;
				}
			}
			
			// Check Datetime
			if ((l_start != 0L) && (l_term != 0L)) {
				if (l_start > l_term) {
					logger.error("Date Error: start={}, term={}", start, term);
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_DATE);
					
					return outData;
				}
			}
			
			// Check Progress
			if ((progress < 0.0) || (progress > 100.0)) {
				logger.error("Progress Error: value={}", progress);
				
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_PROG);
				
				return outData;
			}
			
			// Column ID Check
			List<BigDecimal> colIds = new ArrayList<BigDecimal>();	// IDのリスト
			
			for (ColumnDataBean entry : columns) {
				colIds.add(BigDecimal.valueOf(entry.getId()));
			}
			int idCount = colIds.size();
			if (idCount > 0) {
				List<CbsTaskColDef> result = CbsTaskColDef.find(conn, bdPjId, colIds);
				if (result.size() != idCount) {
					// カラムがない
					logger.error("Column ID error");
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_COLUMN_ID);
					
					return outData;
				}
			}
			
			// DAO: CBS_TASK
			CbsTask taskDao = new CbsTask();
			taskDao.setProjectId(bdPjId);
			taskDao.setTaskName(task);
			
			if (!BigDecimal.ZERO.equals(charge)) {
				taskDao.setResId(charge);
			}
			taskDao.setParentTaskId(bdParentId);
			taskDao.setStartDatetime(start);
			taskDao.setTermDatetime(term);
			taskDao.setProgress(BigDecimal.valueOf(progress));
			taskDao.setCreateUserId(bdUserId);
			taskDao.setUpdateUserId(bdUserId);
			int count = taskDao.insert(conn);
			
			BigDecimal taskId = BigDecimal.ZERO;
			if (count == 1) {
				taskId = taskDao.getTaskId();
				
				// DAO: CBS_TASK_DATA
				for (ColumnDataBean entry : columns) {
					CbsTaskData colDao = new CbsTaskData();
					colDao.setTaskId(taskId);
					colDao.setItemId(BigDecimal.valueOf(entry.getId()));
					colDao.setItemData(entry.getValue());
					colDao.setCreateUserId(bdUserId);
					colDao.setUpdateUserId(bdUserId);
					
					count = colDao.insert(conn);
				}
			}
			
			outData = new TaskControl();
			outData.setStatus(TaskControl.CBS_WS_STAT_SUCCESS);
			outData.setSessionId(sessInfo);
			outData.setTaskId(taskId.intValue());
			
			logger.info("Task Add finish: PjID={}, Name={}, id={}", inData.getPjId(), task, taskId.intValue());
			
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
	
	/**
	 * 指定された親タスクIDが有効か判定する
	 * @param conn Connection
	 * @param pjId プロジェクトID
	 * @param taskId 判定する親タスクID
	 * @return 結果: true=有効, false=無効
	 * @throws SQLException
	 */
	private boolean isValidParentTask(Connection conn, BigDecimal pjId, BigDecimal taskId) throws SQLException {
		boolean valid = false;
		
		CbsTask result = CbsTask.find(conn, taskId);
		if (result != null) {
			if(pjId.equals(result.getProjectId())) {
				valid = true;
			}
		}
		
		return valid;
	}
	
	/**
	 * PJリソースIDが有効か判定
	 * 
	 * @param conn DB接続子
	 * @param pjId プロジェクトID
	 * @param pjResId PJリソースID
	 * @return 有効/無効
	 * @throws SQLException
	 */
	private boolean isValidResId(Connection conn, BigDecimal pjId, BigDecimal pjResId) throws SQLException {
		boolean valid = false;
		
		CbsProjectResource result = CbsProjectResource.find(conn, pjResId);
		if (result != null) {
			if (pjId.equals(result.getPjResId())) {
				valid = true;
			}
		}
		
		return valid;
	}
	
	@POST
	@Path("/modify")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TaskControl modifyTask(TaskControl inData) throws Exception {
		logger.info("/task_ctrl/modify");
		
		TaskControl outData = null;
		
		// Get Packet Data
		BigDecimal bdTaskId = BigDecimal.valueOf(inData.getTaskId());
		
		try (Connection conn = super.getConn()){
			
			// Get SessionInfo
			String sessInfo = inData.getSessionId();
			if (StringUtility.getDaoData(sessInfo) == null) {
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_SESSID_ERROR);
				
				return outData;
			}
			
			int userId = super.getSessionInfo(conn, sessInfo);
			if (userId < 0) {
				outData = new TaskControl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			BigDecimal bdUserId = BigDecimal.valueOf(userId);
			
			// Check Task Permission
			if (!super.checkTaskPermission(conn, bdTaskId, bdUserId)) {
				logger.info("TaskID Error: id={}", bdTaskId.intValue());
				
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_TASKID);
				
				return outData;
			}
			
			// Update Parameters
			CbsTask dao = new CbsTask();
			boolean validParams = false;	// 編集対象のパラメータが設定されているかのフラグ
			
			BigDecimal bdParentValue = null;
			if (inData.getParentTaskId() != null) {		// ParentTask ID
				bdParentValue = BigDecimal.valueOf(inData.getParentTaskId().longValue());
				dao.setParentTaskId(bdParentValue);
				validParams = true;
			}
			if (inData.getResId() != 0) {		// リソースID
				BigDecimal bdPjResId = BigDecimal.valueOf(inData.getResId());
				if (CbsProjectResource.findWithTaskId(conn, bdPjResId, bdTaskId) == null) {
					// リソースIDなし
					logger.error("PJ ResID Error: taskId={}, pjResId={}", bdTaskId, bdPjResId);
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_RES_ID);
					
					return outData;
				}
				
				dao.setResId(bdPjResId);
				validParams = true;
			}
			if (inData.getStartDT() != 0) {		// 開始日時
				dao.setStartDatetime(new Timestamp(inData.getStartDT()));
				validParams = true;
			}
			if (inData.getTermDT() != 0) {		// 終了日時
				dao.setTermDatetime(new Timestamp(inData.getTermDT()));
				validParams = true;
			}
			Double progressObj = inData.getProg();
			if (progressObj != null) {		// 進捗率
				double prog = progressObj.doubleValue();
				if ((prog < 0) || (prog > 100)) {
					// Invalid Progress 
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_PROG);
					
					return outData;
				}
				
				dao.setProgress(BigDecimal.valueOf(prog));
				validParams = true;
			}
			boolean updateOnlyColumn = false;		// カラムリストのみ更新
			// カラムリスト
			List<ColumnDataBean> columnList = inData.getColumns();
			if (columnList != null) {
				if (!validParams) {
					updateOnlyColumn = true;
				}
				validParams = true;
			}
			
			if (!validParams) {
				// 更新するデータ無し
				logger.error("No Update data");
				
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_PACKET);
				
				return outData;
			}
			
			// リソースの存在チェック
			
			// PKのセット
			dao.setTaskId(bdTaskId);
			dao.setUpdateUserId(bdUserId);

			// Update Number
			CbsTask current = null;
			if(bdParentValue == null) {
				// 自分データだけ取得
				current = CbsTask.find(conn, bdTaskId);
				if (current == null) {
					// No Data
					logger.error("Task Not Found: taskID={}", bdTaskId.intValue());
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_TASKID);
					
					return outData;
				}
				
				// UpdateNumberの更新
				dao.setUpdateNumber(current.getUpdateNumber().add(BigDecimal.ONE));
			} else {
				// 親タスク候補と自分のデータを取得
				// TODO 自分と親の候補を取得し、チェックする（親が存在しているかチェック、自分のUpdate_Number）
				BigDecimal[] bdTaskIds = {bdTaskId, bdParentValue};
				List<CbsTask> taskList = CbsTask.select(conn, Arrays.asList(bdTaskIds));

				int check = 0;
				for (int i = 0; i < taskList.size(); ++i) {
					CbsTask cbsTask = taskList.get(i);
					if (bdTaskId.equals(cbsTask.getTaskId())) {
						// 自タスク
						check += 1;
						current = cbsTask;
						dao.setUpdateNumber(cbsTask.getUpdateNumber().add(BigDecimal.ONE));
					} else if (bdParentValue.equals(cbsTask.getTaskId())) {
						// 親タスク
						check += 10;
					}
				}
				if (check < 10) {
					// 親タスクなし
					logger.error("Parent task Not found. taskId={}, parent={}", 
							bdTaskId.intValue(), bdParentValue.intValue());
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_PARENT_NOT_FOUND);
					
					return outData;
				} else if (check == 10) {
					// 自タスクなし
					logger.error("Task Not Found: taskID={}", bdTaskId.intValue());
					
					outData = new TaskControl();
					outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_TASKID);
					
					return outData;
				}
			}
			
			// 日付チェック
			long startDT = 0, termDT = 0;
			if (dao.getStartDatetime() != null) {
				startDT = dao.getStartDatetime().getTime();
			} else if(current.getStartDatetime() != null) {
				startDT = current.getStartDatetime().getTime();
			}
			if (dao.getTermDatetime() != null) {
				termDT = dao.getTermDatetime().getTime();
			} else if(current.getTermDatetime() != null){
				termDT = current.getTermDatetime().getTime();
			}
			if (termDT < startDT) {
				logger.error("Datetime error: start={}, term={}", new Date(startDT), new Date(termDT));
				outData = new TaskControl();
				outData.setStatus(TaskControl.CBS_WS_STAT_INVALID_DATE);
				
				return outData;
			}
			
			
			// Update
			if (!updateOnlyColumn) {
				int count = dao.update(conn);
				if (count != 1) {
					logger.error("Update Failed: PK=[{}], SQL=[{}]", dao.getTaskId(), dao.getLatestUpdateSql());
					
					throw new Exception("Update failed");
				}
			}
			
			if (columnList != null) {
				// TODO カラムリストの更新
			}
			
			// Response
			logger.info("Task Modify finish: TaskID={}", inData.getTaskId());
			
			outData = new TaskControl();
			outData.setStatus(TaskControl.CBS_WS_STAT_SUCCESS);
			outData.setSessionId(sessInfo);
			return outData;
		}
		
	}
}
