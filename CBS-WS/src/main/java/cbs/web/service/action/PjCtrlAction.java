package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsProject;
import cbs.web.service.packet.ProjControl;
import cbs.web.service.util.StringUtility;

@Path("/pj_ctrl")
public class PjCtrlAction extends AbstractAction {
	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjControl addProj(ProjControl inData) throws Exception {
		logger.debug("add");
		
		ProjControl outData = null;
		
		// Packet取り出し
		String name = inData.getName();
		
		//
		boolean isValid = true;
		if (name == null) {
			isValid = false;
		} else if (name.length() == 0) {
			isValid = false;
		}
		if(!isValid) {
			outData = new ProjControl();
			outData.setStatus(ProjControl.CBS_WS_STAT_INVALID_PACKET);
			return outData;
		}
		
		Connection conn = null;
		
		try{
			conn = super.getConn();
			
			// セッション情報からユーザIDを取得
			int userId = super.getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				outData = new ProjControl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			CbsProject dao = new CbsProject();
			dao.setLoginUserId(BigDecimal.valueOf(userId));
			dao.setName(name);
			dao.setCreateUserId(BigDecimal.valueOf(userId));
			dao.setUpdateUserId(BigDecimal.valueOf(userId));
			int count = dao.insert(conn);
			if (count != 1) {
				throw new SQLException("INSERT Error");
			}
			
			// 戻りデータの生成
			int projId = dao.getProjectId().intValue();
			outData = new ProjControl(projId);
			outData.setSessionId(inData.getSessionId());
			
			logger.info("Project INSERT: id={}, name={}", projId, name);
		} catch (PSQLException ex) {
			if("23505".equals(ex.getSQLState())) {
				logger.info("Multiple Project Name: " + name);
				
				// 重複
				outData = new ProjControl();
				outData.setStatus(ProjControl.CBS_WS_STAT_MULTIPLE_NAME);
				
				return outData;
			}
			
			throw(ex);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
	
	@POST
	@Path("/modify")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjControl modifyProj(ProjControl inData) throws Exception {
		logger.debug("modify");
		
		ProjControl outData = null;
		
		// Packet取り出し
		int pjId = inData.getProjId();
		String pjName = inData.getName();
		
		// Packet Validation
		boolean isValid = true;
		if (StringUtility.getDaoData(pjName) == null) {
			isValid = false;
		} else if(pjId < 1) {
			isValid = false;
		}
		if(!isValid) {
			outData = new ProjControl();
			outData.setStatus(ProjControl.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		Connection conn = null;
		
		try {
			conn = getConn();
			
			// Get userId from session info.
			int userId = getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				outData = new ProjControl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			// Get CurrentData
			CbsProject current = CbsProject.find(conn, BigDecimal.valueOf(pjId));
			if (current == null) {
				// 該当PJ IDなし
				outData = new ProjControl();
				outData.setStatus(ProjControl.CBS_WS_STAT_PROJID_ERROR);
				
				return outData;
			}
			
			// Update
			CbsProject dao = new CbsProject();
			dao.setProjectId(BigDecimal.valueOf(pjId));
			dao.setName(pjName);
			// 管理データ
			dao.setUpdateUserId(BigDecimal.valueOf(userId));
			dao.setUpdateNumber(current.getUpdateNumber().add(BigDecimal.valueOf(1)));
			
			// Update exec
			int count = dao.update(conn);
			if (count != 1) {
				logger.info(dao.getLastUpdateSql());
				throw new SQLException("UPDATE Error");
			}
			
			// outbound Packet
			outData = new ProjControl();
			outData.setSessionId(inData.getSessionId());
		} catch(PSQLException ex) {
			if ("23505".equals(ex.getSQLState())) {
				// 重複
				outData = new ProjControl();
				outData.setStatus(ProjControl.CBS_WS_STAT_MULTIPLE_NAME);
				
				logger.info("Project Name is alread existed: {}", pjName);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
}
