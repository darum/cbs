package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsTaskColDef;
import cbs.web.service.packet.MetaControl;
import cbs.web.service.util.StringUtility;

/**
 * タスクカラム操作
 * @author Yamashita
 *
 */
@Path("/taskCol_ctrl")
public class TaskColCtrlAction extends AbstractAction {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public MetaControl addColumn(MetaControl inData) throws Exception {
		logger.info("/taskCol_ctrl/add");
		
		Connection conn = null;
		MetaControl outData = null;
		
		// Get Parameters
		BigDecimal bdPjId = BigDecimal.valueOf(inData.getPjId());
		String column = inData.getColumn();
		String dataType = inData.getDataType();
		if ((bdPjId == BigDecimal.ZERO) 
				|| (dataType == null) 
				|| (StringUtility.getDaoData(column) == null)) {
			// Packet Data invalid
			outData = new MetaControl();
			outData.setStatus(MetaControl.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		try {
			// Get Connection (from CP)
			conn = super.getConn();
			
			// Get SessionInfo
			String sessInfo = inData.getSessionId();
			if (StringUtility.getDaoData(sessInfo) == null) {
				outData = new MetaControl();
				outData.setStatus(MetaControl.CBS_WS_STAT_SESSID_ERROR);
				
				return outData;
			}
			
			int userId = super.getSessionInfo(conn, sessInfo);
			if (userId < 0) {
				outData = new MetaControl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			BigDecimal bdUserId = BigDecimal.valueOf(userId);
			
			// Check Project Permission
			if (!checkProjectPermission(conn, bdPjId, bdUserId)) {
				logger.info("ProjectID Error: id={}", bdPjId.intValue());
				
				outData = new MetaControl();
				outData.setStatus(MetaControl.CBS_WS_STAT_INVALID_PJID);
				
				return outData;
			}
			
			// DAO
			CbsTaskColDef dao = new CbsTaskColDef();
			dao.setProjectId(bdPjId);
			dao.setColName(column);
			dao.setItemDatatype(dataType);
			dao.setCreateUserId(bdUserId);
			dao.setUpdateUserId(bdUserId);
			int count = dao.insert(conn);
			
			int itemId = dao.getItemId().intValue();
			outData = new MetaControl(itemId);
			outData.setStatus(MetaControl.CBS_WS_STAT_SUCCESS);
			
		} catch (SQLException e) {
			String state = e.getSQLState();
			if ("23505".equals(state)) {
				logger.error("ColumnName Duplication. Project={}, Name=[{}]", bdPjId.intValue(), column);
				// Duplication error
				outData = new MetaControl();
				outData.setStatus(MetaControl.CBS_WS_STAT_MULTIPLE_NAME);
				
				return outData;
			}
			
			logger.error("DB Error. SQLState=" + e.getSQLState(), e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
}
