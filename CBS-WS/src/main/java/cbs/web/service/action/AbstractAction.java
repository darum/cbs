package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import cbs.web.service.DBCPManager;
import cbs.web.service.dao.CbsPjuser;
import cbs.web.service.dao.CbsTaskPermission;
import cbs.web.service.dao.CbsWsSession;
import cbs.web.service.packet.AbstractPacket;

/**
 * Action処理クラスのスーパークラス
 * @author Yamashita
 *
 */
abstract class AbstractAction {
	/* 定数 */
	protected final int CBS_WS_SESSID_INVALID = -1;
	protected final int CBS_WS_SESS_TO = -2;

	/**
	 * DBコネクションの取得
	 * @return Connection
	 * @throws Exception エラー
	 */
	protected Connection getConn() throws Exception {
		/* Get DB Connection from Connection Pooling */
		 return DBCPManager.getDataSource().getConnection();
	}

	/**
	 * DBに格納されているセッション情報の取得
	 * @param conn DBコネクション
	 * @param sessionId 検索するセッションID 
	 * @return ユーザID または セッションのエラーコード（マイナスの場合）
	 * @throws Exception 致命的なエラー
	 */
	protected int getSessionInfo(Connection conn, final String sessionId) 
			throws Exception {
		int ret = 0;
		
		CbsWsSession sessionInfo = CbsWsSession.find(conn, sessionId);
		
		if (sessionInfo == null) {
			// セッション情報なし
			ret = CBS_WS_SESSID_INVALID;
		} else if (sessionInfo.getExpireDatetime().getTime() < System.currentTimeMillis()) {
			// タイムアウト
			ret = CBS_WS_SESS_TO;
		} else {
			// 成功：ユーザIDを返す
			ret = sessionInfo.getLoginUserId().intValue();
		}
		
		return ret;
	}
	
	/**
	 * セッションエラーコードからパケットエラーの取得
	 * @param sessErr セッションエラーコード
	 * @return パケットエラーコード
	 */
	protected int getPktErrFromSessErrCode(final int sessErr) {
		int ret = 0;
		
		if (sessErr == CBS_WS_SESSID_INVALID) {
			ret = AbstractPacket.CBS_WS_STAT_SESSID_ERROR;
		} else if (sessErr == CBS_WS_SESS_TO) {
			ret = AbstractPacket.CBS_WS_STAT_SESSION_TO;
		}
		
		return ret;
	}
	
	/**
	 * プロジェクトの権限チェック
	 * @param conn Connection
	 * @param bdPjId プロジェクトID
	 * @param bdUserId ユーザID
	 * @return 結果(true=権限あり, false=権限なし)
	 * @throws Exception
	 */
	protected boolean checkProjectPermission (
			Connection conn, BigDecimal bdPjId, BigDecimal bdUserId) throws Exception {
		List<CbsPjuser> listPjuser = CbsPjuser.findPjuser(conn, bdPjId, bdUserId);
		boolean isValid = true;
		if (listPjuser.size() == 0) {
			isValid = false;
		} else {
			CbsPjuser pjuser = listPjuser.get(0);
			if (pjuser.getPermission().intValue() != 1) {		// TODO 定数
				isValid = false;
			}
		}
		
		return isValid;
	}
	
	/**
	 * タスクの権限チェック
	 * @param conn Connection
	 * @param bdTaskId タスクID
	 * @param bdUserId ユーザID
	 * @return true=権限あり, false=権限なし
	 * @throws Exception
	 */
	protected boolean checkTaskPermission(Connection conn, BigDecimal bdTaskId, BigDecimal bdUserId) throws Exception {
		boolean ret = false;
		CbsTaskPermission dao = CbsTaskPermission.checkExecute(conn, bdTaskId, bdUserId);
		if (dao != null) {
			ret = true;
		}
		
		return ret;
	}
}
