package cbs.web.service.action;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import cbs.web.service.CBSWebServiceSystemInfo;

@Path("/Sysinfo")
public class SysinfoAction {
	/**
	 * 廃止予定（POSTへ移行）
	 * @return
	 */
	@GET
	public String get() {
		StringBuffer res = new StringBuffer();
		
		res.append("===CBS WebService===<BR>\n");
		res.append("API Version=");
		res.append(CBSWebServiceSystemInfo.API_VERSION);
		res.append("<BR>\n");
		
		return res.toString();
	}
	
//	@PUT
//	@Path("/Sysinfo")
//	public SysinfoData post() {
//		SysinfoData resp = new SysinfoData();
//		
//		resp.setApiVersion(API_VERSION);
//		
//		return resp;
//	}

}
