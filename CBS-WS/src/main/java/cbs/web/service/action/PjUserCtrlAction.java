package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsPjuser;
import cbs.web.service.dao.CbsProject;
import cbs.web.service.packet.ProjUserControl;
import cbs.web.service.util.StringUtility;

@Path("/pjuser_ctrl")
public class PjUserCtrlAction extends AbstractAction {
	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());
			
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjUserControl addUser(ProjUserControl inData) throws Exception {
		logger.info("/pjuser_ctrl/add");
		
		Connection conn = null;
		ProjUserControl outData = null;
		
		// Parameter
		BigDecimal bdAddUserId = BigDecimal.valueOf(inData.getSysUserId());
		BigDecimal bdPjId = BigDecimal.valueOf(inData.getPjId());
		BigDecimal bdPermission = BigDecimal.valueOf(inData.getPermission());
		String sessId = inData.getSessionId();
		
		if ((bdAddUserId == BigDecimal.ZERO) ||
				(bdPjId == BigDecimal.ZERO) ||
				(bdPermission == BigDecimal.ZERO)){
			// Packetデータ不正
			outData = new ProjUserControl();
			outData.setStatus(ProjUserControl.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		if (StringUtility.getDaoData(sessId) == null) {
			outData = new ProjUserControl();
			outData.setStatus(ProjUserControl.CBS_WS_STAT_SESSID_ERROR);
			
			return outData;
		}
		
		try{
			// DB Connection
			conn = super.getConn();
			
			// Get SessionInfo
			int userId = getSessionInfo(conn, sessId);
			if (userId < 0) {
				outData = new ProjUserControl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			BigDecimal bdUserId = BigDecimal.valueOf(userId);
			
			CbsProject daoProject = CbsProject.find(conn, bdPjId);
			if (daoProject == null) {
				// PJ見つからない
				outData = new ProjUserControl();
				outData.setStatus(ProjUserControl.CBS_WS_STAT_INVALID_PROJ_ID);
				
				return outData;
			}
			
			// プロジェクト作成のユーザIDとセッションのユーザIDが一致する場合は、
			// 通常の権限チェックは行わない
			if (!bdUserId.equals(daoProject.getLoginUserId())) {
				
				// Permission
				if (!checkProjectPermission(conn, bdPjId, bdUserId)) {
					// 権限エラー
					outData = new ProjUserControl();
					outData.setStatus(ProjUserControl.CBS_WS_STAT_INVALID_PROJ_ID);
					
					return outData;
				}
			}
			
			// DAO
			CbsPjuser dao = new CbsPjuser();
			dao.setProjectId(bdPjId);
			dao.setLoginUserId(bdAddUserId);
			dao.setPermission(BigDecimal.valueOf(inData.getPermission()));
			dao.setCreateUserId(bdUserId);
			dao.setUpdateUserId(bdUserId);
			if(dao.insert(conn) != 1) {
				throw new SQLException("INSERT Error");
			}
			
			// Return Value
			int pjUserId = dao.getProjectUserId().intValue();
			outData = new ProjUserControl();
			outData.setSessionId(sessId);
			outData.setPjUserId(pjUserId);
			
			logger.info("PjUser INSERT: id={}, projectId={}, userId={}", 
					pjUserId, bdPjId.intValue(), bdUserId.intValue());
		
		} catch(PSQLException ex) {
			if ("23503".equals(ex.getSQLState())) {
				// 外部キー(PJユーザに追加するユーザID)なし
				outData = new ProjUserControl();
				outData.setStatus(ProjUserControl.CBS_WS_STAT_INVALID_SYSUSER);
				return outData;
			} else {
				logger.error("State=" + ex.getSQLState(), ex);
				throw ex;
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
	
	@POST
	@Path("/modify")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjUserControl modifyUser(final ProjUserControl inData) throws Exception {
		logger.info("/pjuser_ctrl/modify");
		
		ProjUserControl outData = null;
		Connection conn = null;
		
		// Get Packet Parameters
		BigDecimal bdPjUserId = BigDecimal.valueOf(inData.getPjUserId());
		BigDecimal bdPermission = BigDecimal.valueOf(inData.getPermission());
		
		if (bdPjUserId.equals(BigDecimal.ZERO)
				|| BigDecimal.ZERO.equals(bdPermission)) {
			// Invalid Packet data
			outData = new ProjUserControl();
			outData.setStatus(ProjUserControl.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		try {
			// DB Connection
			conn = getConn();
			
			// GetSessionInfo
			String sessId = inData.getSessionId();
			int userId = getSessionInfo(conn, sessId);
			if (userId < 0) {
				// Session Info Error
				outData = new ProjUserControl();
				outData.setStatus(super.getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			BigDecimal bdUserId = BigDecimal.valueOf(userId);
			
			// UserIDチェック
			boolean validUser = true;
			CbsPjuser pjUserDao = CbsPjuser.find(conn, bdPjUserId);
			if (pjUserDao == null) {
				// PJ UserIDがない
				validUser = false;
			} else if (!bdUserId.equals(pjUserDao.getLoginUserId())) {
				// PjUserIDがログインIDのモノではない
				validUser = false;
			}
			if (!validUser) {
				// PJ User ID is Invalid
				outData = new ProjUserControl();
				outData.setStatus(ProjUserControl.CBS_WS_STAT_INVALID_PJUSER_ID);
				
				return outData;
			}
			
			CbsPjuser dao = new CbsPjuser();
			dao.setProjectUserId(bdPjUserId);
			dao.setPermission(bdPermission);
			dao.setUpdateNumber(pjUserDao.getUpdateNumber().add(BigDecimal.ONE));
			dao.setUpdateUserId(bdUserId);
			int count = dao.update(conn);
			if (count != 1) {
				throw new Exception("Update Error");
			}
			
			// Return Data
			outData = new ProjUserControl();
			outData.setSessionId(sessId);
			outData.setStatus(ProjUserControl.CBS_WS_STAT_SUCCESS);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
	
}
