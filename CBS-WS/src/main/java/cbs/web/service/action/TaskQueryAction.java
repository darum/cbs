package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsTask;
import cbs.web.service.dao.CbsTaskDataEx;
import cbs.web.service.dao.CbsTaskEx;
import cbs.web.service.packet.TaskQuery;
import cbs.web.service.packet.databean.MetaDataBean;
import cbs.web.service.packet.databean.TaskDataBean;

@Path("/task_query")
public class TaskQueryAction extends AbstractAction {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/query")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TaskQuery query(final TaskQuery inData) throws Exception {
		logger.info("/task_query/query");
		
		TaskQuery outData = null;
		
		// Get PacketData
		BigDecimal bdProjId = BigDecimal.valueOf(inData.getProjId());
		BigDecimal bdTaskId = BigDecimal.valueOf(inData.getTaskId());
		int start = inData.getQueryStart();
		if (start < 0) {
			logger.error("startPos Error: {}", start);
			outData = new TaskQuery();
			outData.setStatus(TaskQuery.CBS_WS_STAT_INVALID_START_POS);

			return outData;
		}
		BigDecimal bdStart = BigDecimal.valueOf(start);
		int fetchCount = inData.getQueryCount();
		if (fetchCount < 0) {
			logger.error("fetchCount Error: {}", fetchCount);
			outData = new TaskQuery();
			outData.setStatus(TaskQuery.CBS_WS_STAT_INVALID_COUNT);
			
			return outData;
		}
		BigDecimal bdFetchCount = BigDecimal.valueOf(fetchCount);
		
		Connection conn = null;
		try {
			// Connect DB
			conn = super.getConn();
			
			// Get Session-UserID from SessionID
			int userId = getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				logger.info("SessionID Error. code={}", userId);
				
				outData = new TaskQuery();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			// Check Project Permission
			if (!checkProjectPermission(conn, bdProjId, BigDecimal.valueOf(userId))) {
				// No permission
				logger.info("Project ID Error: id={}", bdProjId.intValue());
				
				outData = new TaskQuery();
				outData.setStatus(TaskQuery.CBS_WS_STAT_INVALID_PROJECT_ID);
				
				return outData;
			}
			
			// Check Task
			if (!BigDecimal.ZERO.equals(bdTaskId)) {
				boolean isValidTask = true;
				CbsTask chkTask = CbsTask.find(conn, bdTaskId);
				if (chkTask == null) {
					isValidTask = false;
				} else if (!chkTask.getProjectId().equals(bdProjId)) {
					isValidTask = false;
				}
				if(!isValidTask) {
					logger.info("Task ID Error: task-ID={}", bdTaskId.intValue());
					
					outData = new TaskQuery();
					outData.setStatus(TaskQuery.CBS_WS_STAT_INVALID_TASK_ID);
					
					return outData;
				}
			}
			
			List <TaskDataBean> list = new ArrayList<TaskDataBean>();
			// DAO
			List<CbsTaskEx> dao = CbsTaskEx.findByParentId(conn, bdProjId, bdTaskId, bdStart, bdFetchCount);
			logger.debug("TaskList: count={}", dao.size());
			for (CbsTaskEx daoBean : dao) {
				TaskDataBean dataBean = new TaskDataBean();
				dataBean.setId(daoBean.getTaskId().intValue());
				dataBean.setTaskName(daoBean.getTaskName());
				BigDecimal tmp = daoBean.getResId();
				if (tmp != null) {
					dataBean.setResId(tmp.intValue());
				}
				dataBean.setResName(daoBean.getResName());
				Timestamp tsTmp;
				tsTmp = daoBean.getStartDatetime();
				if (tsTmp != null) {
					dataBean.setStart(new Date(tsTmp.getTime()));
				}
				tsTmp = daoBean.getTermDatetime();
				if (tsTmp != null) {
					dataBean.setTerm(new Date(tsTmp.getTime()));
				}
				tmp = daoBean.getProgress();
				if (tmp != null) {
					dataBean.setProgress(tmp.doubleValue());
				}
				
				// Column Data(s)
				List<MetaDataBean> listCol = new ArrayList<MetaDataBean>();
				for (CbsTaskDataEx dataColumn : daoBean.getColumns()) {
					MetaDataBean column = new MetaDataBean();
					column.setId(dataColumn.getColId().intValue());
					column.setData(dataColumn.getItemData());
					column.setDataType(dataColumn.getItemDattype());
				}
				dataBean.setColumns(listCol);
				
				list.add(dataBean);
			}
			
			outData = new TaskQuery();
			outData.setSessionId(inData.getSessionId());
			outData.setTask(list);
			
			logger.info("Select count={}", list.size());
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}

}
