package cbs.web.service.action;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsSysuser;
import cbs.web.service.packet.SysUserQuery;
import cbs.web.service.packet.databean.UserDataBean;
import cbs.web.service.util.StringUtility;

@Path("/user_query")
public class UserQueryAction extends AbstractAction {
	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());
			
	@POST
	@Path("/query")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SysUserQuery queryUser(SysUserQuery inData) throws Exception {
		logger.debug("query");
		
		SysUserQuery outData = null;
		
		// パケット取り出し
		String key = inData.getFindAccount();
		
		if (StringUtility.getDaoData(key) == null) {
			outData = new SysUserQuery();
			outData.setStatus(SysUserQuery.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		Connection conn = null;
		try {
			conn = getConn();
			
			// セッションチェック
			int sessErr = getSessionInfo(conn, inData.getSessionId());
			if (sessErr < 0) {
				// セッションエラー
				outData = new SysUserQuery();
				outData.setStatus(super.getPktErrFromSessErrCode(sessErr));
				
				return outData;
			}

			// Query
			CbsSysuser result = CbsSysuser.findByAccount(conn, key);
			
			// 戻りデータの生成
			List<UserDataBean> list = new ArrayList<UserDataBean>();
			if (result != null) {
				UserDataBean bean = new UserDataBean();
				bean.setUserId(result.getLoginUserId().intValue());
				bean.setAccount(result.getLoginAccount());
				bean.setEmail(result.getLoginUserEmail());
				if(result.getUserFirstName() != null) bean.setFirstName(result.getUserFirstName());
				if (result.getUserLastName() != null) bean.setLastName(result.getUserLastName());
				
				list.add(bean);
			}
			outData = new SysUserQuery(key);
			outData.setResult(list);
			
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
}
