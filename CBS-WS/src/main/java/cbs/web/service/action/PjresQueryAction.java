package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsProjectResourceEx;
import cbs.web.service.packet.ProjResQuery;
import cbs.web.service.packet.databean.PjresDataBean;
import cbs.web.service.util.StringUtility;

@Path("/pjres_query")
public class PjresQueryAction extends AbstractAction {
	
	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/query")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjResQuery query(ProjResQuery inData) throws Exception {
		logger.info("/pjres_query/query");
		
		// Response Data
		ProjResQuery outData = null;
		
		// vars
		Connection conn = null;
		
		// Packet
		BigDecimal bdPjId = BigDecimal.valueOf(inData.getPjId());
		
		try {
			// DB Connection
			conn = getConn();
			
			// Get SessionInfo
			String sessInfo = inData.getSessionId();
			if (StringUtility.getDaoData(sessInfo) == null) {
				logger.error("Invalid Session Info");
				
				// SessionID エラー
				outData = new ProjResQuery();
				outData.setStatus(ProjResQuery.CBS_WS_STAT_SESSID_ERROR);
				
				return outData;
			}
			
			int userId = getSessionInfo(conn, sessInfo);
			if (userId < 0) {
				logger.error("UserID error from SessionInfo");
				
				// ユーザIDがとれない場合
				outData = new ProjResQuery();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			// Check Project permission
			if (!checkProjectPermission(conn, bdPjId, BigDecimal.valueOf(userId))) {
				// No permission
				logger.info("ProjectID Error: id={}", bdPjId.intValue());
				
				outData = new ProjResQuery();
				outData.setStatus(ProjResQuery.CBS_WS_STAT_INVALID_PJID);
				
				return outData;
			}
			
			List<PjresDataBean> list = new ArrayList<PjresDataBean>();
			// Execute DB Query
			List<CbsProjectResourceEx> dao = CbsProjectResourceEx.findByPjId(conn, bdPjId);
			if (dao != null) {
				for (CbsProjectResourceEx daoBean: dao) {
					PjresDataBean dataBean = new PjresDataBean();
					dataBean.setPjResId(daoBean.getPjResId().intValue());
					dataBean.setResId(daoBean.getResId().intValue());
					dataBean.setResName(daoBean.getName());
					
					list.add(dataBean);
				}
			}
			
			// Return Data
			outData = new ProjResQuery();
			outData.setSessionId(sessInfo);
			outData.setList(list);
			
			logger.info("Query Result: Count={}", list.size());
			
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}

}
