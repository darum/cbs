package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsPjuserEx;
import cbs.web.service.packet.ProjQuery;
import cbs.web.service.packet.databean.ProjDataBean;

@Path("/pj_query")
public class PjQueryAction extends AbstractAction {

	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());

	@POST
	@Path("/queryAll")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjQuery queryAll(final ProjQuery inData) throws Exception {
		logger.info("/pj_query/queryAll");
		
		ProjQuery outData = null;
		
		// Get packet Data
		BigDecimal bdUserId = BigDecimal.valueOf(inData.getUserId());
		
		Connection conn = null;
		try {
			// Connect DB
			conn = super.getConn();
			
			// Get SessionUserID from Session Info
			int userId = getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				outData = new ProjQuery();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				return outData;
			}
			
			// Check UserId (Unmatch userId and userId in SessionInfo)
			if (bdUserId.intValue() != userId) {
				logger.error("UserID invalid: userId={}, session={}", bdUserId.intValue(), userId);
				
				outData = new ProjQuery();
				outData.setStatus(ProjQuery.CBS_WS_STAT_INVALID_USERID);
				
				return outData;
			}
			
			List<ProjDataBean> beans = new ArrayList<ProjDataBean>();
			// DB Query
			List<CbsPjuserEx> list = CbsPjuserEx.findByUserId(conn, bdUserId);
			if (list != null) {
				for (CbsPjuserEx entry : list) {
					ProjDataBean dataBean = 
							new ProjDataBean(entry.getProjectId().intValue(), 
									entry.getName(), 
									entry.getProjectUserId().intValue(), 
									entry.getPermission().intValue());
					dataBean.setOwnerId(entry.getSysUserId().intValue());
					dataBean.setOwnerAccount(entry.getUserAccount());
					beans.add(dataBean);
				}
			}
			
			// Return Data
			outData = new ProjQuery();
			outData.setSessionId(inData.getSessionId());
			outData.setResult(beans);
			
			logger.info("Query Result: count={}", beans.size());
			
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
			
}
