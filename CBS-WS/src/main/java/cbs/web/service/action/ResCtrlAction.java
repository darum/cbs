package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsResource;
import cbs.web.service.packet.ResCtrl;
import cbs.web.service.util.StringUtility;

@Path("res_ctrl")
public class ResCtrlAction extends AbstractAction {
	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResCtrl addProj(final ResCtrl inData) throws Exception {
		logger.info("/res_ctrl/add");
		
		ResCtrl outData = null;
		
		// Packet取り出し
		int loginUserId = inData.getUserId();
		BigDecimal bdLoginUserId = BigDecimal.valueOf(loginUserId);
		String name = inData.getResName();
		
		// Packet Data Check
		if ((loginUserId == 0) || (StringUtility.getDaoData(name) == null)) {
			outData = new ResCtrl();
			outData.setStatus(ResCtrl.CBS_WS_STAT_INVALID_PACKET);
			
			logger.error("Invalid Packet: userId={}, name={}", loginUserId, name);
			return outData;
		}
		
		Connection conn = null;
		try {
			conn = super.getConn();
			// セッション情報からユーザIDを取得
			int userId = super.getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				outData = new ResCtrl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			// IDが一致しない
			if (loginUserId != userId) {
				outData = new ResCtrl();
				outData.setStatus(ResCtrl.CBS_WS_STAT_INVALID_USER_ID);
				
				logger.error("Invalid UserID: userId={}", loginUserId);
				return outData;
			}
			
			// DAO
			CbsResource dao = new CbsResource();
			dao.setLoginUserId(bdLoginUserId);
			dao.setName(name);
			dao.setCreateUserId(BigDecimal.valueOf(userId));
			dao.setUpdateUserId(BigDecimal.valueOf(userId));
			if (dao.insert(conn) != 1) {
				logger.error("INSERT Error");
				throw new RuntimeException("INSERT Error");
			}
			
			// Result
			outData = new ResCtrl();
			outData.setResId(dao.getResId().intValue());
			outData.setSessionId(inData.getSessionId());
			
			logger.info("Resource INSERT: user={}, resId={}, name={}",
					bdLoginUserId.intValue(), outData.getResId(), inData.getResName());
		} catch (SQLException e) {
			if ("23505".equals(e.getSQLState())) {
				logger.info("Multiple Resource Name: " + name);
				
				outData = new ResCtrl();
				outData.setStatus(ResCtrl.CBS_WS_STAT_MULTIPLE_RES_NAME);
				
				return outData;
			} 
			throw(e);
		}  finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}

}
