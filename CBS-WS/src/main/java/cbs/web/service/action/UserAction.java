package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsSysuser;
import cbs.web.service.packet.SysUserControl;
import cbs.web.service.util.StringUtility;

@Path("/user")
public class UserAction extends AbstractAction {
	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * /user/add：ユーザの追加
	 *
	 * @param inData 入力オブジェクト
	 * @return 戻りオブジェクト
	 * @throws Exception 致命的なエラー
	 */
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SysUserControl addUser(SysUserControl inData) throws Exception {
		logger.info("add");

		Connection conn = null;
		SysUserControl outData = null;
		try {
			// DB接続
			conn = super.getConn();

			// パラメータ設定
			CbsSysuser dao = new CbsSysuser();
			/* アカウント */
			String account = StringUtility.getDaoData(inData.getAccount());
			if(account != null) {
				dao.setLoginAccount(account);
			}
			/* パスワード */
			String password = StringUtility.getDaoData(inData.getPassword());
			if (password != null) {
				dao.setLoginPasswd(password);
			}
			/* E-mail */
			String email = StringUtility.getDaoData(inData.getEmail());
			if (email != null) {
				dao.setLoginUserEmail(email);
			}
			
			if ((account == null) ||
					(password == null) ||
					(email == null)) {
				outData = new SysUserControl();
				outData.setStatus(SysUserControl.CBS_WS_STAT_INVALID_PACKET);
			} else {
				// INSERT実行
				dao.insertAccount(conn);
				
				// 戻り値
				int userId = dao.getLoginUserId().intValue();
				logger.info("Success: id={}", userId);
				
				// 戻りオブジェクトの生成
				outData = new SysUserControl();
				outData.setUserId(userId);
				outData.setAccount(dao.getLoginAccount());
				outData.setPassword(dao.getLoginPasswd());
				outData.setEmail(dao.getLoginUserEmail());
			}
		} catch (SQLException e) {
			if ("23505".equals(e.getSQLState())) {
				// アカウントの重複
				outData = new SysUserControl();
				outData.setStatus(SysUserControl.CBS_WS_STAT_ACCOUNT_DUPLICATED);
				
				logger.info("ACCOUNT already existed: " + inData.getAccount());
			} else {
				logger.error("INSERT", e);
			}

		} catch (Exception e) {
			logger.error("INSERT", e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

		return outData;
	}
	
	@POST
	@Path("/modify")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SysUserControl modifyUser(final SysUserControl inData) throws Exception {
		logger.info("modify");
		
		Connection conn = null;
		SysUserControl outData = null;
		CbsSysuser dao = new CbsSysuser();
		try {
			// DB接続
			conn = super.getConn();

			int userId = super.getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				// Session Error
				outData = new SysUserControl();
				outData.setStatus(super.getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			if (userId != inData.getUserId()) {
				// セッションのユーザIDとパケットのユーザIDが一致しない
				outData = new SysUserControl();
				outData.setStatus(SysUserControl.CBS_WS_START_USERID_INVALID);
				
				return outData;
			}
			
			// Update
			boolean isValued = false;	// 値が設定されているか
			
			dao.setLoginUserId(BigDecimal.valueOf(userId));
			
			CbsSysuser current = CbsSysuser.find(conn, BigDecimal.valueOf(userId));
			if (current == null) {
				// ユーザIDなし
				outData = new SysUserControl();
				outData.setStatus(SysUserControl.CBS_WS_START_USERID_INVALID);
				
				return outData;
			}
			dao.setUpdateNumber(BigDecimal.valueOf(current.getUpdateNumber().intValue() + 1));
			
			// アカウント
			String account = inData.getAccount();
			if (StringUtility.getDaoData(account) != null) {
				dao.setLoginAccount(account);
				isValued = true;
			}
			
			// パスワード
			String password = inData.getPassword();
			if (StringUtility.getDaoData(password) != null) {
				dao.setLoginPasswd(password);
				isValued = true;
			}
			
			// E-mail
			String email = inData.getEmail();
			if (StringUtility.getDaoData(email) != null) {
				dao.setLoginUserEmail(email);
				isValued = true;
			}
			
			// FirstName
			String firstName = inData.getFirstName();
			if (StringUtility.getDaoData(firstName) != null) {
				dao.setUserFirstName(firstName);
				isValued = true;
			}
			
			// LastName
			String lastName = inData.getLastName();
			if (StringUtility.getDaoData(lastName) != null) {
				dao.setUserLastName(lastName);
				isValued = true;
			}
			
			if (!isValued) {
				// 有効な値が設定されていない
				outData = new SysUserControl();
				outData.setStatus(SysUserControl.CBS_WS_STAT_INVALID_PACKET);
				
				return outData;
			}
			
			int count = dao.update(conn);
			if (count == 0) {
				// 更新なし
				logger.error("Update Failed: key={}, SQL={}", userId, dao.getLastUpdateSql());
				
				throw new Exception();
			}
			
			outData = new SysUserControl();
		} catch(SQLException ex) {
			if ("23505".equals(ex.getSQLState())) {
				// アカウントの重複
				outData = new SysUserControl();
				outData.setStatus(SysUserControl.CBS_WS_STAT_ACCOUNT_DUPLICATED);
				
				logger.info("ACCOUNT already existed: " + inData.getAccount());
			} else {
				logger.info("SQL=[{}]", dao.getLastUpdateSql());
				throw new SQLException(ex);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
	
	/**
	 *　/user/delete: ユーザの削除
	 * @param inData
	 * @return 戻りオブジェクト
	 * @throws Exception 致命的なエラー
	 */
	@POST
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SysUserControl deleteUser(SysUserControl inData) throws Exception {
		logger.info("delete");
		
		Connection conn = null;
		SysUserControl outData = null;
		try {
			// DB接続
			conn = super.getConn();

			// DAO
			CbsSysuser dao = new CbsSysuser();
			
			dao.setLoginUserId(BigDecimal.valueOf(inData.getUserId()));
			
			dao.delete(conn);
			logger.info("Success Delete: UserID={}", inData.getUserId());
			
			// 戻りパケット
			outData = new SysUserControl();
			
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

		return outData;
	}
	
}
