package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsProjectResource;
import cbs.web.service.dao.CbsResource;
import cbs.web.service.packet.ProjResCtrl;
import cbs.web.service.util.StringUtility;

/**
 * プロジェクトリソース操作
 * @author Yamashita
 *
 */
@Path("pjres_ctrl")
public class PjresCtrlAction extends AbstractAction {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/add")	
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ProjResCtrl addRes(ProjResCtrl inData) throws Exception {
		logger.info("/pjres_ctrl/add");
		
		Connection conn = null;
		ProjResCtrl outData = null;

		// Get Parameters
		BigDecimal bdPjId = BigDecimal.valueOf(inData.getProjId());
		BigDecimal bdResId = BigDecimal.valueOf(inData.getResId());
		if ((bdResId == BigDecimal.ZERO) || (bdPjId == BigDecimal.ZERO)) {
			// Packet Data is invaid
			outData = new ProjResCtrl();
			outData.setStatus(ProjResCtrl.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		try{
			// DB Connection
			conn = getConn();
			
			// Get SessionInfo
			String sessInfo = inData.getSessionId();
			if (StringUtility.getDaoData(sessInfo) == null) {
				outData = new ProjResCtrl();
				outData.setStatus(ProjResCtrl.CBS_WS_STAT_SESSID_ERROR);
				
				return outData;
			}
			
			int userId = super.getSessionInfo(conn, sessInfo);
			if (userId < 0) {
				outData = new ProjResCtrl();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			BigDecimal bdUserId = BigDecimal.valueOf(userId);
			
			// PJ権限チェック
			if(!checkProjectPermission(conn, bdPjId, bdUserId)) {
				logger.info("ProjectID Error: id={}", bdPjId.intValue());
				// 権限エラー
				outData = new ProjResCtrl();
				outData.setStatus(ProjResCtrl.CBS_WS_STAT_INVALID_PROJID);
				
				return outData;
			}
			
			// リソース権限チェック
			if(!checkResId(conn, bdUserId, bdResId)) {
				// リソース該当なし
				logger.info("ResourceID Error: User={}, Res={}", bdUserId.intValue(), bdResId.intValue());
				
				outData = new ProjResCtrl();
				outData.setStatus(ProjResCtrl.CBS_WS_STAT_INVALID_RESID);
				
				return outData;
			}
			
			// DAO
			CbsProjectResource dao = new CbsProjectResource();
			dao.setProjectId(bdPjId);
			dao.setResId(bdResId);
			dao.setCreateUserId(bdUserId);
			dao.setUpdateUserId(bdUserId);
			int count = dao.insert(conn);
			if (count != 1) {
				throw new SQLException("INSERT Error");
			}
			
			// Return Data
			int plResId = dao.getResId().intValue();
			outData = new ProjResCtrl();
			outData.setSessionId(sessInfo);
			outData.setResId(plResId);
			
			logger.info("Resource INSERT: id={}, project={}, resource={}", 
					plResId, bdPjId.intValue(), bdResId.intValue());
		} catch (PSQLException ex) {
			if ("23505".equals(ex.getSQLState())) {
				// TODO
//				logger.info("Multiple Resource Name: {}", name);
//				
//				outData = new ProjResCtrl();
//				outData.setStatus(ProjResCtrl.CBS_WS_STAT_MULTIPLE_RESNAME);
			} else {
				logger.error("PSQLException State=" + ex.getSQLState(), ex);
				throw (ex);
			}
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}

//	@POST
//	@Path("/modify")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public ProjResCtrl modifyRes(final ProjResCtrl inData) throws Exception {
//		logger.info("/res_ctrl/modify");
//		
//		ProjResCtrl outData = null;
//		
//		// Parameters
//		String name = inData.getResName();
//		BigDecimal bdResId = BigDecimal.valueOf(inData.getResId());
//		if ((StringUtility.getDaoData(name) == null) ||
//				(BigDecimal.ZERO.equals(bdResId))) {
//			// リソース名、リソースIDがない
//			// Invalid Packet
//			outData = new ProjResCtrl();
//			outData.setStatus(ProjResCtrl.CBS_WS_STAT_INVALID_PACKET);
//			
//			return outData;
//		}
//		
//		Connection conn = null;
//		CbsProjectResource dao = null;
//		try {
//			conn = getConn();
//			
//			// GetSessionInfo
//			int userId = getSessionInfo(conn, inData.getSessionId());
//			if (userId < 0) {
//				outData = new ProjResCtrl();
//				outData.setStatus(getPktErrFromSessErrCode(userId));
//				
//				return outData;
//			}
//			BigDecimal bdUserId = BigDecimal.valueOf(userId);
//			
//			// Check Permission
//			if (!PjPermissionMng.checkAdminByResId(conn, bdResId, bdUserId)) {
//				outData = new ProjResCtrl();
//				outData.setStatus(ProjResCtrl.CBS_WS_STAT_INVALID_RESID);
//				
//				return outData;
//			}
//			
//			// GetCurrentData
//			CbsProjectResource current = CbsProjectResource.find(conn, bdResId);
//			if (current == null) {
//				// 該当RES IDなし
//				outData = new ProjResCtrl();
//				outData.setStatus(ProjResCtrl.CBS_WS_STAT_INVALID_RESID);
//				
//				return outData;
//			}
//			
//			// Update
//			dao = new CbsProjectResource();
//			dao.setResId(bdResId);
//			dao.setName(name);
//			// 管理データ
//			dao.setUpdateUserId(bdUserId);
//			dao.setUpdateNumber(current.getUpdateNumber().add(BigDecimal.ONE));
//			if (dao.update(conn) != 1) {
//				throw new SQLException("UPDATE error: SQL=[" + dao.getLastUpdateSql() + "]");
//			}
//
//			// Return
//			outData = new ProjResCtrl();
//			outData.setSessionId(inData.getSessionId());
//		
//		} catch(PSQLException ex) {
//			if ("23505".equals(ex.getSQLState())) {
//				// 重複
//				outData = new ProjResCtrl();
//				outData.setStatus(ProjResCtrl.CBS_WS_STAT_MULTIPLE_RESNAME);
//				
//				logger.info("Res name is already existed: {}", name);
//			} else {
//				logger.error("UPDATE error: SQL=" + dao.getLastUpdateSql(), ex);
//				throw ex;
//			}
//		} finally {
//			if (conn != null) {
//				conn.close();
//			}
//		}
//		
//		return outData;
//	}

	/**
	 * リソースIDの有効性チェック
	 * @param userId ユーザID (存在していること)
	 * @param resId リソースID
	 * @return 判定結果. true=有効、false=無効または存在しない
	 * @throws SQLException 
	 */
	// TODO ResCtrlActionと統合できるか検討
	private boolean checkResId(Connection conn, BigDecimal userId, BigDecimal resId) throws SQLException {
		boolean ret = true;
		
		CbsResource dao = CbsResource.find(conn, resId);
		if (dao == null) {
			/* 該当なし */
			ret = false;
		} else if(!dao.getLoginUserId().equals(userId)) {
			/* ユーザが一致しない */
			ret = false;
		}
		
		return ret;
	}
}
