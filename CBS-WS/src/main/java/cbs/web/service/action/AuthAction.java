package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsSysuser;
import cbs.web.service.dao.CbsWsSession;
import cbs.web.service.packet.SysUserAuth;

@Path("/auth")
public class AuthAction extends AbstractAction {

	private Logger logger = LoggerFactory.getLogger(getClass());
	@Context private HttpServletRequest request;
	
	private static long EXPIRE_MILLIS = 1 * 60 * 1000;	// 1分： TODO 設定ファイル
	
	/**
	 *  /auth/login：ログイン
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SysUserAuth loginAuth(SysUserAuth inData) throws Exception {
		logger.info("login account=[" + inData.getAccount() + "]");
		
		SysUserAuth outData = null;
		
		// パラメータ
		String account = inData.getAccount();
		String password = inData.getPassword();
		
		// 引数チェック
		if((account.length() == 0) || (password.length() == 0)) {
			outData = new SysUserAuth();
			outData.setStatus(SysUserAuth.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		Connection conn = null;
		boolean isAuth = false;
		try {
			// DB接続
			conn = super.getConn();

			int id = -1;
			CbsSysuser search = CbsSysuser.findByAccount(conn, account);
			if (search != null) {
				// ユーザがあった場合：パスワード正当性判定
				isAuth = password.equals(search.getLoginPasswd().trim());
				id = search.getLoginUserId().intValue();
			} else {
				logger.debug("No such user: {}", account);
			}
			
			if (isAuth) {
				// Auth OK
				outData = new SysUserAuth(id, account);
				
				// セッション開始
				String sessionId = request.getSession(true).getId();
				
				// セッション情報記録
				BigDecimal userId = BigDecimal.valueOf(id);
				Timestamp expire = new Timestamp(System.currentTimeMillis() + EXPIRE_MILLIS);
				CbsWsSession sessDao = new CbsWsSession(userId, sessionId, expire);
				if (sessDao.insert(conn) != 1) {
					// SessionID重複
					throw new SQLException();
				}
				
				logger.info("Session Start: loginId=[{}], sessionId=[{}]", id, sessionId);
				outData.setSessionId(sessionId);
			} else {
				//認証エラー
				logger.info("Auth Error");
				outData = new SysUserAuth();
				outData.setStatus(SysUserAuth.CBS_WS_STAT_AUTH_ERROR);
			}

		} catch (Exception e) {
			logger.error("LOGIN Action", e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}

	/**
	 * /auth/logout
	 * @param inData 入力パケット
	 * @return 出力パケット
	 * @throws Exception 例外
	 */
	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public SysUserAuth logoutAuth(SysUserAuth inData) throws Exception {
		logger.info(String.format("logout id=%d", inData.getUserId()));
		
		SysUserAuth outData = null;
		
		// Parameter
		String sessionId = inData.getSessionId();
		int userId = inData.getUserId();
		
		// 引数チェック
		boolean isValid = true;
		if (sessionId == null) {
			isValid = false;
		} else if ((sessionId.length() == 0) || (userId <= 0)) {
			isValid = false;
		}
		
		if (!isValid) {
			outData = new SysUserAuth();
			outData.setStatus(SysUserAuth.CBS_WS_STAT_INVALID_PACKET);
			
			return outData;
		}
		
		Connection conn = null;
		try{
			// DB接続
			conn = super.getConn();
			
			// DAO
			CbsWsSession result = CbsWsSession.find(conn, sessionId);
			
			outData = new SysUserAuth();
			if (result == null) {
				// セッションIDが見つからない
				outData.setStatus(SysUserAuth.CBS_WS_STAT_SESSID_ERROR);
			} else {
				// セッション削除
				CbsWsSession dao = new CbsWsSession();
				dao.setSessionId(sessionId);
				int delCount = dao.delete(conn);
				if (delCount != 1) {
					throw new Exception("DELETE error");
				}
				
				logger.info("Session delete: id=" + inData.getUserId());
			}

			logger.info("Logout Succeccfull");
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
}
