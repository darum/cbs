package cbs.web.service.action;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.dao.CbsTaskColDef;
import cbs.web.service.packet.ColumnQuery;
import cbs.web.service.packet.databean.MetaDataBean;

@Path("/taskCol_query")
public class TaskColQueryAction extends AbstractAction {

	/** Logger */
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@POST
	@Path("/queryAll")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ColumnQuery queryAll(final ColumnQuery inData) throws Exception {
		logger.info("/taskCol_query/queryAll");
		
		ColumnQuery outData = null;
		
		// Get PacketData
		BigDecimal bdProjId = BigDecimal.valueOf(inData.getProjId());
		
		Connection conn = null;
		try {
			// Connect DB
			conn = super.getConn();
			
			// Get SessionUserID from Session Info
			int userId = getSessionInfo(conn, inData.getSessionId());
			if (userId < 0) {
				logger.info("SessionID Error. code=" + userId);
				outData = new ColumnQuery();
				outData.setStatus(getPktErrFromSessErrCode(userId));
				
				return outData;
			}
			
			// Check Project Permission
			if (!checkProjectPermission(conn, bdProjId, BigDecimal.valueOf(userId))) {
				// No Permission
				logger.info("Project ID Error: id={}", bdProjId.intValue());
				
				outData = new ColumnQuery();
				outData.setStatus(ColumnQuery.CBS_WS_STAT_INVALID_PROJECT_ID);
				
				return outData;
			}
			
			List<MetaDataBean> list = new ArrayList<MetaDataBean>();
			// Execute DB Query
			List<CbsTaskColDef> dao = CbsTaskColDef.findByPjId(conn, bdProjId);
			if (dao != null) {
				for (CbsTaskColDef daoBean : dao) {
					MetaDataBean dataBean = new MetaDataBean();
					dataBean.setId(daoBean.getItemId().intValue());
					dataBean.setName(daoBean.getColName());
					dataBean.setDataType(Integer.valueOf(daoBean.getItemDatatype()));
					list.add(dataBean);
				}
			}
			
			// Return Data
			outData = new ColumnQuery();
			outData.setSessionId(inData.getSessionId());
			outData.setColumns(list);
			
			logger.info("Query result: count={}", list.size());
			
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		
		return outData;
	}
}
