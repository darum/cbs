package cbs.web.service;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/api/")
public class CBSWebServiceApplication extends ResourceConfig {

	public CBSWebServiceApplication() {
		packages(true, "cbs.web.service");

		register(MoxyJsonFeature.class);
//		register(JsonMoxyConfigurationContextResolver.class);
	}
}
