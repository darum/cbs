package cbs.web.service;

/**
 * パケット用定数
 * @author Yamashita
 *
 */
public final class CbsWsConstant {

	public static final String INVALID = "0";
	
	/** 文字列データ (varchar) */
	public static final String STRING = "1";
	/** 整数データ (integer) */
	public static final String INT = "2";
	/** 日付データ（timestamp） */
	public static final String DATETIME = "3";
	/** 真偽データ（boolean） */
	public static final String CHECK = "4";

}
