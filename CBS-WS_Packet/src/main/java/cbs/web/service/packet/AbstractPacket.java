package cbs.web.service.packet;

public abstract class AbstractPacket {
	/* ステータスコード */
	public static final int CBS_WS_STAT_SUCCESS = 0;
	public static final int CBS_WS_STAT_SESSID_ERROR = 1;		// セッションIDエラー
	public static final int CBS_WS_STAT_SESSION_TO = 2;		// セッションタイムアウト
	public static final int CBS_WS_STAT_INVALID_PACKET = 3;	// 必要な項目不足（不正パケット）
	
	protected static final int CBS_WS_STAT_DEDICATED_BASE = (byte)0x80;	// 個別エラーのコード起点
	
	/* 共通メンバ */
	/** StatusCode */
	private int status = CBS_WS_STAT_SUCCESS;
	/** Session ID */
	private String sessionId = null;

	/* アクセサメソッド */
	public int getStatus() {
		return status;
	}
	
	public String getSessionId() {
		return sessionId;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
