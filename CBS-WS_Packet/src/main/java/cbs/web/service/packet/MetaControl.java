package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MetaControl extends AbstractPacket {
	
	/* 定数 */
	public static final int CBS_WS_STAT_INVALID_METAID = CBS_WS_STAT_DEDICATED_BASE + 1;
	public static final int CBS_WS_STAT_INVALID_PJID = CBS_WS_STAT_DEDICATED_BASE + 2;
	public static final int CBS_WS_STAT_MULTIPLE_NAME = CBS_WS_STAT_DEDICATED_BASE + 3; 
	
	/* コンストラクタ */
	/**
	 * コンストラクタ(for JAXB)
	 */
	public MetaControl() {
		
	}
	
	/**
	 * コンストラクタ
	 * @param itemId カラムID
	 */
	public MetaControl(int itemId) {
		this.metaId = itemId;
	}
	
	/* メンバ */
	private int metaId;
	private int pjId;
	private String column;
	private String dataType;
	
	/* getter/setter */
	public int getMetaId() {
		return metaId;
	}
	public int getPjId() {
		return pjId;
	}
	public String getColumn() {
		return column;
	}
	public String getDataType() {
		return dataType;
	}
	
	public void setMetaId(int metaId) {
		this.metaId = metaId;
	}
	public void setPjId(int pjId) {
		this.pjId = pjId;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
}
