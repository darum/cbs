package cbs.web.service.packet.databean;

public class ProjDataBean {

	/** プロジェクトID */
	private int projId;
	/** プロジェクト名 */
	private String name;
	/** プロジェクトユーザID */
	private int userId;
	/** 権限 */
	private int permission;
	/** プロジェクトオーナーのSystemID */
	private int ownerId;
	/** プロジェクトオーナーのアカウント名 */
	private String ownerAccount = "";
	
	/* for JAXB */
	public ProjDataBean() {
		
	}
	/**
	 * コンストラクタ
	 * @param id プロジェクトID
	 * @param name プロジェクト名
	 * @param pjUserId プロジェクトユーザID
	 * @param permission 権限
	 */
	public ProjDataBean(int id, String name, int pjUserId, int permission) {
		super();
		this.projId = id;
		this.name = name;
		this.userId = pjUserId;
		this.permission = permission;
	}
	
	/* getter */
	public String getName() {
		return name;
	}
	public int getPermission() {
		return permission;
	}
	public int getUserId() {
		return userId;
	}
	public int getProjId() {
		return projId;
	}
	public int getOwnerId() {
		return ownerId;
	}
	public String getOwnerAccount() {
		return ownerAccount;
	}
	
	/* setter */
	public void setProjId(int projId) {
		this.projId = projId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	public void setOwnerAccount(String account) {
		this.ownerAccount = account;
	}
}
