package cbs.web.service.packet.provider;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import cbs.web.service.packet.ProjControl;
import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONException;

@Provider()
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProjControlJSONProvider 
		implements MessageBodyWriter<ProjControl>, MessageBodyReader<ProjControl> {

	@Override
	public long getSize(ProjControl arg0, Class<?> arg1, Type arg2, Annotation[] arg3,
			MediaType arg4) {
		try{
			return JSON.encode(arg0).getBytes("UTF-8").length;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (arg0 == ProjControl.class);
	}

	@Override
	public void writeTo(ProjControl obj, Class<?> arg1, Type arg2, Annotation[] arg3,
			MediaType arg4, MultivaluedMap<String, Object> arg5,
			OutputStream outStream) throws IOException, WebApplicationException {
		JSON.encode(obj, outStream);
	}

	@Override
	public boolean isReadable(Class<?> type, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (type == ProjControl.class);
	}

	@Override
	public ProjControl readFrom(Class<ProjControl> arg0, Type arg1,
			Annotation[] arg2, MediaType arg3,
			MultivaluedMap<String, String> arg4, InputStream inStream)
			throws IOException, WebApplicationException {
		
		try {
			return JSON.decode(inStream, ProjControl.class);
		} catch(JSONException e) {
			throw new RuntimeException(e);
		}
	}

}
