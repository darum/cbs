package cbs.web.service.packet.databean;

public class MetaDataBean {
	
	/**
	 * コンストラクタ(for JAXB)
	 */
	public MetaDataBean() {
		
	}
	
	/* メンバ */
	/** カラムID */
	private int id;
	/** カラム名称 */
	private String name;
	/** カラムデータタイプ */
	private int dataType;
	/** カラムデータ */
	private String data;
	
	/* getter */
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public int getDataType() {
		return dataType;
	}
	public String getData() {
		return data;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setDataType(int dataType) {
		this.dataType = dataType;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	/* getter/setter */
}
