package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ResCtrl extends AbstractPacket {
	/* 定数 */
	public static final int CBS_WS_STAT_INVALID_USER_ID = CBS_WS_STAT_DEDICATED_BASE + 1;
	public static final int CBS_WS_STAT_MULTIPLE_RES_NAME = CBS_WS_STAT_DEDICATED_BASE + 2;
	
	/* メンバ */
	private int userId;
	private int resId;
	private String resName;
	
	/**
	 * コンストラクタ（for JAXB)
	 */
	public ResCtrl() {
		
	}
	
	/**
	 * コンストラクタ
	 * @param aUserId ユーザID
	 */
	public ResCtrl(int aUserId) {
		this.userId = aUserId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

}
