package cbs.web.service.packet.provider;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;

import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONException;

public abstract class AbstractCbsWsJSONProvider<T> implements
		MessageBodyReader<T>, MessageBodyWriter<T> {
	
	protected final String ENCODING = "UTF-8";
	
	abstract Class<?> getPacketType();

	@Override
	public long getSize(T t, Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType) {
		try {
			return JSON.encode(t).getBytes(ENCODING).length;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType) {
		return (type == getPacketType());
	}

	@Override
	public void writeTo(T t, Class<?> arg1, Type arg2, Annotation[] arg3,
			MediaType arg4, MultivaluedMap<String, Object> arg5,
			OutputStream stream) throws IOException, WebApplicationException {
		JSON.encode(t, stream);
	}

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations,
			MediaType mediaType) {
		return (type == getPacketType());
	}

	@Override
	public T readFrom(Class<T> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, String> httpHeaders,
			InputStream entityStream) throws IOException, WebApplicationException {
		try {
			return JSON.decode(entityStream, getPacketType());
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

}
