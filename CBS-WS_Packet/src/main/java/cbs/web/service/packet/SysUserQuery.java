package cbs.web.service.packet;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import cbs.web.service.packet.databean.UserDataBean;

@XmlRootElement
public class SysUserQuery extends AbstractPacket {
	
	/* メンバ */
	private String findAccount;
	private List<UserDataBean> result;
	
	/* 定数 */
	
	/* Consutructor */
	public SysUserQuery() {
		
	}
	
	public SysUserQuery(String find) {
		this.findAccount = find;
	}
	
	/* getter/setter */
	public String getFindAccount() {
		return findAccount;
	}
	public List<UserDataBean> getResult() {
		return result;
	}
	public void setFindAccount(String findAccount) {
		this.findAccount = findAccount;
	}
	public void setResult(List<UserDataBean> result) {
		this.result = result;
	}
}
