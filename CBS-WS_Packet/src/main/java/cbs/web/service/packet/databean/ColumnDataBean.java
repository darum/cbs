package cbs.web.service.packet.databean;

public class ColumnDataBean {
	private int id;
	private String value;
	
	public ColumnDataBean() {
		
	}
	
	public ColumnDataBean(int argId, String argValue) {
		id = argId;
		value = argValue;
	}							
	
	/* getter */
	public int getId() {
		return id;
	}
	public String getValue() {
		return value;
	}
	
	/* setter */
	public void setId(int id) {
		this.id = id;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
