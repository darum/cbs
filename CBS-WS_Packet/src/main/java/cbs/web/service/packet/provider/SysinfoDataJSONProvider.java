package cbs.web.service.packet.provider;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

import cbs.web.service.packet.SysinfoData;
import net.arnx.jsonic.JSON;

public class SysinfoDataJSONProvider implements MessageBodyWriter<SysinfoData> {

	@Override
	public long getSize(SysinfoData arg0, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4) {
		try{
			return JSON.encode(arg0).getBytes("UTF-8").length;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (arg0 == SysinfoData.class);
	}

	@Override
	public void writeTo(SysinfoData obj, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4,
			MultivaluedMap<String, Object> arg5, OutputStream outStream)
			throws IOException, WebApplicationException {
		JSON.encode(obj, outStream);
	}

}
