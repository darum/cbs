package cbs.web.service.packet;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import cbs.web.service.packet.databean.ColumnDataBean;

@XmlRootElement
public class TaskControl extends AbstractPacket {
	public static final int CBS_WS_STAT_INVALID_PJID = CBS_WS_STAT_DEDICATED_BASE + 1;	// PJ ID不正
	public static final int CBS_WS_STAT_INVALID_TASKID = CBS_WS_STAT_DEDICATED_BASE + 2;	// タスクID不正
	public static final int CBS_WS_STAT_PARENT_NOT_FOUND = CBS_WS_STAT_DEDICATED_BASE + 3;	// 親ID不正
	public static final int CBS_WS_STAT_INVALID_RES_ID = CBS_WS_STAT_DEDICATED_BASE + 4;	// リソースID不正
	public static final int CBS_WS_STAT_INVALID_DATE = CBS_WS_STAT_DEDICATED_BASE + 5;		// 日付不正
	public static final int CBS_WS_STAT_INVALID_PROG = CBS_WS_STAT_DEDICATED_BASE + 6;		// 進捗率不正
	public static final int CBS_WS_STAT_INVALID_COLUMN_ID = CBS_WS_STAT_DEDICATED_BASE + 7;	// カラムID不正
	
	public TaskControl() {
	}
	
	/**
	 * コンストラクタ(/task_ctrl/add用)
	 * @param pjId プロジェクトID
	 * @param name タスク名
	 */
	public TaskControl(int pjId, String name) {
		this.pjId = pjId;
		this.taskName = name;
		
	}

	/**
	 * コンストラクタ(/task_ctrl/modify用）
	 * @param taskId タスクID
	 */
	public TaskControl(int taskId) {
		this.taskId = taskId;
	}
	
	private int taskId;
	private int pjId;
	private String taskName;
	private Integer parentTaskId = null;	// 親タスク：編集対象でない場合はnull。0は親タスク無しを示す
	private int resId;
	private long startDT;		// システムの時刻表現（ms）
	private long termDT;
	private Double prog = null;
//	private HashMap<Integer, String> columns;
	private List<ColumnDataBean> columns;

	public int getTaskId() {
		return taskId;
	}

	public int getPjId() {
		return pjId;
	}

	public String getTaskName() {
		return taskName;
	}

	public Integer getParentTaskId() {
		return parentTaskId;
	}

	public int getResId() {
		return resId;
	}

	public long getStartDT() {
		return startDT;
	}

	public long getTermDT() {
		return termDT;
	}

	public Double getProg() {
		return prog;
	}

	public List<ColumnDataBean> getColumns() {
		return columns;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public void setPjId(int pjId) {
		this.pjId = pjId;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setParentTaskId(Integer parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public void setStartDT(long startDT) {
		this.startDT = startDT;
	}

	public void setTermDT(long termDT) {
		this.termDT = termDT;
	}

	public void setProg(Double prog) {
		this.prog = prog;
	}

	public void setColumns(List<ColumnDataBean> columns) {
		this.columns = columns;
	}

}
