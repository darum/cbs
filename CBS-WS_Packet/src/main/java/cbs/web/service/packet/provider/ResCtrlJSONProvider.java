package cbs.web.service.packet.provider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import cbs.web.service.packet.ResCtrl;

@Provider()
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ResCtrlJSONProvider extends AbstractCbsWsJSONProvider<ResCtrl> {

	@Override
	Class<?> getPacketType() {
		return ResCtrl.class;
	}
	
}
