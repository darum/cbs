package cbs.web.service.packet;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import cbs.web.service.packet.databean.PjresDataBean;

/**
 * /pjres_query 用パケット
 * @author Yamashita
 *
 */
@XmlRootElement
public class ProjResQuery extends AbstractPacket {
	/** Project ID */
	private int pjId;
	/** Result Data */
	private List<PjresDataBean> list;

	/* Error Code */
	public static final int CBS_WS_STAT_INVALID_PJID = CBS_WS_STAT_DEDICATED_BASE + 1;
	
	public ProjResQuery() {
		
	}
	
	public ProjResQuery(int project) {
		pjId = project;
	}

	/* getter */
	public int getPjId() {
		return pjId;
	}
	public List<PjresDataBean> getList() {
		return list;
	}

	/* setter */
	public void setPjId(int pjId) {
		this.pjId = pjId;
	}
	public void setList(List<PjresDataBean> list) {
		this.list = list;
	}
}
