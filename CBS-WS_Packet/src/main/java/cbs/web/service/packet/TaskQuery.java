package cbs.web.service.packet;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import cbs.web.service.packet.databean.TaskDataBean;

/**
 * /task_query 用パケットデータ
 * @author Yamashita
 *
 */
@XmlRootElement
public class TaskQuery extends AbstractPacket {
	
	public static final int CBS_WS_STAT_INVALID_PROJECT_ID = CBS_WS_STAT_DEDICATED_BASE + 1;
	public static final int CBS_WS_STAT_INVALID_TASK_ID = CBS_WS_STAT_DEDICATED_BASE + 2;
	public static final int CBS_WS_STAT_INVALID_START_POS = CBS_WS_STAT_DEDICATED_BASE + 3;
	public static final int CBS_WS_STAT_INVALID_COUNT = CBS_WS_STAT_DEDICATED_BASE + 4;
	
	/** プロジェクトID */
	private int projId;
	/** タスクID */
	private int taskId;
	/** 取得開始位置 */
	private int queryStart;
	/** 取得件数 */
	private int queryCount;
	/** タスク情報 */
	private List<TaskDataBean> task;

	/**
	 * コンストラクタ(JAXB用)
	 */
	public TaskQuery() {
	}

	/**
	 * コンストラクタ
	 * @param proj_id プロジェクトID
	 * @param task_id 親タスクID (0: ルート)
	 */
	public TaskQuery(int proj_id, int task_id) {
		this.projId = proj_id;
		this.taskId = task_id;
	}

	public int getProjId() {
		return projId;
	}

	public int getTaskId() {
		return taskId;
	}

	public int getQueryStart() {
		return queryStart;
	}

	public int getQueryCount() {
		return queryCount;
	}

	public List<TaskDataBean> getTask() {
		return task;
	}

	public void setProjId(int projId) {
		this.projId = projId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public void setQueryStart(int queryStart) {
		this.queryStart = queryStart;
	}

	public void setQueryCount(int queryCount) {
		this.queryCount = queryCount;
	}

	public void setTask(List<TaskDataBean> task) {
		this.task = task;
	}
	
}
