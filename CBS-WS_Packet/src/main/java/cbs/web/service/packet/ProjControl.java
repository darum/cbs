package cbs.web.service.packet;

public class ProjControl extends AbstractPacket {
	public static final int CBS_WS_STAT_MULTIPLE_NAME = CBS_WS_STAT_DEDICATED_BASE + 1;
	public static final int CBS_WS_STAT_PROJID_ERROR = CBS_WS_STAT_DEDICATED_BASE + 2;
	
	public ProjControl() {
		
	}
	
	/**
	 * コンストラクタ：delete用
	 * @param id プロジェクトID
	 */
	public ProjControl(int id) {
		this.projId = id;
	}
	
	/**
	 * コンストラクタ：add用
	 * @param name プロジェクト名
	 */
	public ProjControl(String name) {
		this.name = name;
	}
	
	/* メンバ */
	private int projId;
	private String name;
	
	public int getProjId() {
		return projId;
	}
	public String getName() {
		return name;
	}
	
	public void setProjId(int projId) {
		this.projId = projId;
	}
	public void setName(String name) {
		this.name = name;
	}

}
