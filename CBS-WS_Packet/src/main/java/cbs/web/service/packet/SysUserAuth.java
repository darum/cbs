package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SysUserAuth extends AbstractPacket {
	
	/* 定数 */
	public static final int CBS_WS_STAT_AUTH_ERROR = CBS_WS_STAT_DEDICATED_BASE + 1;
	
	private int userId;
	private String account;
	private String password;

	/**
	 * for JAXB
	 */
	public SysUserAuth() {
		
	}
	
	/**
	 * コンストラクタ /auth/login用
	 * @param account アカウント
	 * @param password パスワード
	 */
	public SysUserAuth(String account, String password) {
		this.account = account;
		this.password = password;
	}
	
	/**
	 * コンストラクタ
	 * @param id
	 * @param account
	 */
	public SysUserAuth(int id, String account) {
		this.userId = id;
		this.account = account;
	}
	
	/**
	 * コンストラクタ(ログアウト用)
	 * @param id ユーザID
	 */
	public SysUserAuth(int id) {
		this.userId = id;
	}
	
	/* アクセサ */
	public int getUserId() {
		return userId;
	}
	public String getAccount() {
		return account;
	}
	public String getPassword() {
		return password;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
