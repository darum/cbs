package cbs.web.service.packet.provider;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;

import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONException;
import cbs.web.service.packet.TaskQuery;

public class TaskQueryJSONProvider implements MessageBodyWriter<TaskQuery>,
MessageBodyReader<TaskQuery> {

	@Override
	public boolean isReadable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return (type == TaskQuery.class);
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return (type == TaskQuery.class);
	}

	@Override
	public TaskQuery readFrom(Class<TaskQuery> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		try {
			return JSON.decode(entityStream, TaskQuery.class);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long getSize(TaskQuery t, Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		try {
			return JSON.encode(t).getBytes("UTF-8").length;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void writeTo(TaskQuery t, Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders,
			OutputStream entityStream) throws IOException,
			WebApplicationException {
		JSON.encode(t, entityStream);
	}

}
