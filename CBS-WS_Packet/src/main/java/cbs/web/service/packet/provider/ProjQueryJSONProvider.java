package cbs.web.service.packet.provider;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONException;
import cbs.web.service.packet.ProjQuery;

@Provider
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProjQueryJSONProvider 
		implements MessageBodyWriter<ProjQuery>, MessageBodyReader<ProjQuery> {

	@Override
	public boolean isReadable(Class<?> arg0, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (arg0 == ProjQuery.class);
	}

	@Override
	public ProjQuery readFrom(Class<ProjQuery> arg0, Type arg1,
			Annotation[] arg2, MediaType arg3,
			MultivaluedMap<String, String> arg4, InputStream arg5)
			throws IOException, WebApplicationException {
		try {
			return JSON.decode(arg5, ProjQuery.class);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long getSize(ProjQuery arg0, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4) {
		try {
			return JSON.encode(arg0).getBytes("UTF-8").length;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean isWriteable(Class<?> arg0, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (arg0 == ProjQuery.class);
	}

	@Override
	public void writeTo(ProjQuery arg0, Class<?> arg1, Type arg2,
			Annotation[] arg3, MediaType arg4,
			MultivaluedMap<String, Object> arg5, OutputStream arg6)
			throws IOException, WebApplicationException {
		JSON.encode(arg0, arg6);
	}

}
