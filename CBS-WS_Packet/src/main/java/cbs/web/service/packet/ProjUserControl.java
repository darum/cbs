package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ProjUserControl extends AbstractPacket {
	
	/**
	 * コンストラクタ (for JAXB)
	 */
	public ProjUserControl() {
		
	}
	
	/**
	 * コンストラクタ
	 * @param sysUserId システムユーザID 
	 * @param projId プロジェクトID
	 */
	public ProjUserControl(int sysUserId, int projId) {
		this.sysUserId = sysUserId;
		this.pjId = projId;
	}
	
	public ProjUserControl(int pjUserId) {
		this.pjUserid = pjUserId;
	}
	
	/* 定数 */
	public static final int CBS_WS_STAT_INVALID_SYSUSER = CBS_WS_STAT_DEDICATED_BASE + 1;
	public static final int CBS_WS_STAT_INVALID_PROJ_ID = CBS_WS_STAT_DEDICATED_BASE + 2;
	public static final int CBS_WS_STAT_INVALID_PJUSER_ID = CBS_WS_STAT_DEDICATED_BASE + 3;
	
	/* メンバ */
	private int sysUserId;
	private int pjId;
	private int pjUserid;
	private int permission;
	
	/* getter/setter */
	public int getSysUserId() {
		return sysUserId;
	}
	public void setSysUserId(int sysUserId) {
		this.sysUserId = sysUserId;
	}
	public int getPjId() {
		return pjId;
	}
	public int getPjUserId() {
		return this.pjUserid;
	}
	public void setPjId(int pjId) {
		this.pjId = pjId;
	}
	public int getPermission() {
		return permission;
	}
	public void setPermission(int permission) {
		this.permission = permission;
	}
	public void setPjUserId(int pjUserId) {
		this.pjUserid = pjUserId;
	}
}
