package cbs.web.service.packet;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import cbs.web.service.packet.databean.ProjDataBean;

@XmlRootElement
public class ProjQuery extends AbstractPacket {
	/* メンバ */
	private int userId;
	private List<ProjDataBean> result;
	
	/* 定数 */
	public static int CBS_WS_STAT_INVALID_USERID = CBS_WS_STAT_DEDICATED_BASE + 1;
	
	/* コンストラクタ */
	public ProjQuery() {
		
	}
	
	public ProjQuery(int userId) {
		this.userId = userId;
	}
	
	/* getter/setter */
	public int getUserId() {
		return userId;
	}
	public List<ProjDataBean> getResult() {
		return result;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setResult(List<ProjDataBean> result) {
		this.result = result;
	}
}
