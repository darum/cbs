package cbs.web.service.packet.databean;

public class PjresDataBean {

	private int pjResId;
	private int resId;
	private String resName;
	
	/* setter */
	public void setPjResId(int pjResId) {
		this.pjResId = pjResId;
	}
	public void setResId(int resId) {
		this.resId = resId;
	}
	public void setResName(String resName) {
		this.resName = resName;
	}
	
	/* getter */
	public int getPjResId() {
		return pjResId;
	}
	public int getResId() {
		return resId;
	}
	public String getResName() {
		return resName;
	}
}
