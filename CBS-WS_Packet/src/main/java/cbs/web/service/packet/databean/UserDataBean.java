package cbs.web.service.packet.databean;

public class UserDataBean {
	private int userId = 0;
	private String account = "";
	private String password = "";
	private String email = "";
	private String firstName = "";
	private String lastName = "";
	
	/* getter/setter */
	public int getUserId() {
		return userId;
	}
	public String getAccount() {
		return account;
	}
	public String getPassword() {
		return password;
	}
	public String getEmail() {
		return email;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
