package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * /pjres_ctrl用パケット構造体
 * @author Yamashita
 *
 */
@XmlRootElement
public class ProjResCtrl extends AbstractPacket {
	
	/* 定数 */
	public static final int CBS_WS_STAT_INVALID_PJRESID = CBS_WS_STAT_DEDICATED_BASE + 1;
	public static final int CBS_WS_STAT_INVALID_PROJID = CBS_WS_STAT_DEDICATED_BASE + 2;
	public static final int CBS_WS_STAT_MULTIPLE_RESNAME = CBS_WS_STAT_DEDICATED_BASE + 3;
	public static final int CBS_WS_STAT_INVALID_RESID = CBS_WS_STAT_DEDICATED_BASE + 4;

	public ProjResCtrl() {
		
	}
	
	/**
	 * コンストラクタ（add用）
	 * @param pjId プロジェクトID
	 * @param resourceId リソースID
	 */
	public ProjResCtrl(int pjId, int resourceId) {
		this.projId = pjId;
		this.resId = resourceId;
	}
	
	/**
	 * コンストラクタ（modify/del用）
	 * @param resId RES ID
	 */
	public ProjResCtrl(int resId) {
		this.resId = resId;
	}
	
	/* メンバ */
	/** プロジェクトリソースID */
	private int pjResId;
	/** プロジェクトID */
	private int projId;
	/** リソースID */
	private int resId;

	/* getter/setter */
	public int getResId() {
		return resId;
	}
	public int getProjId() {
		return projId;
	}
	public int getPjResId() {
		return pjResId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}
	public void setProjId(int projId) {
		this.projId = projId;
	}
	public void setPjResId(int pjResId) {
		this.pjResId = pjResId;
	}
}
