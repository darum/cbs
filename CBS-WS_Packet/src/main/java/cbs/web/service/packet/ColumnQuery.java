package cbs.web.service.packet;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import cbs.web.service.packet.databean.MetaDataBean;

@XmlRootElement
public class ColumnQuery extends AbstractPacket {
	
	public static final int CBS_WS_STAT_INVALID_PROJECT_ID = CBS_WS_STAT_DEDICATED_BASE + 1;

	/* コンストラクタ */
	public ColumnQuery() {
		
	}
	
	/**
	 * コンストラクタ
	 * @param pjId プロジェクトID
	 */
	public ColumnQuery(int pjId) {
		this.projId = pjId;
	}
	
	/* メンバ */
	private int projId;
	private List<MetaDataBean> columns;
	
	/* getter/setter */
	public int getProjId() {
		return projId;
	}
	public List<MetaDataBean> getColumns() {
		return columns;
	}
	public void setProjId(int projId) {
		this.projId = projId;
	}
	public void setColumns(List<MetaDataBean> columns) {
		this.columns = columns;
	}
}
