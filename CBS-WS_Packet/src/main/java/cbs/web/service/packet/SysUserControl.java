package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SysUserControl extends AbstractPacket implements Cloneable {
	
	/* 定数 */
	public static final int CBS_WS_STAT_ACCOUNT_DUPLICATED = CBS_WS_STAT_DEDICATED_BASE + 1;	// アカウント重複
	public static final int CBS_WS_START_USERID_INVALID = CBS_WS_STAT_DEDICATED_BASE + 2;

	/* メンバ */
	private int userId = 0;
	private String account = "";
	private String password = "";
	private String email = "";
	private String firstName = "";
	private String lastName = "";

	/**
	 * for JAXB
	 */
	public SysUserControl() {
		
	}
	
	/**
	 * コンストラクタ（add用）
	 * @param account
	 * @param password
	 * @param email
	 */
	public SysUserControl(String account, String password, String email) {
		super();
		this.account = account;
		this.password = password;
		this.email = email;
	}
	
	/**
	 * modify, del用コンストラクタ
	 * @param userId ユーザID
	 */
	public SysUserControl(int userId) {
		super();
		this.userId = userId;
	}
	
	public SysUserControl clone() {
		try {
			return (SysUserControl) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}
	

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
}
