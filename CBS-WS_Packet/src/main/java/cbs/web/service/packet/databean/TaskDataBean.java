package cbs.web.service.packet.databean;

import java.util.Date;
import java.util.List;

public class TaskDataBean {
	
	/** タスクID */
	private int id;
	/** タスク名 */
	private String taskName;
	/** 担当リソースID */
	private int resId;
	/** 担当リソース名(担当者名) */
	private String resName;
	/** 開始日時 */
	private Date start;
	/** 終了日時 */
	private Date term;
	/** 進捗率 */
	private double progress;
	/** カラム情報 */
	private List<MetaDataBean> columns;
	
	/**
	 * コンストラクタ
	 */
	public TaskDataBean() {
		
	}

	/* getter */
	public int getId() {
		return id;
	}

	public String getTaskName() {
		return taskName;
	}

	public int getResId() {
		return resId;
	}

	public String getResName() {
		return resName;
	}

	public Date getStart() {
		return start;
	}

	public Date getTerm() {
		return term;
	}

	public double getProgress() {
		return progress;
	}

	public List<MetaDataBean> getColumns() {
		return columns;
	}

	/* setter */
	public void setId(int id) {
		this.id = id;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public void setTerm(Date term) {
		this.term = term;
	}

	public void setProgress(double progress) {
		this.progress = progress;
	}

	public void setColumns(List<MetaDataBean> columns) {
		this.columns = columns;
	}

}
