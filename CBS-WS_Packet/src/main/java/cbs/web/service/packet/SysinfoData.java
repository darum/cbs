package cbs.web.service.packet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SysinfoData {
	
	private String apiVersion = "";

	/*
	 * Constructor
	 */
	/**
	 * for JAXB
	 */
	public SysinfoData() {
		
	}

	/*
	 * Accesor
	 */
	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	
	
}
