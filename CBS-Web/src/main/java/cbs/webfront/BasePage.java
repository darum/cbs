package cbs.webfront;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public abstract class BasePage extends WebPage {
	/* 定数 */
	
	/**
	 * 遷移元のページ
	 */
	private WebPage prevPage = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 6245069932950611176L;

	public BasePage(final WebPage page) {
		super();
		this.prevPage = page;
	}

	public BasePage(final WebPage page, PageParameters parameters) {
		super(parameters);
		this.prevPage = page;
	}

	protected WebPage getPrevPage() {
		return prevPage;
	}

}
