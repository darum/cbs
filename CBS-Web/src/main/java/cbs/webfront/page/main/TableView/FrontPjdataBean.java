package cbs.webfront.page.main.TableView;

import java.io.Serializable;
import java.util.List;

/**
 * WebFront用プロジェクトデータ
 * @author Yamashita
 *
 */
@SuppressWarnings("serial")
public class FrontPjdataBean implements Serializable {
	
	/* メンバ */
	/** プロジェクトID */
	private int id;
	
	/** プロジェクト名 */
	private String name;
	
	/** オーナーアカウント名 */
	private String owner;
	
	/** カラムリスト */
	private List<String> columns;
	
	/** タスク情報: 表示用に全てのデータはStringで格納する */
	List<FrontTaskdataBean> taskList;
	
	/* コンストラクタ */
	/**
	 * コンストラクタ
	 * @param pjName プロジェクト名
	 * @param pjOwner プロジェクトオーナー名
	 * @param columnDefs プロジェクトのタスクカラム定義
	 * @param taskData タスクの情報
	 */
	public FrontPjdataBean(int aPjId, String pjName, String pjOwner, List<String> columnDefs, List<FrontTaskdataBean> taskData) {
		this.id = aPjId;
		this.name = pjName;
		this.owner = pjOwner;
		this.columns = columnDefs;
		this.taskList = taskData;
	}

	/* getter */
	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public List<String> getColumns() {
		return columns;
	}

	public List<FrontTaskdataBean> getTaskList() {
		return taskList;
	}

	public int getId() {
		return this.id;
	}
}
