package cbs.webfront.page.main.TreeView;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("serial")
public class TreeViewData implements Serializable {
	
	private List<ProjectDataDispBean> pjDataList;
	
	public TreeViewData(List<ProjectDataDispBean> aList) {
		pjDataList = aList;
	}
	
	
	/**
	 * IDに合致するデータを探す
	 * @param id ID
	 * @return なければnull
	 */
	public ProjectDataDispBean getData(String id) {
		return findData(this.pjDataList, id);
	}
	
	public Iterator<ProjectDataDispBean> getIterator() {
		return this.pjDataList.iterator();
	}
	/**
	 * リストから条件に合うデータを探す
	 * @return 見つかったデータ。該当無しはnull
	 */
	private static ProjectDataDispBean findData(List<ProjectDataDispBean> searchList, String id) {
		for (ProjectDataDispBean data : searchList) {
			if(data.getId().equals(id)) {
				return data;
			}
			
			// 子データの検索
			ProjectDataDispBean child = findData(data.getChildren(), id);
			if (child != null) {
				return child;
			}
		}
		
		return null;
	}
}
