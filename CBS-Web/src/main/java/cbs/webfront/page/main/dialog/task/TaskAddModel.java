package cbs.webfront.page.main.dialog.task;

import java.util.Date;

import org.apache.wicket.util.io.IClusterable;

@SuppressWarnings("serial")
public class TaskAddModel implements IClusterable{
	
	private String taskName;
	private String resource;
	private Date startDate;
	private Date termDate;

	public String getTaskName() {
		return taskName;
	}
	
	public String getResource() {
		return resource;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getTermDate() {
		return termDate;
	}
}
