package cbs.webfront.page.main.dialog.task;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;

import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.page.main.dialog.ICbsDialogCallback;

/**
 * タスク追加ダイアログ
 * @author darum
 *
 */
@SuppressWarnings("serial")
public class TaskAddDialog extends ModalWindow {
	private CbsWebPageParameters param;
	private ICbsDialogCallback callback;
	
	public TaskAddDialog(String id, String cookie, CbsWebPageParameters param, ICbsDialogCallback callback) {
		super(id);
		
		setCookieName(cookie);
		this.param = param;
		this.callback = callback;
	}
	
	public void show(AjaxRequestTarget target, int pjId, String pjName) {
		param.add("PROJECT_ID", pjId);
		param.add("PROJECT_NAME", pjName);
		
		setPageCreator(new ModalWindow.PageCreator() {
			@Override
			public Page createPage(){
				return new TaskAddPage(param, callback);
			}
		});
		
		super.show(target);
	}
}
