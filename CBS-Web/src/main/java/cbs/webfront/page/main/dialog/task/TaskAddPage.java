package cbs.webfront.page.main.dialog.task;

import java.util.Date;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow.WindowClosedCallback;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.packet.TaskControl;
import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.client.TaskAddRequest;
import cbs.webfront.page.main.dialog.ICbsDialogCallback;
import cbs.webfront.page.main.dialog.resource.ResourceAddPageCreator;

@SuppressWarnings("serial")
public class TaskAddPage extends WebPage {
	
	private String strPjName;
	private String account;
	private String password;
	private int pjId;

	// Component
	private FeedbackPanel feedback;
	private ModalWindow resourceAddDialog;
	
	// Callback
	ICbsDialogCallback callback;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * paramsの中身
	 * PROJECT_ID: プロジェクトID
	 * PROJECT_NAME: プロジェクト名
	 * CbsWebPageParameters.PARAM_ACCOUNT: アカウント
	 * CbsWebPageParameters.PARAM_PASSWORD: パスワード
	 * @param params
	 */
	public TaskAddPage(PageParameters params, ICbsDialogCallback callback) {
		strPjName = params.get("PROJECT_NAME").toString();
		account = params.get(CbsWebPageParameters.PARAM_ACCOUNT).toString();
		password = params.get(CbsWebPageParameters.PARAM_PASSWORD).toString();
		pjId = params.get("PROJECT_ID").toInt();
		
		LoggerFactory.getLogger(getClass()).debug("TaskAddPage id={}, name={}", pjId, strPjName);
		
		this.callback = callback;
		
		// タイトル
		add(new Label("title", String.format(getString("title"), strPjName)));
		
		// プロジェクト名
		Label projectName = new Label("projectName", strPjName);
		this.add(projectName);
		
		// Feedback Panel
		feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);
		add(feedback);
		
		// Dialog
		resourceAddDialog = new ModalWindow("resAddDlg");
		add(resourceAddDialog);
		resourceAddDialog.setCookieName("resAddDlog-01");
		resourceAddDialog.setPageCreator(new ResourceAddPageCreator(params));
		resourceAddDialog.setWindowClosedCallback(new WindowClosedCallback(){
			@Override
			public void onClose(AjaxRequestTarget target) {
				target.add(TaskAddPage.this);
			}
		});
		
		
		// Form
		Form<?> taskAdd = new TaskAddForm("taskAdd");
		add(taskAdd);
		// リソース追加
		AjaxLink<Void> resAdd = new AjaxLink<Void>("resAdd"){

			@Override
			public void onClick(AjaxRequestTarget target) {
				resourceAddDialog.show(target);
			}
			
		};
		taskAdd.add(resAdd);
		
	}

	class TaskAddForm extends Form<TaskAddModel> {

		public TaskAddForm(String id) {
			super(id, new CompoundPropertyModel<TaskAddModel>(new TaskAddModel()));
			
			// タスク名
			final RequiredTextField<String> taskNameComponent = new RequiredTextField<String>("taskName");
			add(taskNameComponent);
			
			// 担当者
			TextField<String> resource = new TextField<String>("resource");
			add(resource);
			
			// 開始日
			DateTextField startDate = new DateTextField("startDate", "yyyy/MM/dd");
			startDate.add(new DatePicker());
			add(startDate);
			
			// 終了日
			DateTextField termDate = new DateTextField("termDate", "yyyy/MM/dd");
			termDate.add(new DatePicker());
			add(termDate);
			
			AjaxButton apply = new AjaxButton("apply"){
				@Override
				protected void onError(AjaxRequestTarget target, Form<?> form)
				{
					target.add(feedback);
				}
				
				@Override
				protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
					int wsStatus = -1;

					// タスクの追加
					TaskAddModel model = TaskAddForm.this.getModelObject();
					String taskName = model.getTaskName();
					Date startDate = model.getStartDate();
					Date termDate = model.getTermDate();
					try {
						logger.debug("Start add peocess. TaskName=[{}]", taskName);
						TaskAddRequest taskAddRequest = new TaskAddRequest(account, password, pjId, taskName);
						if (startDate != null) {
							taskAddRequest.setStartDT(startDate.getTime());
						}
						if (termDate != null) {
							taskAddRequest.setTermDT(termDate.getTime());
						}
						// TODO 担当リソースID
						taskAddRequest.execute();
						wsStatus = taskAddRequest.getWsStatus();
						
						switch(wsStatus) {
						case TaskControl.CBS_WS_STAT_SUCCESS:
							break;
						default:
							logger.error("タスク追加エラー: Status={}", wsStatus);
							taskNameComponent.error("タスク追加エラー: Status=" + wsStatus);
							break;
						}
					} catch (Exception e) {
						taskNameComponent.error("タスク追加エラー:" + e.getMessage());
						logger.error("Task Add Error", e);
					}
					
					// ウィンドウを閉じる
					if (wsStatus == TaskControl.CBS_WS_STAT_SUCCESS) {
						callback.onAccept(target);
					} else {
						target.add(feedback);
					}
				}
				
				
			};
			add(apply);
			
			Button buttonClose = new Button("close");
			buttonClose.add(new AjaxEventBehavior("onclick") {

				@Override
				protected void onEvent(AjaxRequestTarget target) {
					callback.onCancel(target);
				}
				
			});
			add(buttonClose);
		}
		
	}
	
	public String getPjName() {
		return strPjName;
	}

}
