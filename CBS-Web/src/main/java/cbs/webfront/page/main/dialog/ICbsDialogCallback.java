package cbs.webfront.page.main.dialog;

import java.io.Serializable;

import org.apache.wicket.ajax.AjaxRequestTarget;

public interface ICbsDialogCallback extends Serializable {
	/**
	 * OKボタンによるコールバック
	 * @param target
	 */
	void onAccept(AjaxRequestTarget target);
	
	/**
	 * キャンセル処理によるコールバック
	 * @param target
	 */
	void onCancel(AjaxRequestTarget target);

}

