package cbs.webfront.page.main.TreeView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * TreeView表示用Bean
 * @author darum
 *
 */
@SuppressWarnings("serial")
public class ProjectDataDispBean implements Serializable
{
	/** ID キーになる */
	private String id;
	/** タスク名 */
	private String taskName;
	/** 担当者 */
	private String resName;
	/** 開始日 */
	private Date start;
	/** 終了日 */
	private Date term;
	
//	private boolean quux;

	private boolean loaded;

	private ProjectDataDispBean parent;

	/** 子データ */
	private List<ProjectDataDispBean> children = new ArrayList<ProjectDataDispBean>();

	public ProjectDataDispBean(String id, String taskName, String resName)
	{
		this.id = id;
		this.taskName = taskName;
		this.resName = resName;
	}

	// TODO このコンストラクタは廃止して、親タスクにAddする形を取る
//	public ProjectDataDispBean(ProjectDataDispBean parent, String name, String taskName, String resName)
//	{
//		this(name, taskName, resName);
//
//		this.parent = parent;
//		this.parent.children.add(this);
//	}

	/**
	 * 子タスクの追加
	 * @param child 追加するタスク
	 */
	public void addChild(ProjectDataDispBean child) {
		// 子タスクの追加
		this.children.add(child);
		
		// 子タスク側に親(this)をセット
		child.parent = this;
	}
	
	public ProjectDataDispBean getParent()
	{
		return parent;
	}

	public String getId()
	{
		return id;
	}

//	public void setQuux(boolean quux)
//	{
//		this.quux = quux;
//
//		if (quux)
//		{
//			// set quux on all descendants
//			for (ProjectDataDispBean foo : children)
//			{
//				foo.setQuux(true);
//			}
//		}
//		else
//		{
//			// clear quux on all ancestors
//			if (parent != null)
//			{
//				parent.setQuux(false);
//			}
//		}
//	}
//
//	public boolean getQuux()
//	{
//		return quux;
//	}

	public List<ProjectDataDispBean> getChildren()
	{
		return Collections.unmodifiableList(children);
	}

	@Override
	public String toString()
	{
		return id;
	}

	public boolean isLoaded()
	{
		return loaded;
	}

	public void setLoaded(boolean loaded)
	{
		this.loaded = loaded;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getTerm() {
		return term;
	}

	public void setTerm(Date term) {
		this.term = term;
	}
}
