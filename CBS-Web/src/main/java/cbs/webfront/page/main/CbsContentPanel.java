package cbs.webfront.page.main;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import cbs.webfront.CbsWebPageParameters;

@SuppressWarnings("serial")
public abstract class CbsContentPanel extends Panel {
	
	private String account;
	private String password;

	public CbsContentPanel(String id, final CbsWebPageParameters param) {
		super(id);
		account = param.get(CbsWebPageParameters.PARAM_ACCOUNT).toString();
		password = param.get(CbsWebPageParameters.PARAM_PASSWORD).toString();
	}
	
	public CbsContentPanel(String id, final CbsWebPageParameters param, IModel<?> model) {
		super(id, model);
		account = param.get(CbsWebPageParameters.PARAM_ACCOUNT).toString();
		password = param.get(CbsWebPageParameters.PARAM_PASSWORD).toString();
	}

	public String getAccount() {
		return account;
	}

	public String getPassword() {
		return password;
	}

}
