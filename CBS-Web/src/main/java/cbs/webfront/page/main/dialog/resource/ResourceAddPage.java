package cbs.webfront.page.main.dialog.resource;

import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import cbs.webfront.CbsWebPageParameters;

@SuppressWarnings("serial")
public class ResourceAddPage extends WebPage {
	// Parameter
	private String account;
	private String password;

	// Component
	private FeedbackPanel feedback;
	
	
	public ResourceAddPage(PageParameters params) {
		account = params.get(CbsWebPageParameters.PARAM_ACCOUNT).toString();
		password = params.get(CbsWebPageParameters.PARAM_PASSWORD).toString();

		// Feedback Panel
		feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);
		add(feedback);
		
		ProjectResourceBean bean = new ProjectResourceBean();
		
		// Form
		Form<ProjectResourceBean> resAdd = new Form<ProjectResourceBean>("form", new CompoundPropertyModel<ProjectResourceBean>(bean));
		add(resAdd);
		
		resAdd.add(new TextField<String>("resName"));
		
		resAdd.add(new AjaxButton("apply"){
			
		});
		resAdd.add(new AjaxButton("close"){
			
		});
	}

}
