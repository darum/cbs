package cbs.webfront.page.main.TableView;

import java.io.Serializable;

/**
 * カラムデータBean
 * @author darum
 *
 */
@SuppressWarnings("serial")
public class ColumnData implements Serializable {
	
	private String value;
	
	public ColumnData(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
