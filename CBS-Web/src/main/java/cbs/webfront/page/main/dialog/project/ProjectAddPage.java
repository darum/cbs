package cbs.webfront.page.main.dialog.project;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnLoadHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.packet.ProjControl;
import cbs.web.service.packet.ProjUserControl;
import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.client.PjAddRequest;
import cbs.webfront.client.PjuserAddRequest;
import cbs.webfront.page.main.dialog.ICbsDialogCallback;

/**
 * プロジェクト追加ページ（ダイアログ）
 * @author Yamashita
 *
 */
public class ProjectAddPage extends WebPage {

	/** */
	private static final long serialVersionUID = -2937689053317547820L;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	// Parameter
	private String account;
	private String password;
	private ICbsDialogCallback callback;
	
	// Component
	private FeedbackPanel feedback;
	
	private Model<String> modelPjName;
	
	/**
	 * コンストラクタ
	 * @param param パラメータ
	 * @param callback 処理終了時のコールバック
	 */
	public ProjectAddPage(final CbsWebPageParameters param, ICbsDialogCallback callback) {
		// Parameters
		this.callback = callback;
		
		account = param.get(CbsWebPageParameters.PARAM_ACCOUNT).toString();
		password = param.get(CbsWebPageParameters.PARAM_PASSWORD).toString();
		
		// Component
		super.add(new Label("title", "プロジェクト追加"));
		this.feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);
		super.add(feedback);
		
		super.add(new InputForm("pjAddForm"));
		
		// ESCボタン
		super.add(new CloseOnESCBehavior(callback));
	}
	
	/**
	 * Form
	 * @author Yamashita
	 *
	 */
	private class InputForm extends Form<PjAddModel> {
		/** */
		private static final long serialVersionUID = 7119177099773479482L;
		
		// for Validator
		private int wsStatus;
		private String addPjName;
		private int addPjId;
		
		@SuppressWarnings("serial")
		public InputForm(String name) {
			super(name, new CompoundPropertyModel<PjAddModel>(new PjAddModel()));
			
			/* projectName 入力 */
			modelPjName = new Model<String>(new String());
			final RequiredTextField<String> projectName = new RequiredTextField<String>("projectName", modelPjName);
			super.add(projectName);
			
			/* Apply Button */
			AjaxButton applyButton = new AjaxButton("applyButton"){
				/** */
				private static final long serialVersionUID = 7211458915258418929L;

				/**
				 * TODO メソッド化
				 */
				@Override
				protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
					logger.debug("onSubmit()");
					try {
						String pjName = modelPjName.getObject();
						if (!"".equals(pjName)){
							try {
								/* /pj_ctrl/add処理 */
								logger.debug("Try to add. ProjectName=[{}]", pjName);
								PjAddRequest pjAddRequest = new PjAddRequest(account, password, pjName);
							
								pjAddRequest.execute();
								wsStatus = pjAddRequest.getWsStatus();
								
								if (wsStatus == ProjControl.CBS_WS_STAT_SUCCESS) {
									addPjId = pjAddRequest.getProjId();
								} else if (wsStatus == ProjControl.CBS_WS_STAT_MULTIPLE_NAME) {
									logger.warn("すでにプロジェクト名は登録されています: {}", addPjName);
									projectName.error("すでにプロジェクト名は登録されています");
								} else {
									logger.error("プロジェクト追加エラー：Status={}", wsStatus);
									projectName.error("プロジェクト追加エラー：Status=" + wsStatus);
								}
							} catch (Exception e) {
								logger.error("致命的なエラー", e);
							}
						}
						// PJ追加はonvalidate()で実施済み
						if (wsStatus == ProjControl.CBS_WS_STAT_SUCCESS) {
							
							/* Pjuser登録 */
							PjuserAddRequest pjuserAdd = 
									new PjuserAddRequest(account, password, addPjId, 1);	// TODO 権限
							pjuserAdd.execute();
							
							if (pjuserAdd.getWsStatus() != ProjUserControl.CBS_WS_STAT_SUCCESS) {
								logger.error("致命的なエラー");
								error("致命的なエラーです");
							} else {
								// すべて完了：ウィンドウを閉じる
								logger.debug("Register Process successfule.");		
								callback.onAccept(target);
							}
						} else {
							// FeedbackPanelを更新する
							target.add(feedback);
						}
						
					} catch (Exception e) {
						logger.error("致命的なエラー", e);
					}
				}
				
				@Override
				protected void onError(AjaxRequestTarget target, Form<?> form) {
					// ErrorMessage shows in Feedback Panel
					target.add(feedback);
				}
			};
			super.add(applyButton);
			
			/* Close Button */
			Button closeButton = new Button("closeButton");
			closeButton.add(new AjaxEventBehavior("onclick"){
				@Override
				protected void onEvent(AjaxRequestTarget target) {
					logger.debug("closeButton.onclick. target={}", target);

					callback.onCancel(target);
				}
				
			});
			super.add(closeButton);
			
		}
		
	}
	
    private static class CloseOnESCBehavior extends AbstractDefaultAjaxBehavior {
    	/** */
    	private static final long serialVersionUID = -2973256782308317435L;

    	ICbsDialogCallback callback;

    	public CloseOnESCBehavior(ICbsDialogCallback callback) {
    		this.callback = callback;
    	}    

    	@Override
    	protected void respond(AjaxRequestTarget target) {
    		callback.onCancel(target);
    	}

    	@Override
    	public void renderHead(Component component, IHeaderResponse response) {
    		response.render(JavaScriptHeaderItem.forReference(
    				new JavaScriptResourceReference(
    						ProjectAddPage.class, 
    						"https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"
    						)));
    		StringBuilder js = new StringBuilder();
    		js.append("$(document).ready(function() {\n");
    		js.append("  $(document).bind('keyup', function(evt) {\n");
    		js.append("    if (evt.keyCode == 27) {\n");
    		js.append(getCallbackScript());
    		js.append("\n");
    		js.append("        evt.preventDefault();\n");
    		js.append("    }\n");
    		js.append("  });\n");
    		js.append("});");
    		response.render(OnLoadHeaderItem.forScript(js));
    	}
    }	


}
