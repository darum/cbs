package cbs.webfront.page.main.TreeView;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.markup.html.repeater.tree.AbstractTree;
import org.apache.wicket.extensions.markup.html.repeater.tree.ITreeProvider;
import org.apache.wicket.extensions.markup.html.repeater.tree.content.CheckedFolder;
import org.apache.wicket.extensions.markup.html.repeater.util.ProviderSubset;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public class CheckedFolderContent extends Content {
	
	private ProviderSubset<ProjectDataDispBean> checked;
	
	public CheckedFolderContent(ITreeProvider<ProjectDataDispBean> provider) {
		checked = new ProviderSubset<ProjectDataDispBean>(provider, false);
	}

	@Override
	public void detach()
	{
		checked.detach();
	}

	protected boolean isChecked(ProjectDataDispBean data)
	{
		return checked.contains(data);
	}

	protected void check(ProjectDataDispBean data, boolean check)
	{
		if (check) {
			checked.add(data);
		} else {
			checked.remove(data);
		}
	}
	
	@Override 
	public Component newContentComponent(String id, final AbstractTree<ProjectDataDispBean> tree, IModel<ProjectDataDispBean> model) {
		
		return new CheckedFolder<ProjectDataDispBean>(id, tree, model) {
			@Override
			protected IModel<Boolean> newCheckBoxModel(final IModel<ProjectDataDispBean> model)
			{
				return new IModel<Boolean>()
				{
					private static final long serialVersionUID = 1L;

					@Override
					public Boolean getObject()
					{
						return isChecked(model.getObject());
					}

					@Override
					public void setObject(Boolean object)
					{
						check(model.getObject(), object);
					}

					@Override
					public void detach()
					{
					}
				};
			}
			
		};
	}
}
