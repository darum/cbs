package cbs.webfront.page.main.TableView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.AjaxEditableLabel;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.databean.MetaDataBean;
import cbs.web.service.packet.databean.ProjDataBean;
import cbs.web.service.packet.databean.TaskDataBean;
import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.client.PjQueryRequest;
import cbs.webfront.client.TaskColumnRequest;
import cbs.webfront.client.TaskModifyRequest;
import cbs.webfront.client.TaskQueryRequest;
import cbs.webfront.page.login.IndexPage;
import cbs.webfront.page.main.CbsContentPanel;
import cbs.webfront.page.main.MainFrame;
import cbs.webfront.page.main.dialog.ICbsDialogCallback;
import cbs.webfront.page.main.dialog.project.ProjectAddDialog;
import cbs.webfront.page.main.dialog.task.TaskAddDialog;

@SuppressWarnings("serial")
public class TableViewPanel extends CbsContentPanel {
	private final String DATE_FORMAT = "yyyy/MM/dd";
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	/** Page Parameter */
	private CbsWebPageParameters pgParams;
	
	/** TABLE表示用のデータ */
	private List<FrontPjdataBean> pjdataList;
	/** プロジェクト表示のListView */
	private ListView<FrontPjdataBean> projectList;
	
	/** リロード前にQueryを実行するかどうか（falseに設定すると実行しない） */
	private boolean query = true;
	
	/** 日時表示のFormatter */
	private SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT); 
	
	// タスク追加
	/** タスク追加ダイアログ */
	private TaskAddDialog modalAddTask;	

	
	/** シリアルID */
	private static final long serialVersionUID = 165641248302210217L;

	/**
	 * コンストラクタ
	 * @param id
	 * @param param
	 */
	public TableViewPanel(String id, final CbsWebPageParameters param) {
		super(id, param);
		this.pgParams = param;
		
		initComponent();
	}

	@Override
	protected void onConfigure() {
		logger.debug("onBeforeRender");
		
		if (this.query) {
			// Project情報を取得する
			pjQueryExecute();
			
			this.projectList.setModelObject(this.pjdataList);
		}
		
		// フラグをリセット
		this.query = true;
		
		super.onConfigure();
	}
	
	/**
	 * 初期化メソッド（オブジェクトの貼り付け）
	 * @throws Exception 
	 */
	private void initComponent() {
		logger.debug("initComponent()");
		
		/* QUERY */
		this.pjQueryExecute();
		this.query = false;			/* Queryを再実行しない */
		
		/* サブメニュー */
		/* Project Addtion Window(Modal) */
		final ProjectAddDialog projectAddDialog = 
				new ProjectAddDialog("modal1", "modal1-cookie", pgParams);
		projectAddDialog.setCallback(
				new ICbsDialogCallback(){

					@Override
					public void onAccept(AjaxRequestTarget target) {
						this.setQuery(true);
						projectAddDialog.close(target);
					}

					@Override
					public void onCancel(AjaxRequestTarget target) {
						this.setQuery(false);
						projectAddDialog.close(target);
					}

					private void setQuery(boolean query) {
						// クローズ後のページを取得
						MainFrame main = (MainFrame) TableViewPanel.this.getParent().getParent();
						main = (MainFrame) main.getPageReference().getPage();

						// MainPageからPanelを取得
						TableViewPanel self = (TableViewPanel) main.getBody().getContent();
						self.setQuery(query);
					}
				});
			
		projectAddDialog.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
			
			@Override
			public void onClose(AjaxRequestTarget arg0) {
				logger.debug("onClose()");

				// Reload Component
				arg0.add(TableViewPanel.this);
			}
		}); 
		super.add(projectAddDialog);
		
		// "Add Project" Link
		super.add(new AjaxLink<Void>("projAddAction"){
			/** */
			private static final long serialVersionUID = -388626332573096644L;

			public void onInitialize() {
				super.onInitialize();
				
				super.add(new Label("projectAdd", "プロジェクト追加"));
			}
			
			@Override
			public void onClick(AjaxRequestTarget target) {
				logger.debug("OnClick()");
				projectAddDialog.show(target);
			}
		});
		
		// TaskAdd ModalDaialog Page
		modalAddTask = new TaskAddDialog(
				"modalAddTask", "modalAddTask-cookie", this.pgParams, new ICbsDialogCallback(){
			@Override
			public void onAccept(AjaxRequestTarget target) {
				modalAddTask.close(target);
			}
			@Override
			public void onCancel(AjaxRequestTarget target) {
				LoggerFactory.getLogger(getClass()).info("onCancel()");
				this.setQuery(false);
				modalAddTask.close(target);
			}
			// TODO プロジェクト追加と共通化
			private void setQuery(boolean query) {
				// クローズ後のページを取得
				MainFrame main = (MainFrame) TableViewPanel.this.getParent().getParent();
				main = (MainFrame) main.getPageReference().getPage();

				// MainPageからPanelを取得
				TableViewPanel self = (TableViewPanel) main.getBody().getContent();
				self.setQuery(query);
			}
		});
		modalAddTask.setWindowClosedCallback(new ModalWindow.WindowClosedCallback() {
			@Override
			public void onClose(AjaxRequestTarget target) {
				target.add(TableViewPanel.this);
			}
		});
		super.add(modalAddTask);
		
		/* プロジェクト TABLE */
		this.projectList = new ProjectList(pjdataList);
		super.add(projectList);
		
		// 	Ajax更新を可能にする
		this.setOutputMarkupId(true);
	}
	
	/**
	 * Project照会を行う
	 * 結果はrowListメンバ変数へ格納
	 */
	private void pjQueryExecute() {
		/* 表示データ */
		this.pjdataList = new ArrayList<FrontPjdataBean>();
		try {
			/* プロジェクトの照会 */
			logger.debug("START PjQuery");
			
			/* DB Query */
			PjQueryRequest data = 
					new PjQueryRequest(super.getAccount(), super.getPassword());
			data.execute();
			if (data.getWsStatus() == AbstractPacket.CBS_WS_STAT_SUCCESS) {
				List<ProjDataBean> dataList = data.getDataList();
				logger.debug("get result. count=" + dataList.size());

				// 表示用へ載せ替え：プロジェクトごとに処理
				for (ProjDataBean bean : dataList) {
					int pjId = bean.getProjId();
					
					List<String> columnNames = getColumnNames(pjId);
					
					///////////////////////////
					// タスクデータの照会
					List<FrontTaskdataBean> taskDatas = getTaskDataList(pjId);
					
					pjdataList.add(
							new FrontPjdataBean(
									bean.getProjId(),
									bean.getName(), 
									bean.getOwnerAccount(), 
									columnNames,
									taskDatas));
				}
				
				logger.debug("END PjQuery: Success");
			} else {
				// TODO エラー処理
			}
		} catch(Exception e) {
			logger.error("Exception", e);
			// TODO エラー画面へ遷移
		}
	}
	
	/**
	 * カラム名リストの取得
	 * @param projId プロジェクトID
	 * @return カラム名リスト
	 * @throws Exception
	 */
	private List<String> getColumnNames(int projId) throws Exception {
		logger.info("getColumnNames: pjID={}", projId);
		
		///////////////////////////
		// カラムの照会
		TaskColumnRequest colReq = 
				new TaskColumnRequest(super.getAccount(), super.getPassword(), projId);
		colReq.execute();
		List<MetaDataBean> columns = colReq.getColumns();

		// カラム名のリスト生成
		List<String> columnNames = new ArrayList<String>();
		for (MetaDataBean col : columns) {
			columnNames.add(col.getName());
		}
		
		return columnNames;
	}

	/**
	 * タスクデータのリスト取得
	 * @param pjId プロジェクトID
	 * @return タスクデータのリスト（表示する順（表示列順）にデータをaddする）
	 * @throws Exception
	 */
	private List<FrontTaskdataBean> getTaskDataList(int pjId) throws Exception {
		// 子タスク情報取得のリクエスト
		logger.info("getTaskDataList: pjId={}", pjId);
		TaskQueryRequest taskReq = 
				new TaskQueryRequest(
						super.getAccount(), 
						super.getPassword(),
						pjId,
						0);
		taskReq.execute();
		
		
		List<FrontTaskdataBean> retTaskdataList = new ArrayList<FrontTaskdataBean>();	// 戻り値
		List<TaskDataBean> recvTasks = taskReq.getTasks();
		
		for (int i = 0; i < recvTasks.size(); ++i) {
			TaskDataBean recvTask = recvTasks.get(i);
			List<ColumnData> taskRow = new ArrayList<ColumnData>();
			
			// 表示データの挿入(固定データ）
			// No.
			taskRow.add(new ColumnData(String.valueOf(i + 1)));						// TODO 表示する番号の検討
			// タスク名
			taskRow.add(new ColumnData(recvTask.getTaskName()));
			// タスク担当名(リソース名)
			taskRow.add(new ColumnData(recvTask.getResName()));
			// 開始日
			Date start = recvTask.getStart();
			String strStart = "";
			if (start != null) {
				strStart = dateFormat.format(recvTask.getStart());
			}
			taskRow.add(new ColumnData(strStart));
			// 終了日
			Date term = recvTask.getTerm();
			String strTerm = "";
			if (term != null) {
				strTerm = dateFormat.format(term);
			}
			taskRow.add(new ColumnData(strTerm));
			// 進捗率
			taskRow.add(new ColumnData(String.valueOf(recvTask.getProgress())));
			
			List<MetaDataBean> columnsData = recvTask.getColumns();
			for (MetaDataBean columnBean : columnsData) {
				taskRow.add(new ColumnData(columnBean.getData()));
			}
			
			retTaskdataList.add(new FrontTaskdataBean(recvTask.getId(), taskRow));
		}

		return retTaskdataList;
	}
	
	public void setQuery(boolean b) {
		this.query = b;
	}

	/**
	 * プロジェクトのリストビュー
	 * @author darum
	 *
	 */
	class ProjectList extends ListView<FrontPjdataBean> {
		
		public ProjectList(List<? extends FrontPjdataBean> list) {
			this("projectList", list);
		}

		public ProjectList(String id, List<? extends FrontPjdataBean> list) {
			super(id, list);
		}

		@Override
		protected void populateItem(ListItem<FrontPjdataBean> item) {
			final FrontPjdataBean dataBean = item.getModelObject();
			
			item.add(new Label("navigator", dataBean.getName()));			// プロジェクト名
			item.add(new Label("owner", "[" + dataBean.getOwner() + "]"));	// プロジェクトオーナー
			
			/* Feedback Panel */
			FeedbackPanel feedback = new FeedbackPanel("feedback");
			item.add(feedback.setOutputMarkupId(true));

			/* タスク追加 */
			AjaxLink<Void> addTask = new AjaxLink<Void>("addTask"){

				@Override
				public void onClick(AjaxRequestTarget target) {
					int id = dataBean.getId();
					String name = dataBean.getName();
							
					LoggerFactory.getLogger(getClass()).
							debug("onClick(): ID={}, Name={}", id, name);
					// Dailog表示
					modalAddTask.show(target, id, name);
				}
			};
			item.add(addTask);

			/* 行ヘッダ */
			List<String> tableHeader = new ArrayList<String>();
			// 固定値の登録
			tableHeader.add("");		// Dummy;
			tableHeader.add("No");
			tableHeader.add("タスク名");
			tableHeader.add("担当");
			tableHeader.add("開始日");
			tableHeader.add("終了日");
			tableHeader.add("進捗率");
			// 個別設定のカラム名
			tableHeader.addAll(dataBean.getColumns());
			
			ListView<String> tableHeaderListView = new TableHeader(tableHeader);
			item.add(tableHeaderListView);

			/* タスク情報：実際のデータ */
			ListView<FrontTaskdataBean> rowListView = new TableData(dataBean.getTaskList(), feedback);
			item.add(rowListView);
		}
	}

	/**
	 * THのデータ設定
	 * @author darum
	 *
	 */
	class TableHeader extends ListView<String> {
		public TableHeader(List<String> tableHeader) {
			super("tableHeaderList", tableHeader);
		}

		@Override
		protected void populateItem(ListItem<String> item) {
			item.add(new Label("header", item.getModelObject()));
			
			// 先頭列：削除ボタン用の幅設定
			if (item.getIndex() == 0) {
				item.add(new AttributeModifier("style", new AbstractReadOnlyModel<String>(){
					@Override
					public String getObject() {
						return "width :16px"; // TODO 設定ファイルまたは定数
					}
				}));
			}
		}
	}
	
	/**
	 * タスクデータの表示
	 * @author darum
	 *
	 */
	class TableData extends ListView<FrontTaskdataBean>{
		private FeedbackPanel feedback;
		public TableData(List<FrontTaskdataBean> taskData, FeedbackPanel feedback) {
			super("rowList", taskData);
			
			this.feedback = feedback;
		}

		@Override
		protected void populateItem(ListItem<FrontTaskdataBean> taskItem) {
			FrontTaskdataBean rowTaskItem = taskItem.getModelObject();
			if (!"".equals(rowTaskItem.get(0).getValue())) {
				// 先頭が空じゃない→削除ボタン用の要素を追加
				rowTaskItem.add(0, "");			// 削除ボタン用に先頭に空要素を追加
			}
			
			ListView<ColumnData> columnListView = new TableColumn(taskItem.getModelObject(), this.feedback);
			taskItem.add(columnListView);
		}
	}
	
	/**
	 * テーブルの行の表示
	 * 
	 * @author darum
	 *
	 */
	class TableColumn extends ListView<ColumnData> {
		private int taskId;
		private FeedbackPanel feedback;
		
		public TableColumn (FrontTaskdataBean taskData, FeedbackPanel feedback) {
			super("columnList", taskData.getDispDataList());
			
			this.taskId = taskData.getTaskId();
			this.feedback = feedback;
		}

		@Override
		protected void populateItem(ListItem<ColumnData> columnItem) {
			Component image;
			Component datasComponent;
			
			int index = columnItem.getIndex();
			if (index == 0) {
				// 先頭セルは削除ボタン
				image = new WebMarkupContainer("image");
				String baseUrl = urlFor(IndexPage.class, new PageParameters()).toString();
				image.add(new AttributeModifier("src", baseUrl + "img/delete.png"));
				
				// ラベルは表示しない
				datasComponent = new Label("datas");
				datasComponent.setVisible(false);
			} else {
				// データ表示セル
				
				// 画像は表示しない
				image = new Image("image", (IModel<?>)null);
				image.setVisible(false);
				
				ColumnData data = columnItem.getModelObject();
				String value = data.getValue();
//				logger.debug("colums:" + data);

				// データ表示
				datasComponent = new Label("datas", value);
				if (index == 6) {
					// Edit可能にする
					// TODO 全セル対応
					datasComponent = new CbsDataCell("datas", taskId, value, this.feedback, Double.class);
					logger.debug(datasComponent.toString());
				}
			}
			columnItem.add(image);
			columnItem.add(datasComponent);
		}
	}
	
	/**
	 * データ表示用のセル（編集可能）
	 * 
	 * @author darum
	 *
	 */
	class CbsDataCell extends AjaxEditableLabel<String> {
		private String data;
		private int taskId;
		private FeedbackPanel feedback;
		
		/**
		 * 
		 * @param id Component ID
		 * @param taskId タスクID (DB更新用)
		 * @param initData 初期表示データ
		 */
		public CbsDataCell(String id, int taskId, String initData, FeedbackPanel feedback, Class<?> type) {
			super(id);
			super.setType(type);
			
			data = initData;
			this.taskId = taskId;
			this.feedback = feedback;
			
			this.setDefaultModel(new CompoundPropertyModel<String>(data));
			
			super.add(new AttributeModifier("class", "data_cell"));
		}
		
		@Override
		protected void onSubmit(AjaxRequestTarget target) {
			TaskModifyRequest req = new TaskModifyRequest(getAccount(), getPassword());
			req.setTaskId(taskId);

			// TODO 暫定：Progressのみ
			Double datas = (Double) this.getDefaultModelObject();
			if (datas == null) {
				datas = new Double(0);
			}
			req.setProg(datas);
			
			try {
				req.execute();
				int status = req.getWsStatus();
				if (status != 0) {
					// TODO メッセージ
					String msg;
					msg = String.valueOf(status);
					logger.warn("Update Error: msg=[{}]", msg);
					error(msg);
					target.add(this.feedback);
					return;
				}
				
			} catch (Exception e) {
				// TODO メッセージ
				error(e.getMessage());
				target.add(this.feedback);
			}
			
			success("更新しました");
			target.add(this.feedback);
			super.onSubmit(target);
		}
		
		@Override
		protected void onError(AjaxRequestTarget target) {
			// TODO エラーメッセージ
			target.add(this.feedback);
		}

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}
	}
}
