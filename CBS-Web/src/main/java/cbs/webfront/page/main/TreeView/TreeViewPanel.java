package cbs.webfront.page.main.TreeView;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeadersToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.NoRecordsToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.extensions.markup.html.repeater.tree.TableTree;
import org.apache.wicket.extensions.markup.html.repeater.tree.table.TreeColumn;
import org.apache.wicket.extensions.markup.html.repeater.tree.theme.WindowsTheme;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.OddEvenItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.databean.MetaDataBean;
import cbs.web.service.packet.databean.ProjDataBean;
import cbs.web.service.packet.databean.TaskDataBean;
import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.client.PjQueryRequest;
import cbs.webfront.client.TaskColumnRequest;
import cbs.webfront.client.TaskQueryRequest;
import cbs.webfront.page.main.CbsContentPanel;

@SuppressWarnings("serial")
public class TreeViewPanel extends CbsContentPanel {

	// for Tree
	/** tree */
	private TableTree<ProjectDataDispBean, String> tree;
	private Content content;
	private Behavior theme;	
	
	public TreeViewPanel(String id, CbsWebPageParameters param) {
		super(id, param);
		
		initComponent();
	}
	
	private void initComponent() {
		
		{
			// カラム定義
			List<IColumn<ProjectDataDispBean, String>> columns = new ArrayList<IColumn<ProjectDataDispBean, String>>();
			{
//				columns.add(new PropertyColumn<ProjectDataDispBean, String>(Model.of("ID"), "id"));
				columns.add(new TreeColumn<ProjectDataDispBean, String>(Model.of("")));
//				columns.add(new AbstractColumn<ProjectDataDispBean, String>(Model.of("Depth")){	// TODO処理確認
//					private static final long serialVersionUID = 1L;
//	
//					@Override
//					public void populateItem(Item<ICellPopulator<ProjectDataDispBean>> cellItem, String componentId,
//							IModel<ProjectDataDispBean> rowModel)
//					{
//						NodeModel<ProjectDataDispBean> nodeModel = (NodeModel<ProjectDataDispBean>)rowModel;
//	
//						cellItem.add(new Label(componentId, "" + nodeModel.getDepth()));
//					}
//	
//					@Override
//					public String getCssClass()
//					{
//						return "number";
//					}
//				});
				columns.add(new PropertyColumn<ProjectDataDispBean, String>(Model.of("TASK"), "taskName"));
				columns.add(new PropertyColumn<ProjectDataDispBean, String>(Model.of("担当者"), "resName"));
			}
			
			// Data
			List<ProjectDataDispBean> dataList = new ArrayList<ProjectDataDispBean>();
//			{
//				ProjectDataDispBean data1 = new ProjectDataDispBean("1", "Data1", ""); 
//				@SuppressWarnings("unused")
//				ProjectDataDispBean sub11 = new ProjectDataDispBean(data1, "1-1", "Data1-Sub1", "Res2");
//				dataList.add(data1);
//				dataList.add(new ProjectDataDispBean("2", "Data2", "Res10"));
//			}
			{
				// TODO ProjectDataと共通化
				try {
					/* DB Query */
					PjQueryRequest data = new PjQueryRequest(super.getAccount(), super.getPassword());
					data.execute();
					if (data.getWsStatus() == AbstractPacket.CBS_WS_STAT_SUCCESS) {
						int pjIdx = 0;
						for (ProjDataBean bean : data.getDataList()) {
							int pjId = bean.getProjId();
							
							// カラム名の取得
							List<String> columnNames = new ArrayList<String>();
							{
								// TODO メソッド
								
								TaskColumnRequest colReq =
										new TaskColumnRequest(super.getAccount(), super.getPassword(), pjId);
								colReq.execute();
								for (MetaDataBean col : colReq.getColumns()) {
									columnNames.add(col.getName());
								}
							}
							
							// タスクデータ
							ProjectDataDispBean pjData = new ProjectDataDispBean(String.valueOf(++pjIdx), bean.getName(), "[" + bean.getOwnerAccount() + "]");
							dataList.add(pjData);
							{
								TaskQueryRequest taskReq = new TaskQueryRequest(super.getAccount(), super.getPassword(), pjId, 0);
								taskReq.execute();
								
								for (TaskDataBean taskBean : taskReq.getTasks()) {
									ProjectDataDispBean task = new ProjectDataDispBean(String.valueOf(taskBean.getId()), taskBean.getTaskName(), taskBean.getResName());
									pjData.addChild(task);
								}
							}
							
						}
					}
					
				} catch(Exception e) {
					// TODO エラー
				}
			}
			TreeViewData holder = new TreeViewData(dataList);
			
			// Provider
			ProjectDataDispBeanProvider provider = new ProjectDataDispBeanProvider(holder);
			
			// Content
			content = new CheckedFolderContent(provider);
			
			// Theme
			theme = new WindowsTheme();
			
			// CreateTree
			tree = new TableTree<ProjectDataDispBean, String>("tree", columns, provider, Integer.MAX_VALUE, new ProjectDataExpansionModel()){

				@Override
				protected Component newContentComponent(String id,
						IModel<ProjectDataDispBean> model) {
					return TreeViewPanel.this.newContentComponent(id, model);
				}
				
				@Override
				protected Item<ProjectDataDispBean> newRowItem(String id, int index, IModel<ProjectDataDispBean> model) {
					return new OddEvenItem<ProjectDataDispBean>(id, index, model);
				}
				
			};
			tree.getTable().addTopToolbar(new HeadersToolbar<String>(tree.getTable(), null));
			tree.getTable().addBottomToolbar(new NoRecordsToolbar(tree.getTable()));
			tree.add(new Behavior()
			{
				private static final long serialVersionUID = 1L;

				@Override
				public void onComponentTag(Component component, ComponentTag tag)
				{
					theme.onComponentTag(component, tag);
				}

				@Override
				public void renderHead(Component component, IHeaderResponse response)
				{
					theme.renderHead(component, response);
				}
			});
//			tree.add(new AttributeAppender("class", "treeview"));
			super.add(tree);
		}
	}

	/**
	 * for TreePanel
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	protected Component newContentComponent(String id, IModel<ProjectDataDispBean> model) {
		return content.newContentComponent(id, tree, model);
	}

}
