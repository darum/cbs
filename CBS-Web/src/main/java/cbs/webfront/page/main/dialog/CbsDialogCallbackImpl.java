package cbs.webfront.page.main.dialog;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;

/**
 * 標準的なダイアログのコールバック処理
 * ダイアログを閉じる処理だけ行う
 * 
 * @author darum
 *
 */
@SuppressWarnings("serial")
public class CbsDialogCallbackImpl implements ICbsDialogCallback {
	
	private ModalWindow dialog;

	CbsDialogCallbackImpl(ModalWindow dialog) {
		this.dialog = dialog;
	}

	@Override
	public void onAccept(AjaxRequestTarget target) {
		dialog.close(target);
	}

	@Override
	public void onCancel(AjaxRequestTarget target) {
		dialog.close(target);
	}

}
