package cbs.webfront.page.main.TreeView;

import org.apache.wicket.Component;
import org.apache.wicket.extensions.markup.html.repeater.tree.AbstractTree;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
public abstract class Content implements IDetachable
{

	/**
	 * Create new content.
	 */
	public abstract Component newContentComponent(String id, AbstractTree<ProjectDataDispBean> tree,
		IModel<ProjectDataDispBean> model);

	@Override
	public void detach()
	{
	}
}