package cbs.webfront.page.main.TreeView;

import java.util.Set;

import org.apache.wicket.model.AbstractReadOnlyModel;

@SuppressWarnings("serial")
public class ProjectDataExpansionModel extends AbstractReadOnlyModel<Set<ProjectDataDispBean>>{

	@Override
	public Set<ProjectDataDispBean> getObject() {
		return ProjectDataExpansion.get();
	}

}
