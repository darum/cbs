package cbs.webfront.page.main.dialog.project;

import org.apache.wicket.Page;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;

import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.page.main.dialog.ICbsDialogCallback;

@SuppressWarnings("serial")
public class ProjectAddDialog extends ModalWindow {
	private CbsWebPageParameters params;
	
	public ProjectAddDialog(String id, String cookie, CbsWebPageParameters params) {
		super(id);
		setCookieName(cookie);
		this.params = params;
	}
	
	public void setCallback(final ICbsDialogCallback callback) {
		setPageCreator(new ModalWindow.PageCreator(){
			@Override
			public Page createPage() {
				return new ProjectAddPage(params, callback);
			}
		});
	}

}
