package cbs.webfront.page.main.dialog.resource;

import org.apache.wicket.Page;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class ResourceAddPageCreator implements ModalWindow.PageCreator {
	private PageParameters params;
	
	public ResourceAddPageCreator(PageParameters params) {
		this.params = params;
	}

	@Override
	public Page createPage() {
		return new ResourceAddPage(this.params);
	}

}
