package cbs.webfront.page.main.TreeView;

import java.util.Iterator;

import org.apache.wicket.extensions.markup.html.repeater.tree.ITreeProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;

@SuppressWarnings("serial")
public class ProjectDataDispBeanProvider implements ITreeProvider<ProjectDataDispBean>{
	
	private TreeViewData pjDataHolder;
	
	public ProjectDataDispBeanProvider(TreeViewData aPjData) {
		pjDataHolder = aPjData;
	}

	@Override
	public void detach() {
		// Nothing to do
	}

	@Override
	public Iterator<? extends ProjectDataDispBean> getChildren(ProjectDataDispBean arg0) {
		return arg0.getChildren().iterator();
	}

	@Override
	public Iterator<? extends ProjectDataDispBean> getRoots() {
		return pjDataHolder.getIterator();
	}

	@Override
	public boolean hasChildren(ProjectDataDispBean arg0) {
		return (arg0.getParent() == null) || !(arg0.getChildren().isEmpty());
	}

	@Override
	public IModel<ProjectDataDispBean> model(ProjectDataDispBean arg0) {
		return new ProjectDataDispBeanModel(arg0, this.pjDataHolder);
	}

	private static class ProjectDataDispBeanModel extends LoadableDetachableModel<ProjectDataDispBean> {
		
		private final String id;
		private TreeViewData dataHolder;
		
		public ProjectDataDispBeanModel(ProjectDataDispBean data, TreeViewData aHolder) {
			super(data);
			
			id = data.getId();
			
			dataHolder = aHolder;
		}

		@Override
		protected ProjectDataDispBean load() {
			return dataHolder.getData(id);
		}
		
		/**
		 * IDが等しいとequal
		 */
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ProjectDataDispBeanModel) {
				return ((ProjectDataDispBeanModel)obj).id.equals(id);
			}
			return false;
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}
	}
}
