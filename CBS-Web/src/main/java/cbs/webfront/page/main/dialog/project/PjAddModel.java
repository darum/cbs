package cbs.webfront.page.main.dialog.project;

import org.apache.wicket.util.io.IClusterable;

/**
 * プロジェクト追加ウィンドウのModel
 * @author Yamashita
 *
 */
@SuppressWarnings("serial")
public class PjAddModel implements IClusterable {

	/** プロジェクト名 */
	private String projectName;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String name) {
		this.projectName = name;
	}

}
