package cbs.webfront.page.main;

import org.apache.wicket.markup.html.panel.Panel;

import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.page.MenuPanel;
import cbs.webfront.page.main.TableView.TableViewPanel;

@SuppressWarnings("serial")
public class BodyPanel extends Panel {
	private CbsContentPanel content;
	
	public BodyPanel(String id, CbsWebPageParameters param) {
		super(id);
		
		/* メニュー（nav）*/
		MenuPanel menu = new MenuPanel("menuPanel", this, param);
		super.add(menu);
		
		/* メインウィンドウ */
		// TODO デフォルト表示画面の切り替え
		content = new TableViewPanel("content", param);
		super.add(content);
		
		this.setOutputMarkupId(true);
	}
	
	/**
	 * content の更新
	 * 
	 * @param contentPanel 要素="content"のPanel
	 */
	public void updateContent(CbsContentPanel contentPanel) {
		this.remove("content");
		add(contentPanel);
		
		content = contentPanel;
	}

	public CbsContentPanel getContent() {
		return content;
	}

}
