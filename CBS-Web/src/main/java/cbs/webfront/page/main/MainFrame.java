package cbs.webfront.page.main;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.page.Footer;
import cbs.webfront.page.Header;

public class MainFrame extends WebPage {
	private BodyPanel body;

	/**
	 * 
	 */
	private static final long serialVersionUID = 672188016000553735L;

	public MainFrame(final CbsWebPageParameters param) {
		Label titile = new Label("title", "CBScheduler");
		super.add(titile);
		
		/* ヘッダ */
		Header header = new Header("headerPanel", true);
		super.add(header);
		
		/* Body */
		body = new BodyPanel("body", param);
		super.add(body);
		
		/* フッター */
		Footer footer = new Footer("footerPanel");
		super.add(footer);
	}

	public BodyPanel getBody() {
		return body;
	}
}
