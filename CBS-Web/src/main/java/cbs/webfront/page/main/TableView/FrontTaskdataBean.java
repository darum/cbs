package cbs.webfront.page.main.TableView;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class FrontTaskdataBean implements Serializable {
	
	private int taskId;
	
	private List<ColumnData> dispDataList;
	
	public FrontTaskdataBean(int taskId, List<ColumnData> dataList) {
		this.taskId = taskId;
		this.dispDataList = dataList;
	}
	
	/**
	 * データリストに要素を追加する
	 * 
	 * @param index
	 * @param element
	 */
	public void add(int index, String element) {
		dispDataList.add(index, new ColumnData(element));
	}
	
	/**
	 * データリストから値を取得する
	 * 
	 * @param index
	 * @return
	 */
	public ColumnData get(int index) {
		return dispDataList.get(index);
	}

	public int getTaskId() {
		return taskId;
	}

	public List<ColumnData> getDispDataList() {
		return dispDataList;
	}

}
