package cbs.webfront.page.main.TreeView;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.wicket.MetaDataKey;
import org.apache.wicket.Session;

@SuppressWarnings("serial")
public class ProjectDataExpansion implements Set<ProjectDataDispBean>, Serializable {
	
	/** Sessionのキー */
	private static MetaDataKey<ProjectDataExpansion> KEY = new MetaDataKey<ProjectDataExpansion>() {
		
	};

	private Set<String> ids = new HashSet<String>();

	private boolean inverse;
	
	public void expandAll()
	{
		ids.clear();

		inverse = true;
	}

	public void collapseAll()
	{
		ids.clear();

		inverse = false;
	}

	@Override
	public boolean add(ProjectDataDispBean arg0) {
		if (inverse) {
			return ids.remove(arg0.getId());
		} else {
			return ids.add(arg0.getId());
		}
	}

	@Override
	public boolean contains(Object arg0) {
		ProjectDataDispBean data = (ProjectDataDispBean) arg0;
		
		if (inverse) {
			return !ids.contains(data.getId());
		} else {
			return ids.contains(data.getId());
		}
	}

	@Override
	public boolean remove(Object arg0) {
		ProjectDataDispBean data = (ProjectDataDispBean) arg0;
		
		if (inverse) {
			return ids.add(data.getId());
		} else {
			return ids.remove(data.getId());
		}
	}

	// 非対応メソッド
	@Override
	public void clear()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int size()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <A> A[] toArray(A[] a)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<ProjectDataDispBean> iterator()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(Collection<? extends ProjectDataDispBean> c)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}

	// static
	/**
	 * SessionからExpansionを取得する
	 * @return
	 */
	public static ProjectDataExpansion get() {
		ProjectDataExpansion expansion = Session.get().getMetaData(KEY);
		if (expansion == null) {
			expansion = new ProjectDataExpansion();
			
			Session.get().setMetaData(KEY, expansion);
		}
		return expansion;
	}

}
