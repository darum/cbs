package cbs.webfront.page.main.dialog.resource;

import java.io.Serializable;

// TODO パッケージを検討
@SuppressWarnings("serial")
public class ProjectResourceBean implements Serializable {
	private String resName;

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

}
