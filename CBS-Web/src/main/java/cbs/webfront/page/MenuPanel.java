package cbs.webfront.page;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.page.main.BodyPanel;
import cbs.webfront.page.main.CbsContentPanel;
import cbs.webfront.page.main.TableView.TableViewPanel;
import cbs.webfront.page.main.TreeView.TreeViewPanel;

@SuppressWarnings("serial")
public class MenuPanel extends Panel {

	/** シリアルID */
	private static final long serialVersionUID = 7909464745321468215L;

	public MenuPanel(String id, BodyPanel parentPanel, CbsWebPageParameters param) {
		super(id);

		List<ButtonMenu> listBtMenu = new ArrayList<ButtonMenu>();
		listBtMenu.add(new ButtonMenu("テーブルビュー", MainContent.TABLE_VIEW));
		listBtMenu.add(new ButtonMenu("ツリービュー", MainContent.TREE_VIEW));

		ListView<ButtonMenu> menuList = new ListView<ButtonMenu>("menuList", listBtMenu) {
			private CbsWebPageParameters param;
			private BodyPanel parentPanel;
			
			// コンストラクタ相当
			public ListView<ButtonMenu> init(BodyPanel panel, final CbsWebPageParameters aParam) {
				this.param = aParam;
				this.parentPanel = panel;
				
				return this;
			}

			@Override
			protected void populateItem(ListItem<ButtonMenu> item) {
				ButtonMenu data = item.getModelObject();
				
				Button button = new AjaxButton("btMenu", new Model<String>(data.getDesc())){
					
					MainContent content;
					
					Button init(MainContent aContent) {
						this.content = aContent;
						
						return this;
					}
					
					@Override
					public void onSubmit(AjaxRequestTarget target, Form<?> form) {
						CbsContentPanel contentPanel = null;
						switch(content) {
						case TABLE_VIEW:
							contentPanel = new TableViewPanel("content", param);
							break;
						case TREE_VIEW:
							contentPanel = new TreeViewPanel("content", param);
							break;
						}
						parentPanel.updateContent(contentPanel);
						
						target.add(parentPanel);
					}
					
				}.init(data.getContent());
				item.add(button);
			}
			
		}.init(parentPanel, param);
		
		Form<?> menuForm = new Form<String>("menuForm");
		menuForm.add(menuList);
		
		add(menuForm);
	}
	
}

