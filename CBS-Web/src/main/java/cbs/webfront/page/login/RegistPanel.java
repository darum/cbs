package cbs.webfront.page.login;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
//import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.packet.SysUserControl;
import cbs.webfront.client.UserRequest;

public class RegistPanel extends Panel {

	/** */
	private static final long serialVersionUID = 6665545374562172765L;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	/** Model: userフィールド */
	private Model<String> mRegUser = null;
	/** Model:passフィールド */
	private Model<String> mRegPass = new Model<String>();
	/** Model:E-mailフィールド */
	private Model<String> mRegEmail = new Model<String>();

	public RegistPanel(String id) {
		super(id);
		
		/* 登録 **********************************/
		super.add(new FeedbackPanel("registValidMessage"));
		
		Form<?> regist = new Form<Object>("regist") {
			/** */
			private static final long serialVersionUID = 2011973740576092451L;
		};
		super.add(regist);

		/* ユーザ名 */
		mRegUser = new Model<String>();
		TextField<String> user =
				new RequiredTextField<String>("user", mRegUser);
		regist.add(user);

		/* パスワード */
		PasswordTextField pass = new PasswordTextField("pass", mRegPass);
		regist.add(pass);

		/* E-mail */
		TextField<String> email =
				new TextField<String>("email", mRegEmail);
		email.setRequired(true);
		email.add(EmailAddressValidator.getInstance());
		regist.add(email);

		/* ボタン */
		Button execButton = new Button("exec") {

			/**
			 *
			 */
			private static final long serialVersionUID = -997136765677385750L;

			public void onSubmit() {
				submitRegistUser();
			}
		};
		regist.add(execButton);
		
	}
		
	private void submitRegistUser() {
		try {
			UserRequest userRequest = new UserRequest(mRegUser.getObject(), mRegPass.getObject());
			userRequest.setEmail(mRegEmail.getObject());
			userRequest.execute();
	
			if (userRequest.getWsStatus() == SysUserControl.CBS_WS_STAT_SUCCESS) {
//				PageParameters params = new PageParameters();
				int userId = userRequest.getUserId();
				logger.info("UserID={}", userId);
//				params.add("ID", String.valueOf(userId));
	//				setResponsePage(new UserRegSuccess(IndexPage.this, params));	// TODO
				
			} else {
				logger.warn("WS Error: Code={}", userRequest.getWsStatus());
			}
		} catch (Exception e) {
			logger.error("Exception", e);	// TODO 
		}
	}

}
