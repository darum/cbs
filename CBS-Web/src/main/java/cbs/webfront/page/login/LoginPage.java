package cbs.webfront.page.login;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel;
import org.apache.wicket.extensions.markup.html.tabs.AbstractTab;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

public class LoginPage extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1119993468113458224L;

	public LoginPage(String id) {
		super(id);

		// Tabs
		List<ITab> tabs = new ArrayList<ITab>();
		
		/* LoginPanel */
		tabs.add(new AbstractTab(new Model<String>("ログイン")) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4298028818464853654L;

			@Override
			public Panel getPanel(String panelId) {
				return new LoginPanel(panelId);
			}
		});
		
		/* Regist Panel */
		tabs.add(new AbstractTab(new Model<String>("ユーザ登録")) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4126522798355962088L;

			@Override
			public Panel getPanel(String id) {
				return new RegistPanel(id);
			}
		});
		
		add(new AjaxTabbedPanel<ITab>("tabs", tabs));
	}
		
}
