package cbs.webfront.page.login;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

import cbs.webfront.page.Footer;
import cbs.webfront.page.Header;

public class IndexPage extends WebPage {

	/** */
	private static final long serialVersionUID = -561198145397971017L;

	public IndexPage() {
		/*  */
		Label title = new Label("title", "CBScheduler - メインページ");
		add(title);

		/* コンテンツ */
		super.add(new Header("headerPanel", false));
//		super.add(new MenuPanel("menuPanel"));
		super.add(new LoginPage("content"));
		super.add(new Footer("footerPanel"));
		
		
	}

}
