package cbs.webfront.page.login;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.SysUserAuth;
import cbs.webfront.CbsWebPageParameters;
import cbs.webfront.WebServiceException;
import cbs.webfront.client.AuthRequest;
import cbs.webfront.page.main.MainFrame;

public class LoginPanel extends Panel {

	/** シリアルID */
	private static final long serialVersionUID = 6665545374562172765L;

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private TextField<String> textUser;
	private Form<?> login;
	
	/** Model(User) */
	private Model<String> mLoginUser;
	/** Model(Pass) */
	private Model<String> mLoginPass;

	/**
	 * コンストラクタ
	 * @param id ID
	 */
	public LoginPanel(String id) {
		super(id);
		
		/* ログイン ************************************/
		super.add(new FeedbackPanel("loginValidMessage"));
		
		login = new Form<Object>("login") {

			/** シリアルID */
			private static final long serialVersionUID = -757994532810641455L;
			
		};
		add(login);
		
		mLoginUser = new Model<String>();
		textUser = new RequiredTextField<String>("user", mLoginUser);
		login.add(textUser);
		mLoginPass = new Model<String>();
		login.add(new PasswordTextField("pass", mLoginPass));
		
		Button loginExec = new Button("exec"){

			/** シリアルID */
			private static final long serialVersionUID = 9057038515667396907L;
			
			@Override
			public void onSubmit() {
				submitLogin();
			}
			
		};
		login.add(loginExec);
	}

	/**
	 * ログイン実行処理
	 */
	private void submitLogin() {
		String user = mLoginUser.getObject();
		String pass = mLoginPass.getObject();	// TODO MD5
		
		AuthRequest authRequest = new AuthRequest(user, pass);
		try {
			authRequest.execute();
			
			int wsStatus = authRequest.getWsStatus();
			if (wsStatus == AbstractPacket.CBS_WS_STAT_SUCCESS) {
				CbsWebPageParameters params = new CbsWebPageParameters(user, pass);
				
				super.setResponsePage(new MainFrame(params));
			} else if (wsStatus == SysUserAuth.CBS_WS_STAT_AUTH_ERROR) {
				logger.info("Auth Error");
				textUser.error("ユーザ名またはパスワードが間違っています");
			}
		} catch (WebServiceException ex) {
			int statusCode = ex.getHttpStatusCode();
			switch(statusCode) {
			case 404:
				login.error("WebServiceに接続できません");
				break;
			case 204:
			case 500:
				login.error("WebServiceが正常に動作していません。Status=" + statusCode);
				break;
			default:
				logger.error(ex.toString(), ex);
				login.fatal("Invalid Response. Status=" + statusCode);
				break;
			}
		} catch (Exception e) {
			logger.error("WebService Error", e);
			login.error("WebService Error");
		}
	}
	
}
