package cbs.webfront.page;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.StringValue;

import cbs.webfront.BasePage;

public class UserRegSuccess extends BasePage {

	/**
	 *
	 */
	private static final long serialVersionUID = -1442745885005281198L;

	public UserRegSuccess(WebPage page, PageParameters parameters) {
		super(page, parameters);

		StringValue id = parameters.get("ID");

		Label userId = new Label("userId", id.toString());
		super.add(userId);

		Link<Void> back = new Link<Void>("back") {

			/**
			 *
			 */
			private static final long serialVersionUID = -9082652915116836160L;

			@Override
			public void onClick() {
				// TODO 自動生成されたメソッド・スタブ

			}

		};
		super.add(back);
	}

}
