package cbs.webfront.page;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.webfront.page.login.IndexPage;

public class Header extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4170296358216400089L;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	public Header(String string, boolean logoutEnable) {
		super(string);
		
		/* メニューリスト */
		List<HeaderMenuBean> listMenuBean = new ArrayList<HeaderMenuBean>();
		listMenuBean.add(new HeaderMenuBean("ログアウト", logoutEnable));	// TODO 場所によってfalse
		
		@SuppressWarnings("serial")
		ListView<HeaderMenuBean> menuList = new ListView<HeaderMenuBean>("menuList", listMenuBean){

			@Override
			protected void populateItem(final ListItem<HeaderMenuBean> item) {
				HeaderMenuBean bean = item.getModelObject();
				
				Link<Void> menuLink = new Link<Void>("menuLink") {
					@Override
					public void onClick() {
						menuProc(item.getIndex());
					}
				};
				Label menuItem = new Label("menuItem", bean.getName());
				menuItem.setVisible(bean.getEnabled());
				menuLink.add(menuItem);
				menuLink.setEnabled(bean.getEnabled());
				item.add(menuLink);
			}
			
		};
		add(menuList);
	}
	
	private void menuProc(int index) {
		logger.debug("index={}", index);
		
		switch(index) {
		case 0:		// ログアウト
			setResponsePage(new IndexPage());
			break;
		}
	}

}
