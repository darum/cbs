package cbs.webfront.page;

import java.io.Serializable;

@SuppressWarnings("serial")
public class HeaderMenuBean implements Serializable {
	
	private String name;
	private boolean enable;

	public HeaderMenuBean(String aName, boolean aEnable) {
		name = aName;
		enable = aEnable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getEnabled() {
		return enable;
	}

}
