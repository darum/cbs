package cbs.webfront.page;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ButtonMenu implements Serializable {

	private String desc;
	private MainContent content;

	public ButtonMenu(String string, MainContent aContent) {
		desc = string;
		this.content = aContent;
	}

	public String getDesc() {
		return desc;
	}
	
	public MainContent getContent() {
		return content;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
