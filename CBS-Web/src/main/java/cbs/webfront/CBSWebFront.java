package cbs.webfront;

import java.util.ResourceBundle;

import org.apache.wicket.Page;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.protocol.http.WebApplication;

import cbs.webfront.page.login.IndexPage;

public class CBSWebFront extends WebApplication {

	private String CHAR_ENCODING = "UTF-8";
	private String PROP_NAME = "cbs";
	
	private String configType;

	public CBSWebFront() throws Exception {
		ResourceBundle bundle = ResourceBundle.getBundle(PROP_NAME);

		configType = bundle.getString("CONFIG_TYPE");
	}
	
	/**
	 * 
	 */
	protected void init() {
		super.init();

		/* Encodingの設定 */
		this.getRequestCycleSettings().setResponseRequestEncoding(CHAR_ENCODING);
		this.getMarkupSettings().setDefaultMarkupEncoding(CHAR_ENCODING);

	}

	/**
	 * HomePage(IndexPage)の設定
	 */
	@Override
	public Class<? extends Page> getHomePage() {
		return IndexPage.class;
	}

	@Override
	public RuntimeConfigurationType getConfigurationType() {
		if ("1".equals(configType)) {
			// DEPLOY Mode
			return RuntimeConfigurationType.DEPLOYMENT;
		} else {
			return RuntimeConfigurationType.DEVELOPMENT;
		}
	}
}
