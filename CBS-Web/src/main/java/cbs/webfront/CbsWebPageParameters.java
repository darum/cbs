package cbs.webfront;

import org.apache.wicket.request.mapper.parameter.PageParameters;

@SuppressWarnings("serial")
public class CbsWebPageParameters extends PageParameters {

	public static final String PARAM_ACCOUNT = "ACCOUNT";
	public static final String PARAM_PASSWORD = "PASSWORD";
	
	public CbsWebPageParameters(String aAccount, String aPassword) {
		super();
		
		this.add(PARAM_ACCOUNT, aAccount);
		this.add(PARAM_PASSWORD, aPassword);
	}
}
