package cbs.webfront;

@SuppressWarnings("serial")
public class WebServiceException extends Exception {
	
	private String requestApi;
	private int httpStatusCode;
	
	public WebServiceException(final String api, int inHttpStatusCode) {
		super("WEB Service Exception");
		
		this.requestApi = api;
		this.httpStatusCode = inHttpStatusCode;
	}
	
	/* getter */
	public int getHttpStatusCode() {
		return httpStatusCode;
	}
	
	public String getRequestApi() {
		return this.requestApi;
	}

}
