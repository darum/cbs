package cbs.webfront.util;

public class Utility {
	
	/**
	 * 文字列が空白かチェックする。
	 *  null, ""(空白除去後も含む）だったらtrue
	 * @param str チェック文字列
	 * @return チェック結果。true=空白
	 */
	public static boolean IsBlankString(final String str) {
		boolean ret = true;
		
		
		if(str != null) {
			if (!"".equals(str.trim())) {
				ret = false;
			}
		}
		
		return ret;
	}
}
