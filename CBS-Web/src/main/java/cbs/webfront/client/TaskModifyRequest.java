package cbs.webfront.client;

import java.util.List;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.TaskControl;
import cbs.web.service.packet.databean.ColumnDataBean;
import cbs.web.service.packet.provider.TaskControlJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

/**
 * /task_ctrl/modify 呼び出しクラス
 *  
 * @author darum
 *
 */
public class TaskModifyRequest extends AbstractRequest {
	private final String API_PATH = "/task_ctrl/modify";
	
	/** I/O Packet */
	private TaskControl packet;
	
	private int taskId;
	
	private Integer parentTaskId = null;
	private int resId;
	private long startDT;
	private long termDT;
	private Double prog = null;
	private List<ColumnDataBean> columns = null;
	
	private int wsStatus;

	public TaskModifyRequest(String aAccount, String aPassword) {
		super(aAccount, aPassword);
	}

	@Override
	protected String getApiPath() {
		return API_PATH;
	}

	@Override
	protected Class<?> getProviderClass() {
		return TaskControlJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return packet;
	}

	@Override
	protected void preRequest() throws Exception {
		packet = new TaskControl(taskId);
		
		packet.setSessionId(getSessionId());
		
		// Set Packet Data
		packet.setParentTaskId(parentTaskId);
		packet.setResId(resId);
		packet.setStartDT(startDT);
		packet.setTermDT(termDT);
		packet.setProg(prog);
		logger.debug("prog={}", prog);
		packet.setColumns(columns);
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		TaskControl data = response.getEntity(TaskControl.class);
		
		wsStatus = data.getStatus();
	}

	/* getter */
	public int getWsStatus() {
		return wsStatus;
	}

	/* setter */
	public void setPacket(TaskControl packet) {
		this.packet = packet;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public void setParentTaskId(int parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public void setStartDT(long startDT) {
		this.startDT = startDT;
	}

	public void setTermDT(long termDT) {
		this.termDT = termDT;
	}

	public void setProg(double prog) {
		this.prog = prog;
	}

	public void setColumns(List<ColumnDataBean> columns) {
		this.columns = columns;
	}

}
