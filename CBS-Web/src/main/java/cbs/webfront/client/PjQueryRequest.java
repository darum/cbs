package cbs.webfront.client;

import java.util.List;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.ProjQuery;
import cbs.web.service.packet.databean.ProjDataBean;
import cbs.web.service.packet.provider.ProjQueryJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

/**
 * PJ照会(/pj_query/queryAll)
 * @author Yamashita
 *
 */
public class PjQueryRequest extends AbstractRequest {
	
	/** APIパス */
	private final String API_PATH = "/pj_query/queryAll";
	/** Request Packet */
	private ProjQuery req;
	
	/* Web frontとのデータやりとり */
	/** Response: Project Data */
	private List<ProjDataBean> dataList;
	
	/**
	 * コンストラクタ
	 * @param aAccount
	 * @param aPassword
	 */
	public PjQueryRequest(String aAccount, String aPassword) {
		super(aAccount, aPassword);
	}

	@Override
	protected String getApiPath() {
		return API_PATH;
	}

	@Override
	protected Class<?> getProviderClass() {
		return ProjQueryJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return req;
	}

	@Override
	protected void preRequest() throws Exception {
		/* Request Construction */
		req = new ProjQuery(super.getUserId());
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		ProjQuery data = response.getEntity(ProjQuery.class);
		
		wsStatus = data.getStatus();
		if (wsStatus == ProjQuery.CBS_WS_STAT_SUCCESS) {
			this.dataList = data.getResult();
		}
	}

	/* getter */
	public List<ProjDataBean> getDataList() {
		return dataList;
	}

}
