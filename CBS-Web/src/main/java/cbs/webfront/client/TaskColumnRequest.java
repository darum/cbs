package cbs.webfront.client;

import java.util.List;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.ColumnQuery;
import cbs.web.service.packet.databean.MetaDataBean;
import cbs.web.service.packet.provider.ColumnQueryJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

public class TaskColumnRequest extends AbstractRequest {
	
	private final String API_PATH = "/taskCol_query/queryAll";
	
	private ColumnQuery packet;
	
	/* Packet Parameter */
	/* 入力パラメータ */
	private int pjId;
	
	/* 出力パラメータ */
	private List<MetaDataBean> columns;
	
	public TaskColumnRequest(final String aAccount, final String aPassword, int inPjId) {
		super(aAccount, aPassword);
		
		pjId = inPjId;
	}

	@Override
	protected String getApiPath() {
		return API_PATH;
	}

	@Override
	protected Class<?> getProviderClass() {
		return ColumnQueryJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return packet;
	}

	@Override
	protected void preRequest() throws Exception {
		if (this.pjId == 0) {
			throw new IllegalStateException("パラメータ設定不足");
		}
		
		/* Request生成 */
		this.packet = new ColumnQuery();
		packet.setSessionId(super.getSessionId());
		packet.setProjId(this.pjId);
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		/* Packet取得 */
		ColumnQuery data = response.getEntity(ColumnQuery.class);
		
		wsStatus = data.getStatus();
		if (wsStatus == ColumnQuery.CBS_WS_STAT_SUCCESS) {
			columns = data.getColumns();
		}
	}

	/* getter */
	public List<MetaDataBean> getColumns() {
		return columns;
	}

}
