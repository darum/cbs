package cbs.webfront.client;

import java.util.List;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.TaskQuery;
import cbs.web.service.packet.databean.TaskDataBean;
import cbs.web.service.packet.provider.TaskQueryJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

/**
 * 子タスク情報の取得
 * "/task_query/query"の実行
 * @author Yamashita
 *
 */
public class TaskQueryRequest extends AbstractRequest {
	
	private TaskQuery packet;
	
	/* In Parameter */
	/** プロジェクトID */
	private int projId;
	/** 検索する親タスクID */
	private int parentId;
	/** 検索開始位置 */
	private int startPos = 0;
	/** 検索件数 */
	private int getCount = 0;
	
	/* Out Params */
	private List<TaskDataBean> tasks;

	public TaskQueryRequest(final String aAccount, final String aPassword, int inProjId, int parentTaskId) {
		super(aAccount, aPassword);
		
		projId = inProjId;
		parentId = parentTaskId;
	}
	
	@Override
	protected String getApiPath() {
		return "/task_query/query";
	}

	@Override
	protected Class<?> getProviderClass() {
		return TaskQueryJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return packet;
	}

	@Override
	protected void preRequest() throws Exception {
		if ((this.projId == 0)
				|| (this.parentId < 0)) {
			throw new IllegalStateException("パラメータ不足");
		}
		
		/* Request生成 */
		this.packet = new TaskQuery();
		packet.setSessionId(getSessionId());
		packet.setProjId(projId);
		packet.setTaskId(parentId);
		packet.setQueryStart(startPos);
		packet.setQueryCount(getCount);
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		TaskQuery data = response.getEntity(TaskQuery.class);
		
		wsStatus = data.getStatus();
		if (wsStatus == TaskQuery.CBS_WS_STAT_SUCCESS) {
			this.tasks = data.getTask();
		}
	}

	/* getter */
	public List<TaskDataBean> getTasks() {
		return tasks;
	}

}
