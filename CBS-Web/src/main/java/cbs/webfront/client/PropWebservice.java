package cbs.webfront.client;

import java.util.ResourceBundle;

/**
 * webservice.propertiesを保持するシングルトンクラス
 * @author Yamashita
 */
public class PropWebservice {
	
	private final String PROP_NAME = "webservice";
	
	private static PropWebservice instance;
	
	/* Property */
	private String wsUrl;
	
	private PropWebservice() {
		ResourceBundle bundle = ResourceBundle.getBundle(PROP_NAME);
		
		wsUrl = bundle.getString("WS_URL");
	}
	
	public String getWsUrl() {
		return wsUrl;
	}

	public synchronized static PropWebservice getInstance() {
		if (instance == null) {
			instance = new PropWebservice();
		}
		
		return instance;
	}
}
