package cbs.webfront.client;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.SysUserControl;
import cbs.web.service.packet.provider.SysUserControlJSONProvider;
import cbs.webfront.WebServiceException;
import cbs.webfront.util.Utility;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class UserRequest extends AbstractRequest {
	
	public UserRequest(String aNewAccount, String aNewPassword) {
		super(null, null);
		
		this.newAccount = aNewAccount;
		this.newPassword = aNewPassword;
	}

	private final String API_PATH = "/user/add";
	
	private String newAccount = "";
	private String newPassword = "";
	private String email = "";
	private String firstName;
	private String lastName;
	private int userId;

	/**
	 * super.execute()は、認証後に処理対象のWSをコールするので、
	 * ユーザ追加は認証処理を必要としないため、execute()をオーバーライドする
	 * @throws WebServiceException 
	 */
	@Override
	public void execute() throws WebServiceException {
		/* パラメータチェック */
		if ((Utility.IsBlankString(newAccount))
				|| (Utility.IsBlankString(newPassword))
				|| (Utility.IsBlankString(email))) {
			throw new IllegalStateException("パラメータ設定不足");
		}
		
		/* Request生成 */
		SysUserControl packet = new SysUserControl(newAccount, newPassword, email);
		if (firstName != null) {
			packet.setFirstName(firstName);
		}
		if (lastName != null) {
			packet.setLastName(lastName);
		}
		
		/* Config */
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		clientConfig.getClasses().add(SysUserControlJSONProvider.class);
		/* Client */
		Client client = Client.create(clientConfig);

		ClientResponse response = getWebServiceResponse(client, API_PATH, packet);
		SysUserControl data = response.getEntity(SysUserControl.class);
		wsStatus = data.getStatus();
		if (wsStatus == SysUserControl.CBS_WS_STAT_SUCCESS) {
			this.userId = data.getUserId();
		}
	}
	
	public int getUserId() {
		return userId;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	// 以下のメソッドは使用しない
	@Override
	protected String getApiPath() {
		return null;
	}

	@Override
	protected Class<?> getProviderClass() {
		return null;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return null;
	}

	@Override
	protected void preRequest() throws Exception {
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
	}

}
