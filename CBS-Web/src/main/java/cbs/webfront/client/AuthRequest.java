package cbs.webfront.client;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.SysUserAuth;
import cbs.web.service.packet.provider.SysUserAuthJSONProvider;
import cbs.webfront.WebServiceException;
import cbs.webfront.util.Utility;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * "/auth" Request
 * @author Yamashita
 */
public class AuthRequest extends AbstractRequest {

	private final String API_PATH = "/auth/login";
	
	public AuthRequest(String aAccount, String aPassword) {
		super(aAccount, aPassword);
	}

	/**
	 * super.execute()は、認証後に処理対象のWSをコールするので、
	 * 認証処理の場合はexecute()をオーバーライドする
	 * @throws WebServiceException 
	 */
	@Override
	public void execute() throws WebServiceException {
		String account = super.getAccount();
		String password = super.getPassword();
		
		/* パラメータチェック */
		if((Utility.IsBlankString(account))
				|| (Utility.IsBlankString(password))) {
			throw new IllegalStateException("パラメータ設定不足");
		}
		
		/* Request生成 */
		SysUserAuth packet = new SysUserAuth(account, password);
		
		/* Config */
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		clientConfig.getClasses().add(SysUserAuthJSONProvider.class);
		/* Client */
		Client client = Client.create(clientConfig);

		ClientResponse response = getWebServiceResponse(client, API_PATH, packet);
		SysUserAuth data = response.getEntity(SysUserAuth.class);
		wsStatus = data.getStatus();
	}

	// 以下のメソッドは使用しない
	@Override
	protected String getApiPath() {
		return null;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return null;
	}

	@Override
	protected Class<?> getProviderClass() {
		return null;
	}

	@Override
	protected void preRequest() throws Exception {
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		
	}
}
