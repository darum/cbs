package cbs.webfront.client;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.ProjUserControl;
import cbs.web.service.packet.provider.ProjUserControlJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

/**
 * PJユーザ追加Request
 * @author Yamashita
 *
 */
public class PjuserAddRequest extends AbstractRequest {
	/** API PATH */
	private final String API_PATH="/pjuser_ctrl/add";
	/** */
	private ProjUserControl request;
	
	/* */
	private int pjId;
	private int permission;
	private int pjUserId;
	
	/**
	 * コンストラクタ
	 * @param sysUid ユーザID
	 * @param projId プロジェクトID
	 * @param perm 権限
	 */
	public PjuserAddRequest(String aAccount, String aPassword, int projId, int perm) {
		super(aAccount, aPassword);
		
		this.pjId = projId;
		this.permission = perm;
	}

	@Override
	protected String getApiPath() {
		return API_PATH;
	}

	@Override
	protected Class<?> getProviderClass() {
		return ProjUserControlJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return request;
	}

	@Override
	protected void preRequest() throws Exception {
		/* parameter check */
		if ((pjId <= 0) ||
				(permission <= 0)) {
			throw new IllegalStateException("Lack of Paraeters");
		}
		
		/* Packet Construction */
		request = new ProjUserControl(super.getUserId(), pjId);
		request.setPermission(permission);
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		/* Get Response Packet */
		ProjUserControl data = response.getEntity(ProjUserControl.class);
		
		wsStatus = data.getStatus();
		if (wsStatus == ProjUserControl.CBS_WS_STAT_SUCCESS) {
			pjUserId = data.getPjUserId();
		}
	}

	public int getPjUserId() {
		return pjUserId;
	}

}
