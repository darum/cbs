package cbs.webfront.client;

import java.util.ArrayList;
import java.util.List;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.TaskControl;
import cbs.web.service.packet.databean.ColumnDataBean;
import cbs.web.service.packet.provider.TaskControlJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

public class TaskAddRequest extends AbstractRequest {
	
	private final String API_PATH = "/task_ctrl/add";
	
	private TaskControl packet;
	
	private int pjId;
	private String taskName;
	
	private int parentID = 0;
	private int taskChargeId = 0;
	private long startDT = 0L;
	private long termDT = 0L;
	private double prog = 0.0;
	private List<ColumnDataBean> columns = null;
	
	private int taskId;

	public TaskAddRequest(String aAccount, String aPassword, int aProjId, String aTaskName) {
		super(aAccount, aPassword);
		columns = new ArrayList<ColumnDataBean>();
		
		pjId = aProjId;
		taskName = aTaskName;
	}

	@Override
	protected String getApiPath() {
		return API_PATH;
	}

	@Override
	protected Class<?> getProviderClass() {
		return TaskControlJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return packet;
	}

	@Override
	protected void preRequest() throws Exception {
		packet = new TaskControl();
		packet.setSessionId(getSessionId());
		packet.setPjId(pjId);
		packet.setTaskName(taskName);
		
		packet.setParentTaskId(parentID);
		if (this.taskChargeId != 0) {
			packet.setResId(taskChargeId);
		}
		if (this.startDT != 0L) {
			packet.setStartDT(startDT);
		}
		if (this.termDT != 0L) {
			packet.setTermDT(termDT);
		}
		packet.setProg(prog);
		packet.setColumns(columns);
		
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		TaskControl data = response.getEntity(TaskControl.class);
		
		wsStatus = data.getStatus();
		if (wsStatus == TaskControl.CBS_WS_STAT_SUCCESS) {
			this.taskId = data.getTaskId();
		}
	}

	/* getter */
	public int getParentID() {
		return parentID;
	}

	public int getTaskChargeId() {
		return taskChargeId;
	}

	public long getStartDT() {
		return startDT;
	}

	public long getTermDT() {
		return termDT;
	}

	public double getProg() {
		return prog;
	}

	public List<ColumnDataBean> getColumns() {
		return columns;
	}

	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

	public void setTaskChargeId(int taskChargeId) {
		this.taskChargeId = taskChargeId;
	}

	public void setStartDT(long startDT) {
		this.startDT = startDT;
	}

	public void setTermDT(long termDT) {
		this.termDT = termDT;
	}

	public void setProg(double prog) {
		this.prog = prog;
	}

	public void setColumns(List<ColumnDataBean> columns) {
		this.columns = columns;
	}

	public int getTaskId() {
		return taskId;
	}

}
