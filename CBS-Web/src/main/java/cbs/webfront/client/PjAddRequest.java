package cbs.webfront.client;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.ProjControl;
import cbs.web.service.packet.provider.ProjControlJSONProvider;

import com.sun.jersey.api.client.ClientResponse;

/**
 * PJ追加
 * @author Yamashita
 *
 */
public class PjAddRequest extends AbstractRequest {
	/** API PATH */
	private final String API_PATH="/pj_ctrl/add";
	/** Request Packet */
	private ProjControl request;
	
	/* Frontとのデータやりとり */
	private String name;
	private int projId;

	/**
	 * コンストラクタ
	 * @param account
	 * @param password
	 * @param pjName
	 */
	public PjAddRequest(String account, String password, String pjName) {
		super(account, password);
		name = pjName;
	}

	@Override
	protected String getApiPath() {
		return API_PATH;
	}

	@Override
	protected Class<?> getProviderClass() {
		return ProjControlJSONProvider.class;
	}

	@Override
	protected AbstractPacket getRequestPacket() {
		return request;
	}

	@Override
	protected void preRequest() throws Exception {
		/* Packet Construction */
		request = new ProjControl(this.name);
		request.setSessionId(getSessionId());
	}

	@Override
	protected void postRequest(ClientResponse response) throws Exception {
		/* get Response packet */
		ProjControl data = response.getEntity(ProjControl.class);
		
		wsStatus = data.getStatus();
		if (wsStatus == ProjControl.CBS_WS_STAT_SUCCESS) {
			this.projId = data.getProjId();
		}
	}

	/* getter / setter */
	public int getProjId() {
		return projId;
	}

}
