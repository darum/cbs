package cbs.webfront.client;

import javax.ws.rs.core.MediaType;

import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cbs.web.service.packet.AbstractPacket;
import cbs.web.service.packet.SysUserAuth;
import cbs.web.service.packet.provider.SysUserAuthJSONProvider;
import cbs.webfront.WebServiceException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

/**
 * WebServiceを呼び出す抽象クラス
 * @author Yamashita
 * @note サブクラスではexecProc()を実装する
 * @note 値はgetterで返す形とする
 */
public abstract class AbstractRequest {
	Logger logger = LoggerFactory.getLogger(getClass());
	
	/* パラメータ */
	private String account;
	private String password;
	
	protected int wsStatus;
	private String sessionId = null;
	private int userId = 0;
	
	private final String LOGIN_API = "/auth/login";
	private final String LOGOUT_API = "/auth/logout";
	
	public AbstractRequest(String aAccount, String aPassword) {
		this.account = aAccount;
		this.password = aPassword;
	}
	
	/**
	 * 
	 * @throws WebServiceException WebServiceにエラーが発生した（HTTPエラーも含む）
	 */
	public void execute() throws Exception {
		logger.info("===StartRequest: " + getClass().toString());
		
		/* Config */
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, true);
		clientConfig.getClasses().add(SysUserAuthJSONProvider.class);
		clientConfig.getClasses().add(getProviderClass());
		
		/* Client */
		Client client = Client.create(clientConfig);
		
		ClientResponse response = null;
		try {
			/* Login */
			SysUserAuth login = new SysUserAuth(account, password);
			response = this.getWebServiceResponse(client, LOGIN_API, login);
			SysUserAuth auth = response.getEntity(SysUserAuth.class);
			String sessionId = auth.getSessionId();
			userId = auth.getUserId();
			response.close();
			response = null;
			logger.debug("WS Login Successful.");
			
			/* Pre-process */
			preRequest();
			/* Execute */
			AbstractPacket packet = this.getRequestPacket();
			packet.setSessionId(sessionId);
			response = this.getWebServiceResponse(client, this.getApiPath(), packet);
			/* Post process */
			postRequest(response);
			response.close();
			response = null;

			/* Logout */
			SysUserAuth logout = new SysUserAuth(userId);
			logout.setSessionId(sessionId);
			response = getWebServiceResponse(client, LOGOUT_API, logout);
			userId = 0;
			
		} finally {
			if (response != null) {
				response.close();
			}
		}
		logger.info("===FinishRequest: " + getClass().toString());
	}

	/**
	 * Requestを実行し、Responseを得る
	 * @param client
	 * @param api
	 * @return 
	 * @throws WebServiceException
	 */
	protected ClientResponse getWebServiceResponse(Client client, final String api, AbstractPacket packet) throws WebServiceException {
		/* Property */
		PropWebservice prop = PropWebservice.getInstance();

		/* Request */
		StringBuffer url = new StringBuffer();
		url.append(prop.getWsUrl());
		url.append(api);
		logger.debug("URL: " + url.toString());

		ClientResponse response = null;
		WebResource webResource = client.resource(url.toString());
		response = webResource.accept(MediaType.APPLICATION_JSON).
					type(MediaType.APPLICATION_JSON).
					post(ClientResponse.class, packet);
			
		int status = response.getStatus();
		if (status != Response.SC_OK) {
			response.close();
			logger.error("WebService HTTP Error: Code={}", status);
			throw new WebServiceException(url.toString(), status);
		} else {
			logger.debug("WebService HTTP_OK");
		}
		
		return response;
	}
	
	/**
	 * APIのパス（例：/user/addを返す）
	 * @return APIのパス
	 */
	abstract protected String getApiPath();
	/**
	 * JSONProviderクラスを返す
	 * @return JSONProvider
	 */
	abstract protected Class<?> getProviderClass();
	/**
	 * Requestパケットを返す
	 * @return リクエストパケット
	 */
	abstract protected AbstractPacket getRequestPacket();
	
	abstract protected void preRequest() throws Exception;
	
	abstract protected void postRequest(final ClientResponse response) throws Exception;

	/* getter */
	public int getWsStatus() {
		return wsStatus;
	}

	protected String getSessionId() {
		return sessionId;
	}

	protected String getAccount() {
		return account;
	}

	protected String getPassword() {
		return password;
	}
	
	protected int getUserId() {
		return this.userId;
	}
	
	/* setter */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

}
