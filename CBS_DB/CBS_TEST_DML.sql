﻿INSERT INTO CBS_SYSUSER
	(login_user_id,
	login_account,
	login_passwd,
	login_user_email)
VALUES
	(DEFAULT,
	'shared',
	'shared00',
	'shared@cbs.com');

INSERT INTO CBS_PROJECT
	(LOGIN_USER_ID,
	NAME)
SELECT login_user_id, 'マイプロジェクト' from cbs_sysuser where login_account='shared';

INSERT INTO CBS_PJUSER
	(PROJECT_ID,
	LOGIN_USER_ID,
	PERMISSION)
SELECT cbs_project.project_id, cbs_sysuser.login_user_id, 1 
from cbs_project, cbs_sysuser 
where cbs_sysuser.login_account='shared' and cbs_project.name='マイプロジェクト';
